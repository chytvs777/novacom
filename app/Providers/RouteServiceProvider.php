<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot()
    {
        //

        parent::boot();
    }

    public function map()
    {
        $this->mapAdminRoutes();
        $this->mapIndexRoutes();
    }

    protected function mapAdminRoutes()
    {
        Route::middleware(['web', 'localeSet:ru'])
            ->prefix('admin')
            ->as('admin.')
            ->namespace($this->namespace . '\Admin')
            ->group(base_path('routes/admin.php'));
    }

    protected function mapIndexRoutes()
    {
        Route::middleware(['web', 'localizationRedirect', 'setBreadcrumb'])
            ->as('index.')
            ->prefix(\LaravelLocalization::setLocale())
            ->namespace($this->namespace . '\Index')
            ->group(base_path('routes/index.php'));
    }
}
