<?php

namespace App\Providers;

use App\Services\Prettifier;
use Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Blade::directive('dd', function($data){
            return "<?php dd(with($data)) ;?>";
        });
        
        Blade::directive('dump', function($data){
            return "<?php dump(with($data)) ;?>";
        });

        Blade::directive('price', function($price){
            return '<?= \App\Services\Prettifier::prettifyPrice(with(' . $price . ')) ?>';
        });

		Blade::directive('textarea', function($text){
			return '<?= \App\Services\Prettifier::prettifyTextArea(with(' . $text . ')) ?>';
		});

		Blade::directive('date', function($date){
			return "<?php echo with($date)->day . ' ' . trans('dates.month.long.' . with($date)->month) . ' ' . with($date)->year;?>";
		});

		Blade::directive('dateShort', function($date){
			return "<?php echo with($date)->day . ' ' . trans('index.dates.month.long.' . with($date)->month) ;?>";
		});
    }

    public function register()
    {
        //
    }
}
