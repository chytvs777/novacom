<?php

namespace App\Providers;

use App\Models\Command;
use App\Models\History;
use App\Models\Setting\Setting;
use App\Observers\CommandObserver;
use App\Observers\HistoryObserver;
use App\Observers\SettingObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Command::observe(CommandObserver::class);
        History::observe(HistoryObserver::class);
        Setting::observe(SettingObserver::class);
    }
    
    public function register()
    {
        //
    }
}
