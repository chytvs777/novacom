<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{

    public function boot()
    {

        Form::macro('labelHtml', function ($name, $value = null, $options = array()) {
            return htmlspecialchars_decode(Form::label($name, $value, $options));
        });

        //Admin
        Form::component('onOffCheckbox', 'admin.partials.form.onOffCheckbox', ['name', 'checked', 'id']);
        Form::component('panelText', 'admin.partials.form.panelText', ['name', 'val', 'class', 'arr' => [], 'col' => true]);
        Form::component('panelRange', 'admin.partials.form.panelRange', ['name', 'min', 'max', 'val', 'class', 'arr' => [], 'col' => true]);
        Form::component('panelTextarea', 'admin.partials.form.panelTextarea', ['name', 'redactor' => false, 'type' => null, 'label' => null, 'disabled' => false]);
        Form::component('panelRadio', 'admin.partials.form.panelRadio', ['name', 'selected' => false, 'class' => '']);
        Form::component('panelSelect', 'admin.partials.form.panelSelect', ['name', 'values', 'selected', 'arr' => [], 'col' => true]);
        Form::component('panelButton', 'admin.partials.form.panelButton', ['text' => '']);
        Form::component('goToUrl', 'admin.partials.form.panelGoToUrl', ['url']);

        Form::component('panelTextLang', 'admin.partials.form.lang.panelTextLang', ['label', 'lang', 'model', 'val' => null]);
        Form::component('panelTextareaLang', 'admin.partials.form.lang.panelTextareaLang', ['label', 'lang', 'model', 'redactor' => false, 'type' => null, 'val' => null]);

    }

    public function register()
    {

    }
}