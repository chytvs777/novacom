<?php

namespace App\Services;

use App\Models\Command;
use App\Models\Project\Option;
use App\Models\Setting\City;
use App\Models\Setting\Country;
use App\Models\Vacancy\Direction;

class SelectService
{
    public static function pressService()
    {
        return Command::query()
            ->whereIsPressService(1)
            ->get()
            ->pluck('full_name', 'id')
            ->prepend('- Выберите сотрудника -', '');
    }

    public static function commands()
    {
        return Command::query()
            ->get()
            ->pluck('full_name', 'id')
            ->prepend('- Выберите сотрудника -', '');
    }

    public static function countries()
    {
        return Country::query()
            ->get()
            ->pluck('title', 'id')
            ->prepend(__("index.elements.fields.country_id.placeholder"), '');
    }

    public static function cities($countryId)
    {
        return City::query()
            ->when($countryId, function ($q) use ($countryId) {
                return $q->whereCountryId($countryId);
            })
            ->get()
            ->pluck('title', 'id')
            ->prepend(__("index.elements.fields.city_id.placeholder"), '');
    }

    public static function directions()
    {
        return Direction::query()
            ->get()
            ->pluck('title', 'id')
            ->prepend(__("index.elements.fields.direction_id.placeholder"), '');
    }

    public static function optionsGroup($frontend = null)
    {
        $select = [];
        $options = Option::all()->groupBy('type');

        foreach($options as $items) {
            $placeholderArr = [];
            $key = __("index.pages.projects.types.{$items->first()->type}");
            if ($frontend) {
                $placeholderArr = ['' => __('index.pages.projects.solutions.all')];
                $key = $items->first()->type;
            }
            $select[$key] = $placeholderArr + $items->pluck('title', 'id')->toArray();
        }

        return $select;
    }
}