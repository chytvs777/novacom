<?php

namespace App\Models;

use App\Traits\ImageableTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    use Translatable, ImageableTrait;

    const KEY_HOME = 'home';
    const KEY_ARTICLES = 'news';
    const KEY_LEADERSHIP = 'leadership';
    const KEY_CONTACTS = 'contacts';
    const KEY_VACANCIES = 'vacancies';
    const KEY_WORK = 'work';
    const KEY_APPLY_NOW = 'apply-now';
    const KEY_PROJECTS = 'projects';
    const KEY_PROJECT_SOLUTIONS = 'project-solutions';
    const KEY_HISTORY = 'history';
    const KEY_ABOUT = 'about';

    public $translatedAttributes = [
        'title', 'content_short', 'content', 'data', 'meta_title', 'meta_description',
    ];

    protected $fillable = [
        'key',
    ];

    protected $with = ['translations'];

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => true,
            'params' => [
                'statuses' => [
                    'w' => 314,
                    'h' => 268,
                    'fit' => 'crop',
                ],
            ],
        ],
    ];

    //Mutators
    public function getGenerateMetaTitleAttribute()
    {
        return $this->meta_title ? $this->meta_title : $this->title;
    }

    public function getGenerateMetaDescriptionAttribute()
    {
        return $this->meta_description ? $this->meta_description : $this->title;
    }

}
