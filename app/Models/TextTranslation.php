<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content_short', 'content', 'data', 'meta_title', 'meta_description',
    ];

    protected $casts = ['data' => 'array'];
}
