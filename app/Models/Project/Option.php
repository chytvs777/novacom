<?php

namespace App\Models\Project;

use App\Traits\ImageableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use Sluggable, Translatable, ImageableTrait;

    public $translatedAttributes = [
        'title',
    ];

    protected $fillable = [
        'slug', 'type',
    ];

    protected $with = ['translations'];

    const TYPE_INDUSTRY = 'industry';
    const TYPE_TECHNOLOGY = 'technology';
    const TYPE_EXPERTISE = 'expertise';

    const TYPES = [
        self::TYPE_INDUSTRY,
        self::TYPE_TECHNOLOGY,
        self::TYPE_EXPERTISE,
    ];

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'small' => [
                    'h' => 68,
                    'fit' => 'max',
                ],
            ],
        ],
    ];

    public function sluggable()
    {
        return ['slug' => ['source' => 'title']];
    }

    //Mutators
    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
