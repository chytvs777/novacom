<?php

namespace App\Models\Project;

use App\Models\Image;
use App\Traits\ImageableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Sluggable, Translatable, ImageableTrait;

    public $translatedAttributes = [
        'title', 'content_short', 'customer_name', 'customer_info', 'customer_site',
        'content', 'data', 'meta_title', 'meta_description',
        'review_name', 'review_position', 'review_text',
    ];

    protected $fillable = [
        'order', 'slug', 'customer_type', 'is_review',
    ];

    protected $with = ['translations'];

    const IMAGE_TYPE_IMAGE = 'image';
    const IMAGE_TYPE_CUSTOMER = 'customer';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'cell' => [
                    'w' => 580,
                    'h' => 351,
                    'fit' => 'crop',
                ],
                'big' => [
                    'w' => 1920,
                    'h' => 546,
                    'fit' => 'crop',
                ],
            ],
        ],
        self::IMAGE_TYPE_CUSTOMER => [
            'multiple' => false,
            'params' => [
                'small' => [
                    'h' => 190,
                    'fit' => 'max',
                ],
            ],
        ],
    ];

    const CUSTOMER_TYPE_GOS = '0';
    const CUSTOMER_TYPE_INDUSTRY = '1';
    const CUSTOMER_TYPE_BUSINESS = '2';

    const CUSTOMER_TYPES = [
        self::CUSTOMER_TYPE_GOS,
        self::CUSTOMER_TYPE_INDUSTRY,
        self::CUSTOMER_TYPE_BUSINESS,
    ];

    public function sluggable()
    {
        return ['slug' => ['source' => 'title']];
    }

    public function mainImage()
    {
        return $this->morphOne(Image::class, 'imageable')->ofType(self::IMAGE_TYPE_IMAGE)->main();
    }

    public function customerImage()
    {
        return $this->morphOne(Image::class, 'imageable')->ofType(self::IMAGE_TYPE_CUSTOMER)->main();
    }

    public function options()
    {
        return $this->belongsToMany(Option::class);
    }

    public function technologies()
    {
        return $this->options()->whereType(Option::TYPE_TECHNOLOGY)->has('mainImage')->with('mainImage');
    }

    //Mutators
    public function getUrlAttribute()
    {
        return route('index.projects.solutions.show', $this);
    }

    public function getGenerateMetaTitleAttribute()
    {
        return $this->meta_title ? $this->meta_title : $this->title;
    }

    public function getGenerateMetaDescriptionAttribute()
    {
        return $this->meta_description ? $this->meta_description : $this->title;
    }

    //Scopes
    public function scopeFilter($query, $data)
    {
        $options = array_get($data, 'options');

        $query
            ->when($options, function ($q) use ($options) {
                return $q->where(function ($q) use ($options) {
                    if (!is_array($options)) $options = [$options];
                    foreach ($options as $option) {
                        $q->whereHas('options', function ($q) use ($option) {
                            $q->where('id', $option);
                        });
                    }
                });
            })
        ;

        return $query;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
