<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class OptionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
    ];
}