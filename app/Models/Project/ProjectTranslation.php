<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content_short', 'customer_name', 'customer_info', 'customer_site',
        'content', 'data', 'meta_title', 'meta_description',
    ];

    protected $casts = [
        'data' => 'array',
    ];
}