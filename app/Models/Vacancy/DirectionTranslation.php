<?php

namespace App\Models\Vacancy;

use Illuminate\Database\Eloquent\Model;

class DirectionTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
    ];
}
