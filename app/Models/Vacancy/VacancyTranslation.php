<?php

namespace App\Models\Vacancy;

use Illuminate\Database\Eloquent\Model;

class VacancyTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content_short', 'data', 'meta_title', 'meta_description',
        'content_title', 'content_sub_title', 'content_one' , 'content_two',
    ];

    protected $casts = [
        'data' => 'array',
    ];
}
