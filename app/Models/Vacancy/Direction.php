<?php

namespace App\Models\Vacancy;

use App\Traits\ImageableTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    use Translatable;

    public $translatedAttributes = [
        'title',
    ];

    protected $fillable = [
        'order',
    ];

    protected $with = ['translations'];

    public function vacancies()
    {
        return $this->hasMany(Vacancy::class);
    }

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
