<?php

namespace App\Models\Vacancy;

use App\Models\Command;
use App\Traits\ImageableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    use Sluggable, Translatable, ImageableTrait;

    public $translatedAttributes = [
        'title', 'content_short', 'data', 'meta_title', 'meta_description',
        'content_title', 'content_sub_title', 'content_one' , 'content_two',
    ];

    protected $fillable = [
        'order', 'is_hot', 'direction_id', 'command_id',
    ];

    protected $with = ['translations'];

    public function sluggable()
    {
        return ['slug' => ['source' => 'title']];
    }

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'poster' => [
                    'w' => 1920,
                    'h' => 548,
                    'fit' => 'crop',
                ],
            ],
        ],
    ];

    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }

    public function command()
    {
        return $this->belongsTo(Command::class);
    }

    //Mutators
    public function getUrlAttribute()
    {
        return route('index.vacancies.show', $this);
    }

    public function getGenerateMetaTitleAttribute()
    {
        return $this->meta_title ? $this->meta_title : $this->title;
    }

    public function getGenerateMetaDescriptionAttribute()
    {
        return $this->meta_description ? $this->meta_description : $this->title;
    }

    //Scopes
    public function scopeFilter($query, $data)
    {
        $directionId = array_get($data, 'direction_id');
        $keyword = array_get($data, 'keyword');

        $query
            ->when($directionId, function ($q) use ($directionId) {
                return $q->where('direction_id', $directionId);
            })
            ->when($keyword, function ($q) use ($keyword) {
                return $q->whereHas('translations', function ($q) use ($keyword) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', 'like', "%$keyword%");
                });
            })
        ;

        return $query;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
