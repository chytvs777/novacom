<?php

namespace App\Models\Article;

use App\Models\Command;
use App\Traits\ImageableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use Sluggable, Translatable, ImageableTrait;

    public $translatedAttributes = [
        'title', 'content', 'content_short', 'announcement', 'review_text', 'review_author',
        'reference_information', 'meta_title', 'meta_description',
    ];

    protected $fillable = [
        'slug', 'command_id',
    ];

    protected $with = ['translations'];

    public function sluggable()
    {
        return ['slug' => ['source' => 'title']];
    }

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'big' => [
                    'w' => 618,
                    'h' => 232,
                    'fit' => 'crop',
                ],
                'small' => [
                    'w' => 303,
                    'h' => 228,
                    'fit' => 'crop',
                ],
                'poster' => [
                    'w' => 1920,
                    'h' => 548,
                    'fit' => 'crop',
                ],
            ],
        ],
    ];

    public function command()
    {
        return $this->belongsTo(Command::class);
    }

    //Mutators
    public function getUrlAttribute()
    {
        return route('index.articles.show', $this->slug);
    }

    public function getGenerateMetaTitleAttribute()
    {
        return $this->meta_title ? $this->meta_title : $this->title;
    }

    public function getGenerateMetaDescriptionAttribute()
    {
        return $this->meta_description ? $this->meta_description : $this->title;
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
