<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

class ArticleTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content', 'content_short', 'announcement', 'review_text', 'review_author',
        'reference_information', 'meta_title', 'meta_description',
    ];
}