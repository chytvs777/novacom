<?php

namespace App\Models;

use Spatie\Glide\GlideImage;
use Illuminate\Database\Eloquent\Model;
use File;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = ['path', 'main', 'type', 'description_ru', 'description_en', 'order'];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function scopeMain($query)
    {
        return $query->where('main', '=', 1);
    }

    public function scopeOfType($query, $type)
    {
        return $query->where('type', '=', $type);
    }

    public function load($params = 'original', $model = null)
    {
        if (!$model) $model = $this->imageable;
        $path = $model->getImagePath($this->type, $params, $this->path);

        if (!File::exists($path)) {
            if ($params != 'original') {
                $path = $this->resize($params, $model);
            }
        }

        $path = str_replace('\\', '/', $path);
        return '/' . $path;
    }

    public function resize($params, $model = null)
    {
        if (!$model) $model = $this->imageable;
        $path = $model->getImagePath($this->type, 'original', $this->path);
        if (!File::exists($path)) {
            return $path;
        }
        $paramsArray = $model::getImageParams($this->type, $params);
        if (empty($paramsArray)) {
            return $model->getImagePath($this->type, 'original', $this->path);
        }
        $newPath = $model->getImagePath($this->type, $params);
        $newPathWithFile = $model->getImagePath($this->type, $params, $this->path);
        if (!File::exists($newPath)) {
            File::makeDirectory($newPath, 0777, true);
        }

        (new GlideImage())->load($path)
            ->useAbsoluteSourceFilePath()
            ->modify($paramsArray)
            ->save($newPathWithFile);

        return $newPathWithFile;
    }

    public static function slug2Model($slug)
    {
        $modelsPath = config('panel.models_path');
        $classElements = explode('-', $slug);
        $classElements = array_map(function ($value) {
            return studly_case($value);
        }, $classElements);
        $class = implode('\\', $classElements);
        $class = $modelsPath . '\\' . $class;
        $class = ltrim($class, '\\');
        return $class;
    }

    public function getTitleAttribute()
    {
        return \LaravelLocalization::getCurrentLocale() == 'ru' ? $this->description_ru : $this->description_en;
    }

}
