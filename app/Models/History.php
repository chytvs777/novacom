<?php

namespace App\Models;

use App\Traits\ImageableTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use Translatable, ImageableTrait;

    public $translatedAttributes = [
        'content',
    ];

    protected $fillable = [
        'slug', 'years', 'order',
    ];

    protected $with = ['translations'];

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'poster' => [
                    'w' => 1918,
                    'h' => 936,
                    'fit' => 'crop',
                ],
            ],
        ],
    ];

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('content', '!=', '');
                });
            });
        }
    }
}
