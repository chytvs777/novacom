<?php

namespace App\Models;

use App\Traits\ImageableTrait;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    use Translatable, ImageableTrait;

    public $translatedAttributes = [
        'full_name', 'position', 'phone_home', 'phone_mobile', 'content', 'review_text',
    ];

    protected $fillable = [
        'slug', 'email', 'is_leadership', 'is_press_service', 'is_show_news', 'is_reviews', 'order',
    ];

    protected $with = ['translations'];

    const IMAGE_TYPE_IMAGE = 'image';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_IMAGE => [
            'multiple' => false,
            'params' => [
                'news' => [
                    'w' => 273,
                    'h' => 273,
                    'fit' => 'crop',
                ],
                'leadership' => [
                    'w' => 370,
                    'h' => 370,
                    'fit' => 'crop',
                ],
            ],
        ],
    ];

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('full_name', '!=', '');
                });
            });
        }
    }
}
