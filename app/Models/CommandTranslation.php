<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommandTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'full_name', 'position', 'phone_home', 'phone_mobile', 'content', 'review_text',
    ];
}
