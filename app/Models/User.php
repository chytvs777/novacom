<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    //Accessors & Mutators
    public function setPasswordAttribute($value)
    {
        if($value) $this->attributes['password'] = bcrypt($value);
    }

    //Scopes
    public function scopeFilter($query, $data)
    {
        $name = array_get($data, 'first_name');
        $query
            ->when($name, function ($q) use ($name) {
                return $q->where('first_name', 'like', "%$name%");
            })
        ;
        return $query;
    }
}
