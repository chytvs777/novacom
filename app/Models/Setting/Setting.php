<?php

namespace App\Models\Setting;

use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ImageableTrait;

class Setting extends Model
{
    use ImageableTrait;

    protected $fillable = [
        'email_for_notification', 'facebook', 'in',
    ];

    const IMAGE_TYPE_CLIENTS = 'clients';
    const IMAGE_TYPE_TECHNOLOGIES_ONE = 'technologies_one';
    const IMAGE_TYPE_TECHNOLOGIES_TWO = 'technologies_two';

    const IMAGES_PARAMS = [
        self::IMAGE_TYPE_CLIENTS => [
            'multiple' => true,
            'params' => [
                'small' => [
                    'h' => 60,
                    'fit' => 'max',
                ],
            ],
        ],
        self::IMAGE_TYPE_TECHNOLOGIES_ONE => [
            'multiple' => true,
            'params' => [
                'small' => [
                    'w' => 200,
                    'fit' => 'max',
                ],
            ],
        ],
        self::IMAGE_TYPE_TECHNOLOGIES_TWO => [
            'multiple' => true,
            'params' => [
                'small' => [
                    'w' => 200,
                    'fit' => 'max',
                ],
            ],
        ],
    ];

    public function clients()
    {
        return $this->morphMany(Image::class, 'imageable')->ofType(self::IMAGE_TYPE_CLIENTS)->orderBy('order');
    }

    public function technologiesOne()
    {
        return $this->morphMany(Image::class, 'imageable')->ofType(self::IMAGE_TYPE_TECHNOLOGIES_ONE)->orderBy('order');
    }

    public function technologiesTwo()
    {
        return $this->morphMany(Image::class, 'imageable')->ofType(self::IMAGE_TYPE_TECHNOLOGIES_TWO)->orderBy('order');
    }
}
