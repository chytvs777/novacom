<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
    ];
}
