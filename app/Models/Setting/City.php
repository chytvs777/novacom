<?php

namespace App\Models\Setting;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Translatable;

    public $translatedAttributes = [
        'title'
    ];

    protected $fillable = [
        'country_id', 'order',
    ];

    protected $with = ['translations'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    protected static function boot()
    {
        parent::boot();

        if (!str_contains(request()->url(), 'admin')) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->whereHas('translations', function ($q) {
                    $q->whereLocale(\LaravelLocalization::getCurrentLocale())->where('title', '!=', '');
                });
            });
        }
    }
}
