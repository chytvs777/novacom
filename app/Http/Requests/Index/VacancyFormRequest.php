<?php

namespace App\Http\Requests\Index;

use App\Http\Requests\Request;
use App\Models\Setting\City;
use App\Models\Setting\Country;
use App\Models\Vacancy\Vacancy;
use Illuminate\Validation\Rule;

class VacancyFormRequest extends Request
{
	public function rules()
	{
        $countryId = $this->get('country_id') ? $this->get('country_id') : 'a';

		$rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
		    'message' => 'required',
		    'country_id' => [
		        'required',
                Rule::exists('countries', 'id'),
            ],
		    'city_id' => [
                'required',
                Rule::exists('cities', 'id')->where('country_id', $countryId),
            ],
		    'file' => 'required|mimes:doc,docx,pdf',
        ];

		return $rules;
	}

    public function getData()
    {
        $data = $this->except('vacancy', 'country', 'city');

        return array_merge($data, [
            'vacancy' => Vacancy::find($this->get('vacancy_id'))->title ?? '',
            'country' => Country::find($this->get('country_id'))->title ?? '',
            'city' => City::find($this->get('city_id'))->title ?? '',
        ]);
	}

    public function attributes()
    {
        return [
            'file' => __('index.elements.fields.file.attribute'),
        ];
	}

    public function messages()
    {
        return [
            'first_name.required' => __('index.elements.fields.first_name.error'),
            'last_name.required' => __('index.elements.fields.last_name.error'),
            'email.required' => __('index.elements.fields.email.error'),
            'email.email' => __('index.elements.fields.email.error_email'),
            'file.required' => __('index.elements.fields.file.error'),
            'message.required' => __('index.elements.fields.message.error'),
            'country_id.required' => __('index.elements.fields.country_id.error'),
            'city_id.required' => __('index.elements.fields.city_id.error'),
            'city_id.exists' => '',
        ];
	}

}