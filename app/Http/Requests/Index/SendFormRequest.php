<?php

namespace App\Http\Requests\Index;

use App\Http\Requests\Request;

class SendFormRequest extends Request
{
	public function rules()
	{
		$rules = [
            'first_name' => 'required',
            'email' => 'required|email',
		    'message' => 'required',
        ];

		return $rules;
	}

    public function messages()
    {
        return [
            'first_name.required' => __('index.elements.fields.first_name.error'),
            'email.required' => __('index.elements.fields.email.error'),
            'email.email' => __('index.elements.fields.email.error_email'),
            'message.required' => __('index.elements.fields.message.error'),
        ];
	}

}