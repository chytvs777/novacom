<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class HistoryRequest extends Request
{
    public function rules()
    {
        $rules = [
            'years' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'years' => __('admin_labels.years'),
        ];
    }
}