<?php

namespace App\Http\Requests\Admin\Setting;

use App\Http\Requests\Request;

class CityRequest extends Request
{
    public function rules()
    {
        $rules = [
            'ru.title' => 'required',
            'country_id' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
            'country_id' => trans('admin_labels.country_id'),
        ];
    }
}