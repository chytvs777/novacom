<?php

namespace App\Http\Requests\Admin\Setting;

use App\Http\Requests\Request;

class CountryRequest extends Request
{
    public function rules()
    {
        $rules = [
            'ru.title' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
        ];
    }
}