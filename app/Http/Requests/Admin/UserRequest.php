<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;
use App\Models\User;

class UserRequest extends Request
{
    public function rules()
    {
        $rules = [
            'first_name'  => 'required',
            'email' => 'required|email|unique:users,email,' . $this->get('id'),
        ];

        if (!$this->get('id')) $rules['password'] = 'required';
        return $rules;
    }

    public function attributes()
    {
        return [
            'first_name' => trans('admin_labels.first_name'),
            'email' => trans('admin_labels.email'),
            'password' => trans('admin_labels.password'),
        ];
    }
}