<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class CommandRequest extends Request
{
    public function rules()
    {
        $rules = [
            'ru.full_name' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'full_name' => trans('admin_labels.full_name'),
        ];
    }
}