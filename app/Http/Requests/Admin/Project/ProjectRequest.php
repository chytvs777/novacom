<?php

namespace App\Http\Requests\Admin\Project;

use App\Http\Requests\Request;

class ProjectRequest extends Request
{
    public function rules()
    {
        $rules = [];

        $rules += [
            'ru.title' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
        ];
    }
}