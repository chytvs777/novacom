<?php

namespace App\Http\Requests\Admin\Article;

use App\Http\Requests\Request;

class ArticleRequest extends Request
{
    public function rules()
    {
        $rules = [];

        $rules += [
            'ru.title' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
        ];
    }

    public function getData()
    {
        $data = $this->all();
        $data['command_id'] = $this->get('command_id') ? $this->get('command_id') : null;

        return $data;
    }
}