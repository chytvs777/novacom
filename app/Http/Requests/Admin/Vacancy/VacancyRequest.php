<?php

namespace App\Http\Requests\Admin\Vacancy;

use App\Http\Requests\Request;

class VacancyRequest extends Request
{
    public function rules()
    {
        $rules = [];

        $rules += [
            'ru.title' => 'required',
            'direction_id' => 'required',
        ];

        return $rules;
    }

    public function getData()
    {
        $data = $this->all();
        $data['command_id'] = $this->get('command_id') ? $this->get('command_id') : null;

        return $data;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
            'direction_id' => trans('admin_labels.direction_id'),
        ];
    }
}