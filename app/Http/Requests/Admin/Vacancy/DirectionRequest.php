<?php

namespace App\Http\Requests\Admin\Vacancy;

use App\Http\Requests\Request;

class DirectionRequest extends Request
{
    public function rules()
    {
        $rules = [];

        $rules += [
            'ru.title' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'ru.title' => trans('admin_labels.title'),
        ];
    }
}