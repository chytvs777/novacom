<?php

namespace App\Http\Middleware;

use Closure;

class SetBreadcrumbMiddleware
{
    public function handle($request, Closure $next, $view = 'index.partials.breadcrumbs')
    {
        config(['breadcrumbs.view' => $view]);

        return $next($request);
    }
}
