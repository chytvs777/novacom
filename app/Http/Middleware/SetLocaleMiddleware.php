<?php

namespace App\Http\Middleware;

use Closure;

class SetLocaleMiddleware
{
    public function handle($request, Closure $next, $locale = 'ru')
    {
        app()->setLocale($locale);

        return $next($request);
    }
}
