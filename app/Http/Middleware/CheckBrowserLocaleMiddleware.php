<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class CheckBrowserLocaleMiddleware
{
    protected $agent;

    protected $currentLang;

    protected $defaultLang;

    public function __construct()
    {
        $this->agent = new Agent();
        $this->currentLang = \LaravelLocalization::getCurrentLocale();
        $this->defaultLang = \LaravelLocalization::getDefaultLocale();
    }

    public function handle($request, Closure $next)
    {
        if (!session('locale-detect') && !$this->agent->isRobot() && $this->currentLang == $this->defaultLang) {

            session(['locale-detect' => true]);
            foreach ($this->agent->languages() as $lang) {
                if ($lang == $this->currentLang) {
                    break;
                }

                if (isset(\LaravelLocalization::getSupportedLocales()[$lang])) {
                    $url = \LaravelLocalization::getLocalizedURL($lang);
                    if (strpos(implode(', ', get_headers($url)), '200 OK')) {
                        return redirect($url);
                    }
                }
            }
        }

        return $next($request);
    }

}