<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Models\User;

class UserController extends Controller
{
	protected $entity = 'users';

	public function index()
	{
		$users = User::filter(request()->all())->latest()->paginate();

		if (request()->ajax() && !request('_pjax')) return $this->ajaxView($users);

		return view('admin.' . $this->entity . '.index', compact('users') + ['entity' => $this->entity]);
	}

	public function create()
	{
		$user = new User();

		return view('admin.' . $this->entity . '.edit', compact('user') + ['entity' => $this->entity]);
	}

	public function edit(User $user)
	{
		return view('admin.' . $this->entity . '.edit', compact('user') + ['entity' => $this->entity]);
	}

	public function store(UserRequest $request)
	{
        $user = User::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        $user->syncImages($request->getData());

		return $this->responseSuccess();
	}

	public function delete(User $user)
	{
		$user->delete();

		return $this->responseSuccess();
	}

	protected function ajaxView($users)
	{
		return response([
			'view' => view('admin.' . $this->entity . '.index.table', compact('users') + ['entity' => $this->entity])->render(),
			'pagination' => view('admin.partials.pagination', ['paginator' => $users])->render(),
		])->header('Cache-Control', 'no-cache, no-store');
	}
}