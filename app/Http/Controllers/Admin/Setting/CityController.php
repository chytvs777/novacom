<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\CityRequest;
use App\Models\Setting\City;
use App\Services\SelectService;

class CityController extends Controller
{
    protected $entity = 'settings.cities';

    public function index()
    {
        $cities = City::with('country')->latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($cities);

        return view('admin.' . $this->entity . '.index', compact('cities') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $city = new City();
        $countries = SelectService::countries();

        return view('admin.' . $this->entity . '.edit', compact('city', 'countries') + ['entity' => $this->entity]);
    }

    public function edit(City $city)
    {
        $countries = SelectService::countries();

        return view('admin.' . $this->entity . '.edit', compact('city', 'countries') + ['entity' => $this->entity]);
    }

    public function store(CityRequest $request)
    {
        $city = City::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        return $this->responseSuccess();
    }

    public function delete(City $city)
    {
        $city->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($cities)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('cities') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $cities])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}