<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\CountryRequest;
use App\Models\Setting\Country;

class CountryController extends Controller
{
    protected $entity = 'settings.countries';

    public function index()
    {
        $countries = Country::latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($countries);

        return view('admin.' . $this->entity . '.index', compact('countries') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $country = new Country();

        return view('admin.' . $this->entity . '.edit', compact('country') + ['entity' => $this->entity]);
    }

    public function edit(Country $country)
    {
        return view('admin.' . $this->entity . '.edit', compact('country') + ['entity' => $this->entity]);
    }

    public function store(CountryRequest $request)
    {
        $country = Country::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        return $this->responseSuccess();
    }

    public function delete(Country $country)
    {
        $country->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($countries)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('countries') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $countries])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}