<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\SettingRequest;
use App\Models\Setting\Setting;

class SettingController extends Controller
{
    public function edit()
    {
        $setting = Setting::first();

        return view('admin.settings.edit', compact('setting'));
    }

    public function store(SettingRequest $request)
    {
        $setting = Setting::first();
        $setting->update(request()->all());
        $setting->syncImages(request()->all());

        return $this->responseSuccess();
    }
}