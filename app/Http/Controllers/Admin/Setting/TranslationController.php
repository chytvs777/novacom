<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\TranslationLoader\LanguageLine;

class TranslationController extends Controller
{
    public function index()
    {
        $languageLines = LanguageLine::where('group', 'index')->get();

        return view('admin.settings.translation.index', compact('languageLines'));
    }

    public function store(Request $request)
    {
        $languageLines = $request->input('language_lines');

        foreach ($languageLines as $id => $params) {
            $text = [];
            foreach (\LaravelLocalization::getSupportedLocales() as $lang => $localization) {
                $text[$lang] = $params[$lang];
            }

            LanguageLine::find($id)->update([
                'text' => $text,
            ]);
        }

        return $this->responseSuccess();
    }
}
