<?php

namespace App\Http\Controllers\Admin\Article;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Article\ArticleRequest;
use App\Models\Article\Article;
use App\Services\SelectService;

class ArticleController extends Controller
{
    protected $entity = 'articles';

    public function index()
    {
        $articles = Article::latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($articles);

        return view('admin.' . $this->entity . '.index', compact('articles') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $article = new Article();
        $commands = SelectService::pressService();

        return view('admin.' . $this->entity . '.edit', compact('article', 'commands') + ['entity' => $this->entity]);
    }

    public function edit(Article $article)
    {
        $commands = SelectService::pressService();

        return view('admin.' . $this->entity . '.edit', compact('article', 'commands') + ['entity' => $this->entity]);
    }

    public function store(ArticleRequest $request)
    {
        $article = Article::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        $article->syncImages($request->getData());

        return $this->responseSuccess();
    }

    public function delete(Article $article)
    {
        $article->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($articles)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('articles') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $articles])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}