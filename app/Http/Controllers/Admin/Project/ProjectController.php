<?php

namespace App\Http\Controllers\Admin\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Project\ProjectRequest;
use App\Models\Project\Project;
use App\Services\SelectService;

class ProjectController extends Controller
{
    protected $entity = 'projects';

    public function index()
    {
        $projects = Project::latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($projects);

        return view('admin.' . $this->entity . '.index', compact('projects') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $project = new Project();
        $optionsGroup = SelectService::optionsGroup();

        return view('admin.' . $this->entity . '.edit', compact('project', 'optionsGroup') + ['entity' => $this->entity]);
    }

    public function edit(Project $project)
    {
        $optionsGroup = SelectService::optionsGroup();

        return view('admin.' . $this->entity . '.edit', compact('project', 'optionsGroup') + ['entity' => $this->entity]);
    }

    public function store(ProjectRequest $request)
    {
        $project = Project::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );
        $project->syncImages($request->all());
        $project->options()->sync($request->input('options', []));

        return $this->responseSuccess();
    }

    public function delete(Project $project)
    {
        $project->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($projects)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('projects') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $projects])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}