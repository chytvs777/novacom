<?php

namespace App\Http\Controllers\Admin\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Project\OptionRequest;
use App\Models\Project\Option;

class OptionController extends Controller
{
    protected $entity = 'projects.options';

    public function index()
    {
        $options = Option::latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($options);

        return view('admin.' . $this->entity . '.index', compact('options') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $option = new Option();

        return view('admin.' . $this->entity . '.edit', compact('option') + ['entity' => $this->entity]);
    }

    public function edit(Option $option)
    {
        return view('admin.' . $this->entity . '.edit', compact('option') + ['entity' => $this->entity]);
    }

    public function store(OptionRequest $request)
    {
        $option = Option::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );
        $option->syncImages($request->all());

        return $this->responseSuccess();
    }

    public function delete(Option $option)
    {
        $option->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($options)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('options') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $options])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}