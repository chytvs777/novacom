<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Auth\LoginController as BaseLoginController;

class LoginController extends BaseLoginController
{
    protected $redirectTo = '/admin/users';

    public function showLoginForm()
    {
        return view('panel::auth.login');
    }
}
