<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TextRequest;
use App\Models\Text;

class TextController extends Controller
{
    protected $entity = 'texts';

    public function index()
    {
        $texts = Text::latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($texts);

        return view('admin.' . $this->entity . '.index', compact('texts') + ['entity' => $this->entity]);
    }

    public function edit(Text $text)
    {
        return view('admin.' . $this->entity . '.edit', compact('text') + ['entity' => $this->entity]);
    }

    public function store(TextRequest $request)
    {
        $text = Text::find($request->input('id'));
        $text->update($request->getData());
        $text->syncImages($request->all());

        return $this->responseSuccess();
    }

    protected function ajaxView($texts)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('texts') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $texts])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}