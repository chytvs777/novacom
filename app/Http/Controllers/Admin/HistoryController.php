<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\HistoryRequest;
use App\Models\History;

class HistoryController extends Controller
{
    protected $entity = 'histories';

    public function index()
    {
        $histories = History::orderBy('order')->get();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($histories);

        return view('admin.' . $this->entity . '.index', compact('histories') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $history = new History();

        return view('admin.' . $this->entity . '.edit', compact('history') + ['entity' => $this->entity]);
    }

    public function edit(History $history)
    {
        return view('admin.' . $this->entity . '.edit', compact('history') + ['entity' => $this->entity]);
    }

    public function store(HistoryRequest $request)
    {
        $history = History::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        $history->syncImages($request->getData());

        return $this->responseSuccess();
    }

    public function delete(History $history)
    {
        $history->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($histories)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('histories') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $histories])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}