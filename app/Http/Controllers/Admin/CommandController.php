<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CommandRequest;
use App\Models\Command;

class CommandController extends Controller
{
    protected $entity = 'commands';

    public function index()
    {
        $commands = Command::orderBy('order')->get();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($commands);

        return view('admin.' . $this->entity . '.index', compact('commands') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $command = new Command();

        return view('admin.' . $this->entity . '.edit', compact('command') + ['entity' => $this->entity]);
    }

    public function edit(Command $command)
    {
        return view('admin.' . $this->entity . '.edit', compact('command') + ['entity' => $this->entity]);
    }

    public function store(CommandRequest $request)
    {
        $command = Command::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        $command->syncImages($request->getData());

        return $this->responseSuccess();
    }

    public function delete(Command $command)
    {
        $command->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($commands)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('commands') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $commands])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}