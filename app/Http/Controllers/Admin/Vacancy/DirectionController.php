<?php

namespace App\Http\Controllers\Admin\Vacancy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vacancy\DirectionRequest;
use App\Models\Vacancy\Direction;

class DirectionController extends Controller
{
    protected $entity = 'vacancies.directions';

    public function index()
    {
        $directions = Direction::withCount('vacancies')->latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($directions);

        return view('admin.' . $this->entity . '.index', compact('directions') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $direction = new Direction();

        return view('admin.' . $this->entity . '.edit', compact('direction') + ['entity' => $this->entity]);
    }

    public function edit(Direction $direction)
    {
        return view('admin.' . $this->entity . '.edit', compact('direction') + ['entity' => $this->entity]);
    }

    public function store(DirectionRequest $request)
    {
        $direction = Direction::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        return $this->responseSuccess();
    }

    public function delete(Direction $direction)
    {
        $direction->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($directions)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('directions') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $directions])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}