<?php

namespace App\Http\Controllers\Admin\Vacancy;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vacancy\VacancyRequest;
use App\Models\Vacancy\Vacancy;
use App\Services\SelectService;

class VacancyController extends Controller
{
    protected $entity = 'vacancies';

    public function index()
    {
        $vacancies = Vacancy::filter(request()->all())->with('direction')->latest()->paginate();

        if (request()->ajax() && !request('_pjax')) return $this->ajaxView($vacancies);

        return view('admin.' . $this->entity . '.index', compact('vacancies') + ['entity' => $this->entity]);
    }

    public function create()
    {
        $vacancy = new Vacancy();
        $directions = SelectService::directions();
        $commands = SelectService::commands();

        return view('admin.' . $this->entity . '.edit', compact('vacancy', 'directions', 'commands') + ['entity' => $this->entity]);
    }

    public function edit(Vacancy $vacancy)
    {
        $directions = SelectService::directions();
        $commands = SelectService::commands();

        return view('admin.' . $this->entity . '.edit', compact('vacancy', 'directions', 'commands') + ['entity' => $this->entity]);
    }

    public function store(VacancyRequest $request)
    {
        $vacancy = Vacancy::updateOrCreate(
            ['id' => $request->input('id')],
            $request->getData()
        );

        $vacancy->syncImages($request->getData());

        return $this->responseSuccess();
    }

    public function delete(Vacancy $vacancy)
    {
        $vacancy->delete();

        return $this->responseSuccess();
    }

    protected function ajaxView($vacancies)
    {
        return response([
            'view' => view('admin.' . $this->entity . '.index.table', compact('vacancies') + ['entity' => $this->entity])->render(),
            'pagination' => view('admin.partials.pagination', ['paginator' => $vacancies])->render(),
        ])->header('Cache-Control', 'no-cache, no-store');
    }
}