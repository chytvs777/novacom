<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function reorder()
    {
        $data = request('data');
        $model = request('class');
        foreach ($data as $item) {
            $model::find($item['id'])->update(['order' => $item['order']]);
        }
        return $this->responseSuccess();
    }
}