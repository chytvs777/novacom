<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($result = 'success', $data = [])
    {
        return ['result' => $result] + $data;
    }

    public function responseSuccess($data = [])
    {
        return $this->response('success', $data);
    }

    public function responseError($data = [])
    {
        return $this->response('error', $data);
    }
}
