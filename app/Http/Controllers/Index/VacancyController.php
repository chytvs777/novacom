<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Http\Requests\Index\VacancyFormRequest;
use App\Models\Setting\Country;
use App\Models\Setting\Setting;
use App\Models\Text;
use App\Models\User;
use App\Models\Vacancy\Direction;
use App\Models\Vacancy\Vacancy;
use App\Notifications\VacancyFormNotification;
use App\Services\SelectService;

class VacancyController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::filter(request()->all())->latest()->get();
        $directionsActive = Direction::whereIn('id', [request('direction_id')])->get();

        if (request('is_ajax')) {
            return ['view' => view('index.vacancies.partials.content', compact('directionsActive', 'vacancies'))->render()];
        }

        $text = Text::where('key', Text::KEY_VACANCIES)->first();
        $directions = SelectService::directions();

        return view('index.vacancies.index', compact('vacancies', 'text', 'directions', 'directionsActive'));
    }

    public function show(Vacancy $vacancy)
    {
        $vacancies = Vacancy::filter(['direction_id' => $vacancy->direction_id])
            ->where('id', '!=', $vacancy->id)
            ->latest()
            ->get();

        return view('index.vacancies.show', compact('vacancy', 'vacancies'));
    }

    public function applyNow()
    {
        $text = Text::where('key', Text::KEY_APPLY_NOW)->first();
        $countries = SelectService::countries();
        $countryId = $countries->keys()->slice(1)->first();
        $cities = SelectService::cities($countryId);

        return view('index.vacancies.apply-now', compact('text', 'countries', 'cities'));
    }

    public function form(VacancyFormRequest $request)
    {
        $email = Setting::first()->email_for_notification;

        if ($email) {
            (new User(['email' => $email]))
                ->notify(new VacancyFormNotification($request->getData()));
        }

        return $this->responseSuccess(['message' => __('index.messages.success')]);
    }

    public function getCities(Country $country)
    {
        return SelectService::cities($country->id);
    }
}