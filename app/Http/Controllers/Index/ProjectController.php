<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Models\Project\Option;
use App\Models\Project\Project;
use App\Models\Text;
use App\Services\SelectService;

class ProjectController extends Controller
{
    public function index()
    {
        $text = Text::where('key', Text::KEY_PROJECTS)->first();
        $optionsGroup = SelectService::optionsGroup();

        return view('index.projects.index', compact('text', 'optionsGroup'));
    }

    public function solutions()
    {
        $projects = Project::filter(request()->all())->latest()->get();
        $optionsActive = Option::whereIn('id', request('options', []))->get();

        if (count(request('options', []))) {
            session(['options' => request('options')]);
        }

        if (request('is_ajax')) {
            return ['view' => view('index.projects.solutions.content', compact('optionsActive', 'projects'))->render()];
        }

        $text = Text::where('key', Text::KEY_PROJECT_SOLUTIONS)->first();
        $optionsGroup = SelectService::optionsGroup(true);

        return view('index.projects.solutions', compact('projects', 'text', 'optionsGroup', 'optionsActive'));
    }

    public function show(Project $project)
    {
        $projects = Project::where('id', '!=', $project->id)
            ->filter(['options' => session('options', [])])
            ->latest()
            ->take(6)
            ->get();

        return view('index.projects.show', compact('project', 'projects'));
    }
}