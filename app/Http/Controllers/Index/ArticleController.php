<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Models\Article\Article;
use App\Models\Command;
use App\Models\Text;

class ArticleController extends Controller
{
    protected $perPage = 6;

    public function index()
    {
        $articles = Article::query();

        $page = request('page', 1);
        $paginator = $articles->count() > $page * $this->perPage ? true : false;

        $articles = $articles
            ->with('mainImage')
            ->latest()
            ->skip(request('is_ajax') ? (($page - 1) * $this->perPage) : 0)
            ->take(request('is_ajax') ? $this->perPage : $page * $this->perPage)
            ->get();

        if (!$articles->count() && request('page') || request('page') == 1) return redirect(request()->url(), 301);

        if (request()->ajax()) {
            return response()->json([
                'result' => 'success',
                'view_content' =>  view('index.articles.partials.content', compact('articles'))->render(),
                'view_paginator' =>  view('index.articles.partials.paginator', compact('articleCategory', 'page', 'paginator'))->render(),
            ])->header('Cache-Control', 'no-cache, no-store');
        }

        $command = Command::whereIsShowNews(1)->first();
        $text = Text::where('key', Text::KEY_ARTICLES)->first();

        return view('index.articles.index', compact('articles', 'paginator', 'page', 'command', 'text'));
    }

    public function show(Article $article)
    {
        $articlesLast = Article::where('id', '!=', $article->id)->latest()->take(2)->get();

        return view('index.articles.show', compact('article', 'articlesLast'));
    }
}