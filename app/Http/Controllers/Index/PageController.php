<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Models\Command;
use App\Models\History;
use App\Models\Project\Project;
use App\Models\Text;
use App\Models\Vacancy\Vacancy;

class PageController extends Controller
{
    public function about()
    {
        $text = Text::where('key', Text::KEY_ABOUT)->first();
        $projects = Project::has('customerImage')
            ->with('customerImage')
            ->get()
            ->sortBy('customer_type')
            ->groupBy('customer_type');

        return view('index.pages.about', compact('text', 'projects'));
    }

    public function leadership()
    {
        $commands = Command::whereIsLeadership(1)->with('mainImage')->get();
        $text = Text::where('key', Text::KEY_LEADERSHIP)->first();

        return view('index.pages.leadership', compact('commands', 'text'));
    }

    public function history()
    {
        $histories = History::has('mainImage')->with('mainImage')->orderBy('order')->get();
        $text = Text::where('key', Text::KEY_HISTORY)->first();

        return view('index.pages.history', compact('histories', 'text'));
    }

    public function work()
    {
        $vacancies = Vacancy::whereIsHot(1)->with('direction')->get();
        $commands = Command::whereIsReview(1)->with('mainImage')->get();
        $text = Text::where('key', Text::KEY_WORK)->first();

        return view('index.pages.work', compact('vacancies', 'text', 'commands'));
    }

    public function contacts()
    {
        $text = Text::where('key', Text::KEY_CONTACTS)->first();

        return view('index.pages.contacts', compact('text'));
    }


    public function career()
    {
        return redirect(route('index.work'), 301);
    }

    public function company()
    {
        return redirect(route('index.about'), 301);
    }
}