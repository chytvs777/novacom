<?php

namespace App\Http\Controllers\Index;

use App\Http\Controllers\Controller;
use App\Http\Requests\Index\SendFormRequest;
use App\Models\Article\Article;
use App\Models\Project\Project;
use App\Models\Setting\Setting;
use App\Models\Text;
use App\Models\User;
use App\Notifications\SendFormNotification;

class HomeController extends Controller
{
    public function index()
    {
        $text = Text::where('key', Text::KEY_HOME)->first();
        $articles = Article::latest()->take(5)->get();
        $projects = Project::whereIsReview(1)->latest()->take(8)->with('customerImage')->get();

        return view('index.home.index', compact('articles', 'text', 'projects'));
    }

    public function sendForm(SendFormRequest $request)
    {
        $email = Setting::first()->email_for_notification;

        if ($email) {
            (new User(['email' => $email]))
                ->notify(new SendFormNotification($request->all()));
        }

        return $this->responseSuccess(['message' => __('index.messages.success')]);
    }
}