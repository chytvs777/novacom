<?php

namespace App\Observers;

use App\Models\History;

class HistoryObserver
{
	public function creating(History $history)
	{
        $history->order = History::max('order') + 1;
	}
}