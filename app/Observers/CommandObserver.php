<?php

namespace App\Observers;

use App\Models\Command;

class CommandObserver
{
	public function creating(Command $command)
	{
        $command->order = Command::max('order') + 1;
	}
}