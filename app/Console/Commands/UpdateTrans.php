<?php

namespace App\Console\Commands;

use App\Models\Product\Brand;
use App\Models\Product\Category;
use App\Models\Product\Product;
use App\Services\ApiService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Spatie\TranslationLoader\LanguageLine;

class UpdateTrans extends Command
{
    protected $signature = 'update:trans';

    public function handle()
    {
        app()->setLocale('ru');

        $transFile = array_keys(array_dot(trans('index_file')));
        $transDb = LanguageLine::where('group', 'index')->pluck('key')->toArray();
        $diff = array_diff($transFile, $transDb);

        foreach ($diff as $key) {
            $text = [];
            foreach (\LaravelLocalization::getSupportedLocales() as $lang => $localization) {
                app()->setLocale($lang);
                $text[$lang] = trans('index_file.' . $key);
            }

            LanguageLine::create([
                'group' => 'index',
                'key' => $key,
                'text' => $text,
            ]);
        }

        $this->comment('Add '. count($diff). ' translate');

    }
}
