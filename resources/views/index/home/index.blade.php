@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')
    <div class="index-prewiews">
        <div class="index-prewiews__item">
            <div class="index-prewiews__item-inner bg-layer" style="background-image: url({{ asset('assets/index/images/index-banner1.jpg') }});">
                <div class="index-prewiews__inner">
                    <div class="index-prewiews__header">заказная разработка <br>  информационных систем</div><a class="index-prewiews__btn btn btn--white" href="#">@lang('index.pages.home.learn_more')</a>
                </div>
            </div>
        </div>
        <div class="index-prewiews__item">
            <div class="index-prewiews__item-inner bg-layer" style="background-image: url({{ asset('assets/index/images/index-banner2.jpg') }});">
                <div class="index-prewiews__inner">
                    <div class="index-prewiews__header">промышленный <br> интернет вещей</div><a class="index-prewiews__btn btn btn--white" href="#">@lang('index.pages.home.learn_more')</a>
                </div>
            </div>
        </div>
        <div class="index-prewiews__item">
            <div class="index-prewiews__item-inner bg-layer" style="background-image: url({{ asset('assets/index/images/index-banner3.jpg') }});">
                <div class="index-prewiews__inner">
                    <div class="index-prewiews__header">электронное <br> взаимодействие</div><a class="index-prewiews__btn btn btn--white" href="#">@lang('index.pages.home.learn_more')</a>
                </div>
            </div>
        </div>
        <div class="index-prewiews__item">
            <div class="index-prewiews__item-inner bg-layer" style="background-image: url({{ asset('assets/index/images/index-banner4.jpg') }});">
                <div class="index-prewiews__inner">
                    <div class="index-prewiews__header">Электронная <br> коммерция </div><a class="index-prewiews__btn btn btn--white" href="#">@lang('index.pages.home.learn_more')</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')


    <section class="section g-about" style="background-image: url({{ asset('assets/index/images/shadow__heading-about.png') }});">
        <div class="box">
            <div class="grid">
                <div class="grid__cell">
                    @if(isset($text->data['box_one_title']))
                        <div class="heading heading--g-companu">{{ $text->data['box_one_title'] }}</div>
                    @endif
                    @if(isset($text->data['box_one_description']))
                        <div class="sect-title">
                            @textarea($text->data['box_one_description'])
                        </div>
                    @endif
                </div>
                <div class="grid__cell">
                    <div class="sect-list">
                        <div class="sect-list__header">@lang('index.pages.home.we_specialize_in')</div>
                        @if(isset($text->data['box_one']) && count($text->data['box_one']))
                            <ul class="sect-list__list">
                                @foreach($text->data['box_one'] as $item)
                                    <li class="sect-list__item">{{ $item['title'] }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
            <div class="info-companu">
                @include('index.partials.widgets.advantages')
                <div class="btn-inner"><a class="btn info-companu__btn" href="{{ route('index.about') }}">@lang('index.pages.home.learn_more')</a></div>
            </div>
        </div>
    </section>

    @if($articles->count())
        <div class="small-news-slider owl-carousel">
            @foreach($articles as $article)
                <a class="small-news" href="{{ $article->url }}">
                    <div class="box">
                        <div class="small-news__header">{{ $article->title }}</div>
                    </div>
                </a>
            @endforeach
        </div>
    @endif

    <section class="section g-technologies section--bg-gray" style="background-image: url({{ asset('assets/index/images/shadow__heading-technology.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.home.technologies.title')</div>
            <div>
                <div class="g-technologies__inner">
                    <div class="grid">
                        <div class="grid__cell">
                            <div class="g-technologies__content">@lang('index.pages.home.technologies.text_one')</div>
                        </div>
                        <div class="grid__cell">
                            <div class="techn-images g-technologies__images">
                                @foreach($setting->technologiesOne as $image)
                                    <div class="techn-images__item"><div class="techn-images__link"><img src="{{ $image->load('small', $setting) }}" alt="{{ $image->title }}"></div></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-technologies__inner">
                    <div class="grid">
                        <div class="grid__cell">
                            <div class="g-technologies__content">@lang('index.pages.home.technologies.text_two')</div>
                        </div>
                        <div class="grid__cell">
                            <div class="techn-images g-technologies__images">
                                @foreach($setting->technologiesTwo as $image)
                                    <div class="techn-images__item"><div class="techn-images__link"><img src="{{ $image->load('small', $setting) }}" alt="{{ $image->title }}"></div></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-inner"><a class="btn info-companu__btn" href="{{ route('index.projects.index') }}">@lang('index.pages.home.technologies.more')</a></div>
        </div>
    </section>

    @if($projects->count())
        <section class="section g-otzivi section--bg-red" style="background-image: url({{ asset('assets/index/images/shadow__heading-reviews.png') }});">
            <div class="box">
                <div class="heading">@lang('index.pages.home.reviews_clients.title')</div>
                <div class="g-otzivi__slider">
                    <div class="slider-otziv">
                        <div class="owl-carousel slider-otziv__inner">
                            @foreach($projects as $project)
                                <div class="slider-otziv__item">
                                    <div class="slider-otziv__data">
                                        @if($project->customerImage)
                                            <div class="slider-otziv__images"><img src="{{ $project->customerImage->load('', $project) }}" alt="{{ $project->customerImage->title }}"></div>
                                        @endif
                                        <div class="slider-otziv__header">{{ $project->review_name }}</div>
                                        <div class="slider-otziv__tittle">{{ $project->review_position }}</div>
                                        <div class="slider-otziv__btn"><a class="btn btn--black slider-otziv__btn" href="{{ $project->url }}">@lang('index.pages.home.reviews_clients.show_project')</a></div>
                                    </div>
                                    <div class="slider-otziv__content">
                                        @textarea($project->review_text)
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                {{--<div class="btn-inner"><a class="g-otzivi__btn" href="#">больше отзывов</a></div>--}}
            </div>
        </section>
    @endif

    <section class="section g-klients" style="background-image: url({{ asset('assets/index/images/shadow__heading-clients.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.home.our_clients.title')</div>
            {{--<div class="g-klients__title">@lang('index.pages.home.our_clients.description')</div>--}}
            <div class="g-klients__slider">
                <div class="slid-clients">
                    <div class="slid-clients__inner owl-carousel">
                        @foreach($setting->clients as $image)
                            <div class="slid-clients__item"><img src="{{ $image->load('small', $setting) }}" alt="{{ $image->title }}"></div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="btn-inner"><a class="btn btn--black" href="{{ route('index.projects.index') }}">@lang('index.pages.home.technologies.more')</a></div>
        </div>
    </section>

    @if(isset($text->data['box_two_title']))
        <section class="section part-of-team bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/part-of-team.png') }});">
            <div class="section__inner">
                <div class="box">
                    <div class="heading">{!! $text->data['box_two_title'] !!}</div>
                    @if(isset($text->data['box_two_description']))
                        <div class="part-of-team__tittle">
                            @textarea($text->data['box_two_description'])
                        </div>
                    @endif
                    @if(isset($text->data['box_two']) && count($text->data['box_two']))
                        <ul class="part-of-team__list">
                            @foreach($text->data['box_two'] as $item)
                                <li class="part-of-team__item">
                                    <div class="part-of-team__header">{{ $item['title'] }}</div>
                                    <div class="part-of-team__small-header">{{ $item['description'] }}</div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                    <div class="btn-inner"><a class="btn btn--color-white" href="{{ route('index.vacancies.index') }}">@lang('index.pages.home.vacancies.show')</a></div>
                </div>
            </div>
        </section>
    @endif

    @include('index.partials.footer.application')

@endsection