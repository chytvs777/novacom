@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    <div class="prewiews bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/bg-p-vacancy.jpg') }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $text->title }}</h1>
            <div class="prewiews__content ">{!! $text->content_short !!}</div>
        </div>
    </div>
@endsection

@section('content')

    <div class="form-search">
        <div class="box js_filter js_filter--vacancies">
            {!! Form::open(['class' => 'form-search__inner form-search__inner--padding js_filter__form js_filter__form--vacancies']) !!}
                <div class="form-search__item">
                    @include('index.partials.forms.partials.select', [
                        'name' => 'direction_id',
                        'options' => $directions,
                    ])
                </div>
                <div class="form-search__item">
                    @include('index.partials.forms.partials.text', ['name' => 'keyword'])
                </div>
                <div class="form-search__item">
                    @include('index.partials.forms.partials.btn', ['name' => 'search'])
                </div>
            {!! Form::close() !!}
            <div class="js_filter__wrap">
                @include('index.vacancies.partials.content')
            </div>

        </div>
    </div>
    <section class="section section--bg-red g-help" style="background-image: url({{ asset('assets/index/images/shadow__heading-help.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.career.vacancies.did_not_found.title')</div>
            <div class="g-help__small-heading">@lang('index.pages.career.vacancies.did_not_found.description')</div>
            <div class="btn-inner"><a class="btn btn--white help-form__btn" href="{{ route('index.vacancies.applyNow') }}">@lang('index.pages.career.vacancies.did_not_found.btn')</a></div>
        </div>
    </section>

@endsection