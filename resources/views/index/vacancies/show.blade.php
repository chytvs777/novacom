@extends('index.layouts.main')

@section('title', $vacancy->generateMetaTitle)
@section('meta_description', $vacancy->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    @php($poster = $vacancy->mainImage ? $vacancy->mainImage->load('poster', $vacancy) : asset('assets/index/images/bg-p-open-vacancy.jpg'))

    <div class="prewiews bg-layer bg-cover prewiews--open-vacancy" style="background-image: url({{ $poster }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $vacancy->title }}</h1>
            <div class="prewiews__content">{!! $vacancy->content_short !!}</div>
        </div>
    </div>
@endsection

@section('content')

    @if(isset($vacancy->data['box_one_title']))
        <section class="section def-list">
            <div class="section__inner">
                <div class="box">
                    <div class="def-list__header">{{ $vacancy->data['box_one_title'] }}</div>
                    @if(isset($vacancy->data['box_one']) && count($vacancy->data['box_one']))
                        <ul class="def-list__list">
                            @foreach($vacancy->data['box_one'] as $item)
                                <li class="def-list__item">{{ $item['title'] }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </section>
    @endif

    <section class="section demands-list section--bg-red">
        <div class="section__inner">
            <div class="box">
                @if($vacancy->content_title)
                    <div class="demands-list__header">{{ $vacancy->content_title }}</div>
                @endif
                @if($vacancy->content_sub_title)
                    <div class="demands-list__small-header">{{ $vacancy->content_sub_title }}</div>
                @endif
                <div class="content content--demands-list">
                    @include('index.partials.content', ['item' => $vacancy])
                </div>
            </div>
        </div>
    </section>

    @if(isset($vacancy->data['box_two_title']))
        <section class="section def-list">
            <div class="section__inner">
                <div class="box">
                    <div class="def-list__header">{{ $vacancy->data['box_two_title'] }}</div>
                    @if(isset($vacancy->data['box_two']) && count($vacancy->data['box_two']))
                        <ul class="def-list__list">
                            @foreach($vacancy->data['box_two'] as $item)
                                <li class="def-list__item">{{ $item['title'] }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </section>
    @endif

    @if($vacancy->command)
        <section class="section contact-face section--bg-gray" style="background-image: url({{ asset('assets/index/images/shadow__heading-contact-face.png') }});">
            <div class="box">
                <div class="heading">@lang('index.pages.career.vacancies_show.contact_face')</div>
                <div class="contact-face__info-inner">
                    @if($vacancy->command->mainImage)
                        <div class="contact-face__images"><img src="{{ $vacancy->command->mainImage->load('news', $vacancy->command) }}" alt="{{ $vacancy->command->mainImage->title }}"></div>
                    @endif
                    <div class="contact-face__info">
                        <div class="contact-face__name">{{ $vacancy->command->full_name }}</div>
                        @if($vacancy->command->phone_home)
                            <a class="contact-face__link contact-face__link--home-tel" href="tel:{{ $vacancy->command->phone_home }}">{{ $vacancy->command->phone_home }}</a>
                        @endif
                        @if($vacancy->command->phone_mobile)
                            <a class="contact-face__link contact-face__link--tel" href="tel:{{ $vacancy->command->phone_mobile }}">{{ $vacancy->command->phone_mobile }}</a>
                        @endif
                        @if($vacancy->command->email)
                            <a class="contact-face__link contact-face__link--mail" href="mailto:{{ $vacancy->command->email }}">{{ $vacancy->command->email }}</a>
                        @endif
                    </div>
                    <div class="contact-face__btn-inner">
                        <a class="btn contact-face__btn" href="{{ route('index.vacancies.applyNow', ['vacancy_id' => $vacancy->id]) }}#send">@lang('index.pages.career.vacancies_show.respond_to_vacancies')</a>
                        <a class="btn btn--green contact-face__btn" href="#">@lang('index.pages.career.vacancies_show.apply_with_linked_in')</a>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if($vacancies->count())
        <section class="section section--similar-vacancy">
            <div class="box">
                <div class="heading">@lang('index.pages.career.vacancies_show.similar_vacancies')</div>
                <div class="hot-vacancy hot-vacancy--white">
                    @foreach($vacancies as $vacancy)
                        @include('index.partials.cell.vacancy')
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    @include('index.partials.footer.application')

@endsection