<div class="form-search__result">
    @foreach($directionsActive as $directionActive)
        <div class="form-search__result-item">
            <span class="form-search__result-close js_filter__remove-option" data-name="direction_id">x</span>
            <span>{{ $directionActive->title }}</span>
        </div>
    @endforeach
</div>
<div class="result-search">
    <div class="result-search__heading">@lang('index.pages.career.vacancies.in_the_list_found') {{ $vacancies->count() }} {{ trans_choice('chosen.vacancies', $vacancies->count()) }}</div>
    <div class="hot-vacancy hot-vacancy--white">
        @foreach($vacancies as $vacancy)
            <div class="hot-vacancy__item {{ $vacancy->is_hot ? 'hot-vacancy__item--hot-status' : '' }}">
                <div class="hot-vacancy__name">{{ $vacancy->title }}</div>
                <div class="hot-vacancy__value">{{ $vacancy->direction->title }}</div>
                <div class="hot-vacancy__btn-inner">
                    <a class="hot-vacancy__btn" href="{{ $vacancy->url }}">подробнее о вакансии</a>
                </div>
            </div>
        @endforeach
    </div>
</div>