<div class="form-search__result">
    @foreach($directionsActive as $directionActive)
        <div class="form-search__result-item">
            <span class="form-search__result-close js_filter__remove-option" data-type="direction_id">x</span>
            <span>{{ $directionActive->title }}</span>
        </div>
    @endforeach
</div>
<div class="result-search">
    <div class="result-search__heading">@lang('index.pages.career.vacancies.in_the_list_found') {{ $vacancies->count() }} {{ trans_choice('chosen.vacancies', $vacancies->count()) }}</div>
    <div class="hot-vacancy hot-vacancy--white">
        @foreach($vacancies as $vacancy)
            @include('index.partials.cell.vacancy')
        @endforeach
    </div>
</div>