@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')

    <div class="prewiews bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/bg-p-rezume.jpg') }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $text->title }}</h1>
            <div class="prewiews__content">{{ $text->content_short }}</div>
        </div>
    </div>
@endsection

@section('content')

    <section id="send" class="section p-resume" style="background-image: url({{ asset('assets/index/images/shadow__heading-resume.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.career.apply_now.title')</div>
            <div class="p-resume__inner">
                <div class="grid">
                    <div class="grid__cell">
                        <div class="g-help__inner">
                            {!! Form::open(['route' => 'index.vacancies.form', 'class' => 'def-form js_form js_form--vacancy', 'files' => true, 'data-url-cities' => route('index.vacancies.getCities')]) !!}
                            {!! Form::hidden('vacancy_id', request('vacancy_id')) !!}
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.text', ['name' => 'first_name'])
                                    @include('index.partials.forms.partials.text', ['name' => 'last_name'])
                                </div>
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.text', ['name' => 'email'])
                                    @include('index.partials.forms.partials.text', ['name' => 'phone'])
                                </div>
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.select', [
                                        'name' => 'country_id',
                                        'options' => $countries,
                                    ])
                                    @include('index.partials.forms.partials.select', [
                                        'name' => 'city_id',
                                        'options' => $cities,
                                    ])
                                </div>
                                <div class="def-form__row">
                                    <div class="def-form__group def-form__group--file js_form__wrap">
                                        <div class="def-form__file-inner">
                                            <input class="def-form__file-input" name="file" type="file" id="file">
                                            <label class="def-form__file-label" for="file">@lang("index.elements.fields.file.placeholder")</label>
                                        </div>
                                        <div class="def-form__helper js_form__error"></div>
                                        {{--<div class="def-form__file-upload">resume.txt</div>--}}
                                    </div>
                                </div>
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.textarea', ['name' => 'message'])
                                </div>
                                <div class="btn-inner"><button class="btn" type="submit">@lang('index.elements.buttons.send')</button></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="grid__cell">
                        <div class="btn-inner btn-inner--right"><a class="btn btn--green" href="#">использовать linkedin</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection