@extends('index.root')

@section('main')

    <div class="wrapper">
        @include('index.partials.header')
        @yield('previews')
        <main class="main">
            @yield('content')
        </main>
        @include('index.partials.footer')
    </div>

@stop