<!doctype html>
<html class="no-js page" lang="{{ \LaravelLocalization::getCurrentLocale() }}">
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('meta_description')" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" content="notranslate">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/index/css/style.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;amp;subset=cyrillic">

    @yield('meta_og')
</head>

<body class="@yield('body_class')">

    @yield('main')

    <script src="{{ asset('assets/index/js/main.js') }}"></script>
    <script src="{{ elixir('assets/index/js/app.js') }}"></script>

    @stack('scripts')
    @stack('popups')

    @include('index.partials.metrics.yandex')
    @include('index.partials.metrics.google')

    <script>
        window.error_message = '{{ __('index.messages.error') }}'
    </script>

    <!-- Browser update message -->
    <script>
        var $buoop = {vs:{i:10,f:-8,o:-6,s:7,c:-8},api:4};
        function $buo_f(){
            var e = document.createElement("script");
            e.src = "//browser-update.org/update.min.js";
            document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
    </script>

</body>
</html>
