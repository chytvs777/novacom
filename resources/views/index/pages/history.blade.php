@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')


    <div class="prewiews bg-layer bg-cover history">
        <div class="breadcrumb breadcrumb--history">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner prewiews__inner--history-slide">
            <h1 class="heading heading--history">{{ $text->title }}</h1>
            <div class="history__inner">
                <div class="history__body">
                    @foreach($histories as $history)
                        <div class="history__item">
                            <div class="history-slide__images" style="background-image: url({{ $history->mainImage->load('poster', $history) }});" ></div>
                            <div class="history-slide__data">{{ $history->years }}</div>
                            <div class="history-slide__content">{{ $history->content }}</div>
                        </div>
                    @endforeach
                </div>
                <div class="history__thumbs">
                    @foreach($histories as $history)
                        <div class="thumbs__item">{{ $history->years }}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@section('content')
    @include('index.partials.footer.application')
@endsection