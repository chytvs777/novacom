@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')
    <div class="prewiews">
        <div class="breadcrumb breadcrumb--bg-gray">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>
@endsection

@section('content')
    <section class="section lidership" style="background-image: url({{ asset('assets/index/images/shadow__heading-lidership.png') }});">
        <div class="box">
            <div class="heading">{{ $text->title }}</div>
            <div class="lidership__title">{{ $text->content_short }}</div>
            <div class="lidership__inner">
                @foreach($commands as $command)
                    <div class="lidership__item">
                        @if($command->mainImage)
                            <div class="lidership__avatar">
                                <img src="{{ $command->mainImage->load('leadership', $command) }}" alt="{{ $command->mainImage->title }}">
                            </div>
                        @endif
                        <div class="lidership__data">
                            <div class="lidership__header">{{ $command->full_name }}</div>
                            @if($command->position)
                                <div class="lidership__position">{{ $command->position }}</div>
                            @endif
                            <div class="lidership__content">@textarea($command->content)</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    @include('index.partials.footer.application')
@endsection