@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')
    <div class="prewiews bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/bg-about-companu.jpg') }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $text->title }}</h1>
            <div class="prewiews__content prewiews__content--about">{{ $text->content_short }}</div>
        </div>
    </div>
@endsection

@section('content')

    @if(isset($text->data['box_one']) && count($text->data['box_one']))
        <section class="section">
            <div class="box">
                <div class="info-about-companu">
                    <div class="info-about-companu__inner">
                        @foreach($text->data['box_one'] as $item)
                            <div class="info-companu__item">
                                @if(isset($item['icon']) && $item['icon'] && isset($item['title']))
                                    <div class="info-companu__images-inner">
                                        <div class="info-companu__first-circle show-circle">
                                            <div class="info-companu__second-circle"><img src="../assets/index/images/{{ $item['icon'] }}" alt="{{ $item['title'] }}"></div>
                                        </div>
                                    </div>
                                @endif
                                <div class="info-companu__data">
                                    @if(isset($item['title']) && $item['title'])
                                        <div class="info-companu__header">{{ $item['title'] }}</div>
                                    @endif
                                    @if(isset($item['description']) && $item['description'])
                                        <div class="info-companu__title">@textarea($item['description'])</div>
                                    @endif

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="section participation-assoc section--bg-gray" style="background-image: url({{ asset('assets/index/images/shadow__heading-assn.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.company.about.participation_in_associations.title')</div>
            <div class="participation-assoc__inner">
                <div class="participation-assoc__item">
                    @lang('index.pages.company.about.participation_in_associations.one')
                </div>
                <div class="participation-assoc__item">
                    @lang('index.pages.company.about.participation_in_associations.two')
                </div>
                <div class="participation-assoc__item">
                    @lang('index.pages.company.about.participation_in_associations.tree')
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="box">
            <div class="info-companu">
                @include('index.partials.widgets.advantages')
                {{--<div class="btn-inner"><a class="btn info-companu__btn" href="#">узнать больше</a></div>--}}
            </div>
        </div>
    </section>

    <section class="section section--bg-red team-project bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/bg-team-project.jpg') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.company.about.in_the_project_team.title')</div>
            <div class="team-project__inner">
                <div class="team-project__item">
                    <div class="team-project__images"><img src="{{ asset('assets/index/images/ico--team-project1.png') }}" alt="@lang('index.pages.company.about.in_the_project_team.one')"></div>
                    <div class="team-project__content"> @lang('index.pages.company.about.in_the_project_team.one')</div>
                </div>
                <div class="team-project__item">
                    <div class="team-project__images"><img src="{{ asset('assets/index/images/ico--team-project2.png') }}" alt="('index.pages.company.about.in_the_project_team.two')"></div>
                    <div class="team-project__content"> @lang('index.pages.company.about.in_the_project_team.two')</div>
                </div>
                <div class="team-project__item">
                    <div class="team-project__images"><img src="{{ asset('assets/index/images/ico--team-project3.png') }}" alt="@lang('index.pages.company.about.in_the_project_team.tree')"></div>
                    <div class="team-project__content"> @lang('index.pages.company.about.in_the_project_team.tree')</div>
                </div>
                <div class="team-project__item">
                    <div class="team-project__images"><img src="{{ asset('assets/index/images/ico--team-project4.png') }}" alt="@lang('index.pages.company.about.in_the_project_team.four')"></div>
                    <div class="team-project__content"> @lang('index.pages.company.about.in_the_project_team.four')</div>
                </div>
                <div class="team-project__item">
                    <div class="team-project__images"><img src="{{ asset('assets/index/images/ico--team-project5.png') }}" alt=" @lang('index.pages.company.about.in_the_project_team.five')"></div>
                    <div class="team-project__content"> @lang('index.pages.company.about.in_the_project_team.five')</div>
                </div>
            </div>
        </div>
    </section>

    <section class="section red-reward section--bg-red" style="background-image: url({{ asset('assets/index/images/shadow__heading-reward-red.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.company.about.rewards.title')</div>
            <div class="red-reward__inner">
                <div class="red-reward__item">
                    <div class="red-reward__images"><img src="{{ asset('assets/index/images/ico-red-reward1.png') }}" alt="@lang('index.pages.company.about.rewards.one.title')"></div>
                    <div class="red-reward__data">
                        <div class="red-reward__heading"> @lang('index.pages.company.about.rewards.one.title')</div>
                        <div class="red-reward__content">
                            @lang('index.pages.company.about.rewards.one.text')
                        </div>
                    </div>
                </div>
                <div class="red-reward__item">
                    <div class="red-reward__images"><img src="{{ asset('assets/index/images/ico-red-reward2.png') }}" alt="@lang('index.pages.company.about.rewards.two.title')"></div>
                    <div class="red-reward__data">
                        <div class="red-reward__heading">@lang('index.pages.company.about.rewards.two.title')</div>
                        <div class="red-reward__content">@lang('index.pages.company.about.rewards.two.text')</div>
                    </div>
                </div>
                <div class="red-reward__item">
                    <div class="red-reward__images"><img src="{{ asset('assets/index/images/ico-red-reward3.png') }}" alt="@lang('index.pages.company.about.rewards.tree.title')"></div>
                    <div class="red-reward__data">
                        <div class="red-reward__heading"> @lang('index.pages.company.about.rewards.tree.title')</div>
                        <div class="red-reward__content">
                            @lang('index.pages.company.about.rewards.tree.text')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section abaut-clients" style="background-image: url({{ asset('assets/index/images/shadow__heading-clients.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.company.about.clients.title')</div>
            <div class="abaut-clients__inner">
                <div class="tabs-clients">
                    <div class="tabs-clients__item tabs-clients__item--active">
                        <div class="tabs-clients__ico-tabs"><img src="{{ asset('assets/index/images/ico-about-clients-tabs1.png') }}" alt="@lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_GOS)"></div><span> @lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_GOS)</span>
                    </div>
                    <div class="tabs-clients__item">
                        <div class="tabs-clients__ico-tabs"><img src="{{ asset('assets/index/images/ico-about-clients-tabs2.png') }}" alt="@lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_INDUSTRY)"></div><span> @lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_INDUSTRY)</span>
                    </div>
                    <div class="tabs-clients__item">
                        <div class="tabs-clients__ico-tabs"><img src="{{ asset('assets/index/images/ico-about-clients-tabs3.png') }}" alt="@lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_BUSINESS)"></div><span> @lang('index.pages.projects.customer_types.'. \App\Models\Project\Project::CUSTOMER_TYPE_BUSINESS)</span>
                    </div>
                </div>
                <div class="page-clients">
                    @foreach($projects as $items)
                        <div class="page-clients__item {{ $loop->index == 0 ? 'page-clients__item--active' : '' }}">
                            <div class="page-clients__inner">
                                @foreach($items->take(6) as $project)
                                    <div class="abaut-clients__item">
                                        <div class="abaut-clients__images"><img src="{{ $project->customerImage->load('original', $project) }}" alt="{{ $project->customerImage->title }}"></div>
                                        <div class="abaut-clients__content">
                                            <span>{{ $project->title }}</span>
                                            <div class="btn-inner"><a class="btn btn--white" href="{{ $project->url }}">@lang('index.pages.company.about.clients.show_history')</a></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section class="section partners" style="background-image: url({{ asset('assets/index/images/shadow__heading-partners.png') }});">
        <div class="box">
            <div class="heading">@lang('index.pages.company.about.partners.title')</div>
            <div class="partners__slider">
                <div class="slid-partners">
                    <div class="slid-partners__inner owl-carousel">
                        @foreach($text->images as $image)
                            <div class="slid-partners__item"><img src="{{ $image->load('original', $text) }}" alt="{{ $image->title }}"></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('index.partials.footer.application')
@endsection