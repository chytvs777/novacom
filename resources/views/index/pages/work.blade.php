@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')
    <div class="prewiews">
        <div class="breadcrumb breadcrumb--bg-gray">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>
@endsection

@section('content')

    <section class="section b-work" style="background-image: url({{ asset('assets/index/images/shadow__heading-work.png') }});">
        <div class="box">
            <div class="heading">{{ $text->title }}</div>
            <div class="b-work__title">
                {{ $text->content_short }}
            </div>
        </div>
    </section>

    @if(isset($text->data['box_one_title']))
        <section class="section part-of-team part-of-team--p-work bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/offer-list.png') }});">
            <div class="section__inner">
                <div class="box">
                    <div class="heading">{{ $text->data['box_one_title'] }}</div>
                    @if(isset($text->data['box_one_description']))
                        <div class="part-of-team__tittle">
                            @textarea($text->data['box_one_description'])
                        </div>
                    @endif
                    @if(isset($text->data['box_one']) && count($text->data['box_one']))
                        <ul class="part-of-team__list">
                            @foreach($text->data['box_one'] as $item)
                                <li class="part-of-team__item">
                                    <div class="part-of-team__small-header">{{ $item['title'] }}</div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </section>
    @endif
    <section class="section hot-vacancy section--bg-red">
        <div class="box">
            <div class="heading">@lang('index.pages.career.work.hot')</div>
            <div class="hot-vacancy">
                @foreach($vacancies as $vacancy)
                    @include('index.partials.cell.vacancy')
                @endforeach
            </div>
        </div>
    </section>
    @if($text->images->count())
        <section class="section b-status" style="background-image: url({{ asset('assets/index/images/shadow__heading-reward.png') }});">
            <div class="box">
                <div class="heading">@lang('index.pages.career.work.statuses_and_rewards')</div>
                <div class="b-status__inner">
                    <div class="grid">
                        @foreach($text->images as $image)
                            <div class="grid__cell">
                                <div class="b-status__item">
                                    <div class="b-status__images"><img src="{{ $image->load('statuses', $text) }}" alt="status"></div>
                                    <div class="b-status__info-inner">
                                        <div class="b-status__text">{{ $image->title }}</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if($commands->count())
        <section class="section section--bg-gray otziv-personal" style="background-image: url({{ asset('assets/index/images/shadow__heading-opinios.png') }});">
            <div class="box">
                <div class="heading">@lang('index.pages.career.work.employee_feedback')</div>
                <div class="otziv-personal__inner">
                    <div class="grid">
                        @foreach($commands as $command)
                            <div class="grid__cell">
                                <div class="otziv-personal__item">
                                    @if($command->mainImage)
                                        <div class="otziv-personal__images">
                                            <img src="{{ $command->mainImage->load('news', $command) }}" alt="{{ $command->mainImage->title }}">
                                        </div>
                                    @endif
                                    <div class="otziv-personal__data">
                                        <div class="otziv-personal__text">
                                            @textarea($command->review_text)
                                        </div>
                                        <div class="otziv-personal__name">
                                            {{ $command->full_name }}
                                            @if($command->position)
                                                , {{ $command->position }}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    @if(isset($text->data['box_two_title']))
        <section class="section part-of-team bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/part-of-team.png') }});">
            <div class="section__inner">
                <div class="box">
                    <div class="heading">{!! $text->data['box_two_title'] !!}</div>
                    @if(isset($text->data['box_two_description']))
                        <div class="part-of-team__tittle">
                            @textarea($text->data['box_two_description'])
                        </div>
                    @endif
                    @if(isset($text->data['box_two']) && count($text->data['box_two']))
                        <ul class="part-of-team__list">
                            @foreach($text->data['box_two'] as $item)
                                <li class="part-of-team__item">
                                    <div class="part-of-team__header">{{ $item['title'] }}</div>
                                    <div class="part-of-team__small-header">{{ $item['description'] }}</div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </section>
    @endif
    @include('index.partials.footer.application')
@endsection