@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('previews')
    <div class="prewiews">
        <div class="breadcrumb breadcrumb--bg-gray">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>
@endsection

@section('content')

    <section class="section p-contacts" style="background-image: url({{ asset('assets/index/images/shadow__heading-contacts.png') }});">
        <div class="box">
{{--            <div class="heading">{{ $text->title }}</div>--}}
            {{--<div class="p-contacts__header-title">@textarea($text->content_short)</div>--}}
            <div class="p-contacts__inner">
                <div class="p-contacts__item">
                    <div class="p-contacts__header">@lang('index.pages.contacts.top.call_us')</div>
                    <a class="p-contacts__link-tel" href="tel:@lang('index.pages.contacts.top.phone')">@lang('index.pages.contacts.top.phone')</a>
                </div>
                <div class="p-contacts__item">
                    <div class="p-contacts__header">@lang('index.pages.contacts.top.write_us')</div>
                    <a class="p-contacts__link-mail" href="mailto:@lang('index.pages.contacts.top.email')">@lang('index.pages.contacts.top.email')</a>
                </div>
                <div class="p-contacts__item">
                    <div class="p-contacts__header">@lang('index.pages.contacts.top.come_to_office')</div>
                    <div class="p-contacts__address">@lang('index.pages.contacts.top.office_one')</div>
                    <div class="p-contacts__address">@lang('index.pages.contacts.top.office_two')</div>
                </div>
            </div>
        </div>
    </section>
    <div class="maps js-maps" id="maps"></div>
    <section class="section contacts-form">
        <div class="box">
            <div class="contacts-form__inner">
                <div class="grid">
                    <div class="grid__cell">
                        <div class="p-contacts__header">@lang("index.blocks.forms.ask_your_question.title")</div>
                        <div class="contacts-form__inner">
                            {!! Form::open(['route' => 'index.send.form', 'class' => 'def-form js_form']) !!}
                            {!! Form::hidden('url', request()->url()) !!}
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.text', ['name' => 'first_name'])
                                    @include('index.partials.forms.partials.text', ['name' => 'company_name'])
                                </div>
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.text', ['name' => 'email'])
                                    @include('index.partials.forms.partials.text', ['name' => 'phone'])
                                </div>
                                <div class="def-form__row">
                                    @include('index.partials.forms.partials.textarea', ['name' => 'message'])
                                </div>
                                <div class="btn-inner"><button class="btn" type="submit">@lang('index.elements.buttons.send')</button></div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="grid__cell contacts-form__data-cell">
                        <div class="contacts-form__data-inner">
                            <div class="p-contacts__header">@lang('index.pages.contacts.other_ways_of_communication.title')</div>
                            <div class="contacts-form__title">@lang('index.pages.contacts.other_ways_of_communication.skype')</div>
                            <div class="contacts-form__btn btn" data-popup="popup_callback">@lang('index.pages.contacts.other_ways_of_communication.submit_your_application')</div>
                            <a class="select-partners" href="@lang('index.pages.contacts.other_ways_of_communication.select_office_link')" target="_blank">@lang('index.pages.contacts.other_ways_of_communication.select_office_text')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&amp;lang=ru-RU"></script>

    @push('popups')
        @include('index.partials.popups.feedback')
    @endpush

@endsection