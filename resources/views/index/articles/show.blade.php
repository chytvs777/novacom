@extends('index.layouts.main')

@section('title', $article->generateMetaTitle)
@section('meta_description', $article->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    @if($article->mainImage)
        <meta property="og:image" content="{{ $article->mainImage->load('poster', $article) }}"/>
    @endif
@endsection

@section('previews')
    @php($poster = $article->mainImage ? $article->mainImage->load('poster', $article) : '')
    <div class="prewiews bg-cover prewiews--content bg-layer bg-cover" style="background-image: url({{ $poster }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $article->title }}</h1>
            <div class="prewiews__date">@date($article->created_at)</div>
            <div class="prewiews__content">
                {{ $article->announcement }}
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="full-news">
        <div class="box">
            <div class="grid">
                <div class="grid__cell">
                    <div class="content content--padding-right">
                        {!! $article->content !!}
                        @if($article->review_text)
                            <blockquote>
                                {{ $article->review_text }}
                                <span>{{ $article->review_author }}</span>
                            </blockquote>
                        @endif
                    </div>
                </div>
                @if($articlesLast->count())
                    <div class="grid__cell">
                        <div class="banners">
                            <div class="banners__header">Последние новости</div>
                            @foreach($articlesLast as $articleLast)
                                @include('index.partials.cell.article', [
                                    'article' => $articleLast,
                                    'type' => 'small',
                                ])
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if($article->reference_information)
        <div class="info-content">
            <div class="box">
                <div class="info-content__header">@lang('index.pages.company.articles.reference_info')</div>
                <div class="info-content__text">{!! $article->reference_information !!}</div>
            </div>
        </div>
    @endif

    @if($article->command)
        @include('index.articles.partials.command', ['command' => $article->command])
    @endif

@endsection