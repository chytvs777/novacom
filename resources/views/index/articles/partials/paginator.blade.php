<div class="btn-inner js_show--more">
    @if($paginator)
        <a class="btn btn--red" href="{{ route('index.articles.index', ['page' => $page + 1]) }}">@lang('index.pages.company.articles.more_news')</a>
    @endif
</div>