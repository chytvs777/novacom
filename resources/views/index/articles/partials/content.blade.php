@foreach($articles as $article)
    <div class="grid__cell">
        @include('index.partials.cell.article', ['type' => 'big'])
    </div>
@endforeach