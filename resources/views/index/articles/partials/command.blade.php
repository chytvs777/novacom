<section class="section contact-face section--bg-gray" style="background-image: url({{ asset('assets/index/images/shadow__heading-contact-face.png') }});">
    <div class="box">
        <div class="heading">@lang('index.pages.company.articles.contact_face')</div>
        <div class="contact-face__info-inner">
            @if($command->mainImage)
                <div class="contact-face__images">
                    <img src="{{ $command->mainImage->load('news', $command) }}" alt="{{ $command->mainImage->title }}">
                </div>
            @endif
            <div class="contact-face__info">
                <div class="contact-face__name">
                    {{ $command->full_name }}
                    @if($command->position)
                        <div class="contact-face__position">{{ $command->position }}</div>
                    @endif
                </div>
                @if($command->phone_home)
                    <a class="contact-face__link contact-face__link--home-tel" href="tel:{{ $command->phone_home }}">{{ $command->phone_home }}</a>
                @endif
                @if($command->phone_mobile)
                    <a class="contact-face__link contact-face__link--tel" href="tel:{{ $command->phone_mobile }}">{{ $command->phone_mobile }} (29) 258-97-47</a><a class="contact-face__link contact-face__link--mail" href="mailto:V.Yakuta@novacom.by">V.Yakuta@novacom.by</a>
                @endif
            </div>
        </div>
    </div>
</section>