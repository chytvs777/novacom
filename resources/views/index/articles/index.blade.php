@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    <div class="prewiews">
        <div class="breadcrumb breadcrumb--bg-gray">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>
@endsection

@section('content')

    <section class="section p-news" style="background-image: url({{ asset('assets/index/images/shadow__heading-news.png') }});">
        <div class="box">
            <div class="heading">{{ $text->title }}</div>
            <div class="grid js_wrapper--content">
                @include('index.articles.partials.content')
            </div>
            <div class="js_wrapper--paginator">
                @include('index.articles.partials.paginator')
            </div>
        </div>
    </section>
    @if($command)
        @include('index.articles.partials.command')
    @endif

@endsection