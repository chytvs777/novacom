@if(config('app.env') == 'production')
User-agent: *
Disallow: /*?
Disallow: /*&
Disallow: /admin/
Disallow: /*index.php
Disallow: /markup/
Allow: /*?page

User-agent: Yandex
Disallow: /*?
Disallow: /*&
Disallow: /admin/
Disallow: /*index.php
Disallow: /markup/
Allow: /*?page
Host: {{ config('app.url') }}/

Sitemap: {{ config('app.url') }}/sitemap.xml
@else
User-agent: *
Disallow: /

User-agent: Yandex
Disallow: /
@endif
