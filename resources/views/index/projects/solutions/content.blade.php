<div class="form-search__result">
    @foreach($optionsActive as $optionActive)
        <div class="form-search__result-item">
            <span class="form-search__result-close js_filter__remove-option" data-type="{{ $optionActive->type }}">x</span>
            <span>{{ $optionActive->title }}</span>
        </div>
    @endforeach
</div>

<div class="pr-solutions__inner">
    @foreach($projects as $project)
        @include('index.projects.partials.cell')
    @endforeach
</div>