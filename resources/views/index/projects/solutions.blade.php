@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    <div class="prewiews">
        <div class="breadcrumb breadcrumb--bg-gray">
            {!! Breadcrumbs::render() !!}
        </div>
    </div>
@endsection

@section('content')

    <section class="section pr-solutions form-search" style="background-image: url({{ asset('assets/index/images/shadow__heading-pr-solutions.png') }});">
        <div class="box js_filter">
            <div class="heading">{{ $text->title }}</div>
            <div class="form-pr-solutions">
                {!! Form::open(['class' => 'form-search__inner js_filter__form js_filter__form--projects']) !!}
                    @foreach($optionsGroup as $type => $options)
                    <div class="form-search__item">
                        @include('index.partials.forms.partials.select', [
                            'name' => 'options[]',
                            'options' => $options,
                            'label' => __("index.pages.projects.types.{$type}"),
                            'type' => $type,
                        ])
                    </div>
                    @endforeach
                {!! Form::close() !!}
            </div>

            <div class="js_filter__wrap">
                @include('index.projects.solutions.content')
            </div>
        </div>
    </section>

    @include('index.partials.footer.application')

@endsection