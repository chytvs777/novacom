@extends('index.layouts.main')

@section('title', $text->generateMetaTitle)
@section('meta_description', $text->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    <div class="prewiews bg-layer bg-cover" style="background-image: url({{ asset('assets/index/images/bg-p-project.jpg') }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $text->title }}</h1>
            <div class="prewiews__content">{{ $text->content_short }}</div>
        </div>
    </div>
@endsection

@section('content')
    <section class="section project">
        <div class="box">
            <div class="grid">
                @foreach($optionsGroup as $type => $options)
                    <div class="grid__cell">
                        <div class="project__name">{{ $type }}</div>
                        <div class="project__inner">
                            @foreach($options as $id => $optionTitle)
                                <a class="project__link" href="{{ route('index.projects.solutions.index', ['options[]' => $id]) }}">{{ $optionTitle }}</a>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('index.partials.footer.application')
@endsection