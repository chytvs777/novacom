<div class="pr-solutions__item">
    @if($project->mainImage)
        <a class="pr-solutions__images" href="{{ $project->url }}" style="background-image: url({{ $project->mainImage->load('cell', $project) }});"></a>
    @endif
    <div class="pr-solutions__data">
        <div class="pr-solutions__header">{{ $project->title }}</div>
        <div class="pr-solutions__content">{{ $project->content_short }}</div>
        <div class="btn-inner btn-inner--left"><a class="btn btn--black pr-solutions__btn" href="{{ $project->url }}">читать полностью</a></div>
    </div>
</div>