@extends('index.layouts.main')

@section('title', $project->generateMetaTitle)
@section('meta_description', $project->generateMetaDescription)

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
@endsection

@section('previews')
    @php($poster = $project->mainImage ? $project->mainImage->load('big', $project) : '')

    <div class="prewiews bg-layer bg-cover prewiews--open-vacancy" style="background-image: url({{ $poster }});">
        <div class="breadcrumb">
            {!! Breadcrumbs::render() !!}
        </div>
        <div class="prewiews__inner">
            <h1 class="heading">{{ $project->title }}</h1>
            @if(isset($project->data['box_one']) && count($project->data['box_one']))
                <ul class="heder-list">
                    @foreach($project->data['box_one'] as $item)
                        <li class="heder-list__item">{{ $item['title'] }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
@endsection

@section('content')

    @if($project->technologies->count())
        <div class="project-technolog">
            <div class="box">
                <div class="project-technolog__inner">
                    @foreach($project->technologies as $technology)
                        <div class="project-technolog__item"><img src="{{ $technology->mainImage->load('small', $technology) }}" alt="{{ $technology->mainImage->title }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($project->customer_name)
        <section class="section customer">
            <div class="section__inner">
                <div class="box">
                    <div class="customer__inner">
                        {{--<div class="customer__header">@lang('index.pages.projects.solutions.show.customer')</div>--}}
                        {{--<hr>--}}
                        <div class="customer__item">
                            @if($project->customerImage)
                                <div class="customer__images"><img src="{{ $project->customerImage->load('small', $project) }}" alt="{{ $project->customerImage->title }}"></div>
                            @endif
                            <div class="customer__data data-customer">
                                <div class="data-customer__header">{{ $project->customer_name }}</div>
                                <div class="data-customer__content">{{ $project->customer_info }}</div>
                                @if($project->customer_site)
                                    <a class="data-customer__link" target="_blank" href="{{ $project->customer_site }}">{{ $project->customer_site }}</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(isset($project->data['box_two_title']))
        <section class="section section--bg-red def-list def-list--white">
            <div class="section__inner">
                <div class="box">
                    <div class="def-list__header">{{ $project->data['box_two_title'] }}</div>
                    @if(isset($project->data['box_two']) && count($project->data['box_two']))
                        <ul class="def-list__list">
                            @foreach($project->data['box_two'] as $item)
                                <li class="def-list__item">{{ $item['title'] }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </section>
    @endif

    <section class="section section--content">
        <div class="box">
            <div class="content content--project">
                {!! $project->content !!}
            </div>
        </div>
    </section>

    @if($projects->count())
        <div class="slide-progect">
            <div class="box">
                <div class="slide-progect__header">@lang('index.pages.projects.solutions.show.next_project')</div>
                <div class="owl-carousel slide-progect__inner">
                    @foreach($projects as $item)
                        @include('index.projects.partials.cell', ['project' => $item])
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @include('index.partials.footer.application', ['formName' => 'request_a_detailed_description'])

@endsection