<div class="hot-vacancy__item {{ $vacancy->is_hot ? 'hot-vacancy__item--hot-status' : '' }}">
    <div class="hot-vacancy__name">{{ $vacancy->title }}</div>
    <div class="hot-vacancy__value">{{ $vacancy->direction->title ?? '' }}</div>
    <div class="hot-vacancy__btn-inner">
        <a class="hot-vacancy__btn" href="{{ $vacancy->url }}">@lang('index.pages.career.vacancies.more')</a>
    </div>
</div>