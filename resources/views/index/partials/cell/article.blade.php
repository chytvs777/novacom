<div class="news-card {{ $type == 'small' ? 'news-card--baner' : '' }}">
    <div class="news-card__date">
        <div class="news-card__date-inner">
            <div>{{ $article->created_at->format('d.m') }}</div>
            <div>{{ $article->created_at->format('Y') }}</div>
        </div>
    </div>
    @if($article->mainImage)
        <a href="{{ $article->url }}" class="news-card__images">
            <img src="{{ $article->mainImage->load($type, $article) }}" alt="{{ $article->mainImage->title }}">
        </a>
    @endif
    <div class="news-card__data">
        <a class="news-card__header" href="{{ $article->url }}">{{ $article->title }}</a>
        @if($type == 'big')
            <div class="news-card__content">
                {{ $article->content_short }}
            </div>
        @endif
        <a class="btn btn--black news-card__btn" href="{{ $article->url }}">@lang('index.pages.company.articles.read_completely')</a>
    </div>
</div>