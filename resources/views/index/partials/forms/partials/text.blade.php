@php($label = $label ?? $name)
@php($classType = $classType ?? 'def')

<div class="{{ $classType }}-form__group js_form__wrap">
    {!! Form::text($name, request($name), [
        'class' => "{$classType}-form__input",
        'placeholder' => __("index.elements.fields.{$label}.placeholder"),
        'data-default' => '',
    ])!!}
    <div class="{{ $classType }}-form__helper js_form__error"></div>
</div>