@php($type = $type ?? $name)
<div class="def-form__group js_form__wrap">
    @if(isset($label))
        <div class="def-form__name">{{ $label }}</div>
    @endif
    {!! Form::select($name, $options, request($name), [
        'class' => 'def-form__select',
        'data-default' => is_array($options) ? head(array_keys($options)) : $options->keys()->first(),
        'data-type' => $type,
    ]) !!}
    <div class="def-form__helper js_form__error"></div>
</div>