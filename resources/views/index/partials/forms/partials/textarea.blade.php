@php($label = $label ?? $name)
@php($classType = $classType ?? 'def')

<div class="{{ $classType }}-form__group {{ $classType }}-form__group--textarea js_form__wrap">
    {!! Form::textarea($name, request($name), [
        'class' => "{$classType}-form__textarea",
        'placeholder' => __("index.elements.fields.{$label}.placeholder"),
        'data-default' => '',
    ])!!}
    <div class="{{ $classType }}-form__helper js_form__error"></div>
</div>