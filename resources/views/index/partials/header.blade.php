<header class="header">
    <div class="box">
        <div class="header__inner">
            <div class="logo-inner"><a class="logo" href="{{ route('index.home') }}">
                <img src="{{ asset('assets/index/images/logo_Novacom.png') }}" width="211" height="55" alt="logo"></a>
                <span>@lang('index.elements.header.description')</span>
            </div>
            <div class="header__data data-hedaer">
                <a class="header-mail data-hedaer__item" href="mailto:@lang('index.elements.header.email')">@lang('index.elements.header.email')</a>
{{--                <div class="data-hedaer__sity data-hedaer__item">@lang('index.elements.header.city')</div>--}}
                <a class="header-tel data-hedaer__item" href="tel:@lang('index.elements.header.phone')">@lang('index.elements.header.phone')</a>
                <a href="{{ \LaravelLocalization::getLocalizedURL('ru') }}" class="data-hedaer__lang {{ \LaravelLocalization::getCurrentLocale() == 'ru' ? 'data-hedaer__lang--active' : '' }} data-hedaer__item">Ru</a>
                <a href="{{ \LaravelLocalization::getLocalizedURL('en') }}" class="data-hedaer__lang {{ \LaravelLocalization::getCurrentLocale() == 'en' ? 'data-hedaer__lang--active' : '' }} data-hedaer__item">En</a>
            </div>
            <div class="header__data-m m-head-data">
                <div class="m-head-data__lang"></div>
                <div class="lang-variant">
                    <a href="{{ \LaravelLocalization::getLocalizedURL('ru') }}" class="lang-variant__item {{ \LaravelLocalization::getCurrentLocale() == 'ru' ? 'lang-variant__item--active' : '' }}">русский</a>
                    <a href="{{ \LaravelLocalization::getLocalizedURL('en') }}" class="lang-variant__item {{ \LaravelLocalization::getCurrentLocale() == 'en' ? 'lang-variant__item--active' : '' }}">english</a>
                </div>
                <a class="m-head-data__message" href="mailto:@lang('index.elements.header.email')"></a>
                <div class="btn-mob">
                    <div class="btn-mob__item"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="nav-menu">
    <div class="box">
        <ul class="nav-menu__list">
            <li class="nav-menu__item">
                <a class="nav-menu__link" href="#">решения</a>
            </li>
            <li class="nav-menu__item">
                <div class="nav-menu__drop-arrow"></div>
                <a class="nav-menu__link {{ str_contains(request()->url(), route('index.projects.index')) ? 'nav-menu__link--active' : '' }}" href="{{ route('index.projects.index') }}">@lang('index.pages.projects.title')</a>
                <ul class="dropdown-menu">
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.projects.index') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.projects.index') }}">@lang('index.pages.projects.menu')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.projects.solutions.index') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.projects.solutions.index') }}">@lang('index.pages.projects.solutions.menu')</a></li>
                </ul>
            </li>
            <li class="nav-menu__item">
                <div class="nav-menu__drop-arrow"></div>
                <a class="nav-menu__link {{ str_contains(request()->url(), route('index.company')) ? 'nav-menu__link--active' : '' }}" href="{{ route('index.company') }}">@lang('index.pages.company.title')</a>
                <ul class="dropdown-menu">
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.about') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.about') }}">@lang('index.pages.company.about.title')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.history') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.history') }}">@lang('index.pages.company.history.title')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.leadership') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.leadership') }}">@lang('index.pages.company.leadership.title')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ str_contains(request()->url(), route('index.articles.index')) ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.articles.index') }}">@lang('index.pages.company.articles.title')</a></li>
                </ul>
            </li>
            <li class="nav-menu__item">
                <div class="nav-menu__drop-arrow"></div>
                <a class="nav-menu__link {{ str_contains(request()->url(), route('index.career')) ? 'nav-menu__link--active' : '' }}" href="{{ route('index.career') }}">@lang('index.pages.career.title')</a>
                <ul class="dropdown-menu">
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.work') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.work') }}">@lang('index.pages.career.work.title')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ request()->url() == route('index.vacancies.index') ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.vacancies.index') }}">@lang('index.pages.career.vacancies.title')</a></li>
                    <li class="dropdown-menu__item"><a class="dropdown-menu__link {{ str_contains(request()->url(), route('index.vacancies.applyNow')) ? 'dropdown-menu__link--active' : '' }}" href="{{ route('index.vacancies.applyNow') }}">@lang('index.pages.career.apply_now.title')</a></li>
                </ul>
            </li>
            <li class="nav-menu__item"><a class="nav-menu__link {{ request()->url() == route('index.contacts') ? 'nav-menu__link--active' : '' }}" href="{{ route('index.contacts') }}">@lang('index.pages.contacts.title')</a></li>
        </ul>
    </div>
    <div class="mob-clos-menu">
        <div class="mob-clos-menu__item1"></div>
        <div class="mob-clos-menu__item2"></div>
    </div>
</nav>