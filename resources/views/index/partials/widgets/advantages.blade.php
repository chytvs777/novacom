<div class="info-companu__inner">
    <div class="info-companu__item">
        <div class="info-companu__images-inner">
            <div class="info-companu__first-circle">
                <div class="info-companu__second-circle"><img src="{{ asset('assets/index/images/ico-diplom.png') }}" alt="@lang('index.pages.home.advantages.one.text')"></div>
            </div>
        </div>
        <div class="info-companu__header"><span class="spincrement">@lang('index.pages.home.advantages.one.number')</span>@lang('index.pages.home.advantages.one.number_text')</div>
        <div class="info-companu__title">@lang('index.pages.home.advantages.one.text')</div>
    </div>
    <div class="info-companu__item">
        <div class="info-companu__images-inner">
            <div class="info-companu__first-circle">
                <div class="info-companu__second-circle"><img src="{{ asset('assets/index/images/ico-project.png') }}" alt="@lang('index.pages.home.advantages.two.text')"></div>
            </div>
        </div>
        <div class="info-companu__header"><span class="spincrement">@lang('index.pages.home.advantages.two.number')</span>@lang('index.pages.home.advantages.two.number_text')</div>
        <div class="info-companu__title">@lang('index.pages.home.advantages.two.text')</div>
    </div>
    <div class="info-companu__item">
        <div class="info-companu__images-inner">
            <div class="info-companu__first-circle">
                <div class="info-companu__second-circle"><img src="{{ asset('assets/index/images/ico-person.png') }}" alt="@lang('index.pages.home.advantages.tree.text')"></div>
            </div>
        </div>
        <div class="info-companu__header"><span class="spincrement">@lang('index.pages.home.advantages.tree.number')</span>@lang('index.pages.home.advantages.tree.number_text')</div>
        <div class="info-companu__title">@lang('index.pages.home.advantages.tree.text')</div>
    </div>
    <div class="info-companu__item">
        <div class="info-companu__images-inner">
            <div class="info-companu__first-circle">
                <div class="info-companu__second-circle"><img src="{{ asset('assets/index/images/ico-support.png') }}" alt="@lang('index.pages.home.advantages.four.text')"></div>
            </div>
        </div>
        <div class="info-companu__header">@lang('index.pages.home.advantages.four.number_text')</div>
        <div class="info-companu__title">@lang('index.pages.home.advantages.four.text')</div>
    </div>
</div>