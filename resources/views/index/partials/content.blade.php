@if($item->content_one && $item->content_two)
    <div class="grid">
        <div class="grid__cell">
            {!! $item->content_one !!}
        </div>
        <div class="grid__cell">
            {!! $item->content_two !!}
        </div>
    </div>
@else
    {!! $item->content_one !!}
    {!! $item->content_two !!}
@endif