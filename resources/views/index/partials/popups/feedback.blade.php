<div class="popup" id="popup_callback">
    <div class="popup__box">
        <div class="popup__body">
            <div class="feedback__header">
                <a class="js-popup__close popup__close close close_big" href="#">
                    <span class="close-span close-span--first"></span>
                    <span class="close-span close-span--last"></span>
                </a>
                <div class="popup__heading"><span>@lang("index.blocks.forms.ask_your_question.title")</span></div>

                {!! Form::open(['route' => 'index.send.form', 'class' => 'def-form def-form--popup js_form js_form--popup-close']) !!}
                {!! Form::hidden('url', request()->url()) !!}
                {{--{!! Form::hidden('type', 'question') !!}--}}
                    <div class="def-form__row">
                        @include('index.partials.forms.partials.text', ['name' => 'first_name'])
                    </div>
                    <div class="def-form__row">
                        @include('index.partials.forms.partials.text', ['name' => 'email'])
                    </div>
                    <div class="def-form__row">
                        @include('index.partials.forms.partials.text', ['name' => 'company_name'])
                    </div>
                    <div class="def-form__row">
                        @include('index.partials.forms.partials.textarea', ['name' => 'message'])
                    </div>
                    <div class="btn-inner">
                        <input class="btn popup__btn" type="submit" value="@lang('index.elements.buttons.send')">
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>