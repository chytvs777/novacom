<footer class="footer ">
    <div class="box">
        <div class="footer__inner">
            <div class="footer__title">© Новаком 2007-2017</div>
            <div class="footer__data">
                <div class="footer__title">г. Минск, ул. Либаво-Роменская, д. 23, 5 этаж, Минск, 220028</div>
                <div class="footer__social">
                    <a class="footer__link" href="#"><img src="{{ asset('assets/index/images/ico-fb.png') }}" alt="facebook"></a>
                    <a class="footer__link" href="#"><img src="{{ asset('assets/index/images/ico-linked.png') }}" alt="linkedin"></a>
                </div>
            </div>
        </div>
    </div>
</footer>