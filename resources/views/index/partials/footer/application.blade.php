@php($formName = $formName ?? 'how_can_we_help_you')
<section class="section section--bg-red g-help" style="background-image: url({{ asset('assets/index/images/shadow__heading-help.png') }});">
    <div class="box">
        <div class="heading">@lang("index.blocks.forms.{$formName}.title")</div>
        <div class="g-help__inner">
            {!! Form::open(['route' => 'index.send.form', 'class' => 'help-form js_form']) !!}
            {!! Form::hidden('url', request()->url()) !!}
                <div class="help-form__row">
                    @include('index.partials.forms.partials.text', ['name' => 'first_name', 'classType' => 'help'])
                    @include('index.partials.forms.partials.text', ['name' => 'phone', 'classType' => 'help'])
                </div>
                <div class="help-form__row">
                    @include('index.partials.forms.partials.text', ['name' => 'company_name', 'classType' => 'help'])
                    @include('index.partials.forms.partials.text', ['name' => 'email', 'classType' => 'help'])
                </div>
                <div class="help-form__row">
                    @include('index.partials.forms.partials.textarea', ['name' => 'message', 'classType' => 'help'])
                </div>
                <div class="btn-inner"><button class="btn help-form__btn" type="submit">@lang('index.elements.buttons.send')</button></div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
