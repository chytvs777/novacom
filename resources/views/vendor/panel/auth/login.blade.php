@extends('panel::layouts.auth')

@section('title', 'Авторизация')

@section('auth')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h3>Вход в панель управления</h3>
            {!! Form::open(['action' => 'Admin\Auth\LoginController@login', 'class' => 'm-t']) !!}
            <div class="form-group">
                {!! Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
            <div class="form-group">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Пароль']) !!}
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Войти</button>
            @if ($errors->count() > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </div>
            @endif
            {!! Form::close() !!}
            <p class="m-t"><small>@include('panel::layouts.partials.footer.copyright')</small></p>
        </div>
    </div>
@stop