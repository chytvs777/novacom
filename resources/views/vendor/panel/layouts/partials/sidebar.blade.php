<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                @include('panel::layouts.partials.sidebar.user')
                <div class="logo-element">
                    NAV
                </div>
            </li>
            @include('admin.partials.menu')
        </ul>
    </div>
</nav>