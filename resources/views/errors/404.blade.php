@extends('index.layouts.main')

@section('title', __('index.pages.404.title'))

@section('meta_og')
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('title')"/>
    <meta property="og:url" content="{{ request()->url() }}"/>
    {{--<meta property="og:image" content=""/>--}}
@endsection

@section('content')

    <section class="section demands-list section--bg-red">
        <div class="section__inner">
            <div class="box">
            <div class="heading">@lang('index.pages.404.title')</div>
            <div class="demands-list__header" style="font-size: 16px;">
                @lang('index.pages.404.description') <br>
                <a href="{{ route('index.home') }}" style="color: #2b2a29; line-height: 47px;">@lang('index.pages.404.back_home')</a>
            </div>
            </div>
        </div>
    </section>

@endsection