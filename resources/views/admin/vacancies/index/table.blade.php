@if($vacancies->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th>{{ __('admin_labels.direction_id') }}</th>
            <th>{{ __('admin_labels.slug') }}</th>
            <th>{{ __('admin_labels.created_at') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($vacancies as $vacancy)
            <tr>
                <td>{{ $vacancy->id }}</td>
                <td>
                    <a href="{{route ('admin.'. $entity.'.edit', $vacancy)}}" class="pjax-link">{{ $vacancy->title }}</a>
                    @if($vacancy->is_hot)
                        <span class="fa fa-fire text-danger"></span>
                    @endif
                </td>
                <td>{{ $vacancy->direction ? $vacancy->direction->title : '' }}</td>
                <td>
                    {!! Form::goToUrl($vacancy->url) !!}
                </td>
                <td>{{ $vacancy->created_at }}</td>
                <td class="td-actions">
                    <a href="{{route ('admin.'. $entity.'.edit', $vacancy) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route ('admin.'. $entity.'.delete', $vacancy) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif