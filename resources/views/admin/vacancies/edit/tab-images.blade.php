<div role="tabpanel" class="tab-pane " id="images">
    <h2>Изображение</h2>

    <div class="hr-line-dashed"></div>
    <h3 class="h3-content">Фото</h3>
    {!! $vacancy->getImagesView($vacancy::IMAGE_TYPE_IMAGE) !!}

    <small>1920x548</small>

</div>