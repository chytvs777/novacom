<div role="tabpanel" class="tab-pane" id="seo">
    <h2>SEO</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang', ['class' => 'seo'])

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="seo-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('meta_title', $lang, $vacancy) }}
                        {{ Form::panelTextareaLang('meta_description', $lang, $vacancy) }}
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>