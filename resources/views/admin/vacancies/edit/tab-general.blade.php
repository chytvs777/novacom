<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang')

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('title', $lang, $vacancy) }}
                        {{ Form::panelTextareaLang('content_short', $lang, $vacancy, 'small') }}
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Блок1</h3>
                    {{ Form::panelTextLang('box_one_title', $lang, $vacancy) }}
                    @include('admin.partials.components.box_one', ['model' => $vacancy, 'lang' => $lang])
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Блок2</h3>
                    {{ Form::panelTextLang('box_two_title', $lang, $vacancy) }}
                    @include('admin.partials.components.box_two', ['model' => $vacancy, 'lang' => $lang])
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Контент</h3>
                    {{ Form::panelTextLang('content_title', $lang, $vacancy) }}
                    {{ Form::panelTextLang('content_sub_title', $lang, $vacancy) }}
                    {{ Form::panelTextareaLang('content_one', $lang, $vacancy, true, 'withTitle') }}
                    {{ Form::panelTextareaLang('content_two', $lang, $vacancy, true, 'withTitle') }}
                </div>
            </div>
        @endforeach
    </div>
    <div class="hr-line-dashed"></div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::panelSelect('direction_id', $directions) }}
            {{ Form::panelSelect('command_id', $commands) }}
            {{ Form::panelRadio('is_hot') }}
{{--            {{ Form::panelText('slug', null, '', ['placeholder' => 'Заполняется автоматически']) }}--}}
        </div>
    </div>

</div>