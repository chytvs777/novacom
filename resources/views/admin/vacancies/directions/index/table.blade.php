@if($directions->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th class="td-actions">#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th>{{ __('admin.vacancies.title') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($directions as $direction)
            <tr>
                <td>{{ $direction->id }}</td>
                <td><a href="{{ route ('admin.'. $entity.'.edit', $direction) }}" class="pjax-link">{{ $direction->title }}</a></td>
                <td><a href="{{ route ('admin.vacancies.index', ['direction_id' => $direction->id]) }}" class="pjax-link">{{ $direction->vacancies_count }}</a></td>
                <td class="td-actions">
                    <a href="{{ route ('admin.'. $entity.'.edit', $direction) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route ('admin.'. $entity.'.delete', $direction) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif