<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang')

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('title', $lang, $article) }}
                        {{ Form::panelTextareaLang('content_short', $lang, $article) }}
                        {{ Form::panelTextareaLang('announcement', $lang, $article) }}
                    </div>
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-12">
                        <div class="hr-line-dashed"></div>
                        <h3 class="edit">Отзыв</h3>
                        <div class="row">
                            <div class="col-md-6">
                                {{ Form::panelTextareaLang('review_text', $lang, $article) }}
                            </div>
                            <div class="col-md-6">
                                {{ Form::panelTextLang('review_author', $lang, $article) }}
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::panelTextareaLang('reference_information', $lang, $article, 'small', 'withTitle') }}
                {{ Form::panelTextareaLang('content', $lang, $article, true, 'withTitle') }}
            </div>
        @endforeach
    </div>
    <div class="hr-line-dashed"></div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::panelSelect('command_id', $commands) }}
{{--            {{ Form::panelText('slug', null, '', ['placeholder' => 'Заполняется автоматически']) }}--}}
        </div>
    </div>

</div>