<div role="tabpanel" class="tab-pane " id="images">

    <div class="hr-line-dashed"></div>
    <h2>Клиенты</h2>
    {!! $setting->getImagesView($setting::IMAGE_TYPE_CLIENTS) !!}

    <div class="hr-line-dashed"></div>
    <h2>Технологии 1</h2>
    {!! $setting->getImagesView($setting::IMAGE_TYPE_TECHNOLOGIES_ONE) !!}

    <div class="hr-line-dashed"></div>
    <h2>Технологии 2</h2>
    {!! $setting->getImagesView($setting::IMAGE_TYPE_TECHNOLOGIES_TWO) !!}
</div>