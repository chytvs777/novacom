@extends('panel::layouts.main')

@section('title', __('admin.settings.edit'))

@section('actions')
    <a href="{{ url()->previous() }}" class="btn btn-default js_form-ajax-back pjax-link"><i class="fa fa-chevron-left"></i> Вернуться назад</a>
@endsection

@section('main')

    {!! Form::model($setting, ['route' => 'admin.settings.store', 'class' => 'ibox form-horizontal js_form-ajax js_form-ajax-reset'])  !!}
    <ul class="nav nav-tabs nav-products1" role="tablist">
        @include('admin.settings.edit.nav')
    </ul>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    @include('admin.settings.edit.tab-general')
                    @include('admin.settings.edit.tab-images')
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-footer">
        {{ Form::panelButton() }}
    </div>
@endsection