@extends('panel::layouts.main')

@section('title', __('admin.settings.translation.index'))

@section('actions')
    <a href="{{ url()->previous() }}" class="btn btn-default js_form-ajax-back pjax-link"><i
                class="fa fa-chevron-left"></i> Вернуться назад</a>
@endsection

@section('main')
    {!! Form::open(['route' => 'admin.settings.translation.store', 'class' => 'ibox form-horizontal js_form-ajax js_form-ajax-reset'])  !!}
    <ul class="nav nav-tabs nav-products1" role="tablist">
        @foreach(\LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <li role="presentation" class="{{ $loop->index == 0 ? 'active' : '' }}">
                <a href="#localization-{{ $lang }}" role="tab" data-toggle="tab">{{ $localization['name'] }}</a>
            </li>
        @endforeach
    </ul>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    @foreach(\LaravelLocalization::getSupportedLocales() as $lang => $localization)
                        <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="localization-{{ $lang }}">
                            <h2>Перевод</h2>
                            <div class="hr-line-dashed"></div>
                            @foreach($languageLines as $languageLine)
                                <div class="row m-b-sm">
                                    <div class="col-md-4 text-right">
                                        <strong>{{ $languageLine->key }}</strong>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"
                                               name="language_lines[{{ $languageLine->id }}][{{ $lang }}]"
                                               value="{{ $languageLine->text[$lang] ?? '' }}">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-footer">
        {{ Form::panelButton() }}
    </div>
    {!! Form::close() !!}
@endsection