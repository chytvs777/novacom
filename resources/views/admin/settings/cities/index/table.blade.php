@if($cities->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th>{{ __('admin_labels.country_id') }}</th>
            <th>{{ __('admin_labels.created_at') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($cities as $city)
            <tr>
                <td>{{ $city->id }}</td>
                <td><a href="{{route ('admin.'. $entity.'.edit', $city)}}" class="pjax-link">{{ $city->title }}</a></td>
                <td>{{ $city->country ? $city->country->title : '' }}</td>
                <td>{{ $city->created_at }}</td>
                <td class="td-actions">
                    <a href="{{route ('admin.'. $entity.'.edit', $city) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route ('admin.'. $entity.'.delete', $city) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif