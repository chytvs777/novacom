@extends('panel::layouts.main')

@section('title', $city->id ? __('admin.'. $entity . '.edit') : __('admin.'. $entity . '.create'))

@section('actions')
    <a href="{{ url()->previous() }}" class="btn btn-default js_form-ajax-back pjax-link"><i class="fa fa-chevron-left"></i> Вернуться назад</a>
@endsection

@section('main')

    {!! Form::model($city, ['route' => 'admin.'. $entity.'.store', 'class' => 'ibox form-horizontal js_form-ajax js_form-ajax-reset'])  !!}
    {!! Form::hidden('id') !!}
        <ul class="nav nav-tabs nav-products1" role="tablist">
            @include('admin.'. $entity.'.edit.nav')
        </ul>
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        @include('admin.'. $entity.'.edit.tab-general')
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-footer">
            {{ Form::panelButton() }}
        </div>
    {!! Form::close() !!}
@endsection