@extends('panel::layouts.main')

@section('title', __('admin.'. $entity . '.title'))

@section('actions')
@endsection

@section('main')
    <div class="ibox">
        <div class="ibox-content">
            {{--<h2>{{ __('admin.'. $entity . '.list') }}</h2>--}}
            {{--<div class="hr-line-dashed"></div>--}}
            <div class="js_table-wrapper">
                @include('admin.'. $entity . '.index.table')
            </div>
        </div>
        <div class="ibox-footer js_table-pagination">
            @include('admin.partials.pagination', ['paginator' => $texts])
        </div>
    </div>
@endsection