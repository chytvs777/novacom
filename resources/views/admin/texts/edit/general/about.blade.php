@include('admin.partials.form.lang.tabsLang')

<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
        <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::panelTextLang('title', $lang, $text) }}
                    {{ Form::panelTextareaLang('content_short', $lang, $text, 'small') }}
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Блок1</h3>
                    @include('admin.partials.components.box_one', ['model' => $text, 'lang' => $lang, 'withDescription' => true, 'withIcon' => __('admin.texts.about.icons')])
                </div>
            </div>
        </div>
    @endforeach

    <h3 class="edit">Партнеры</h3>

    <div class="hr-line-dashed"></div>
    {!! $text->getImagesView($text::IMAGE_TYPE_IMAGE) !!}
</div>