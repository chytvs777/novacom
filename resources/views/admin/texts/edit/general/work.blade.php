@include('admin.partials.form.lang.tabsLang')

<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
        <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
            <div class="row">
                <div class="col-md-12">
                    {{ Form::panelTextLang('title', $lang, $text) }}
                    {{ Form::panelTextareaLang('content_short', $lang, $text, 'small') }}
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Блок1</h3>
                    {{ Form::panelTextLang('box_one_title', $lang, $text) }}
                    {{ Form::panelTextareaLang('box_one_description', $lang, $text) }}
                    @include('admin.partials.components.box_one', ['model' => $text, 'lang' => $lang])
                </div>
                <div class="col-md-12 m-b-md">
                    <div class="hr-line-dashed"></div>
                    <h3 class="edit">Блок2</h3>
                    {{ Form::panelTextLang('box_two_title', $lang, $text) }}
                    {{ Form::panelTextareaLang('box_two_description', $lang, $text) }}
                    @include('admin.partials.components.box_two', ['model' => $text, 'lang' => $lang, 'withDescription' => true])
                </div>
            </div>
        </div>
    @endforeach

    <h3 class="edit">Статусы и награды</h3>

    <div class="hr-line-dashed"></div>
    {!! $text->getImagesView($text::IMAGE_TYPE_IMAGE) !!}

    <small>314x268</small>
</div>