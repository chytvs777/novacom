@include('admin.partials.form.lang.tabsLang')

<div class="tab-content">
    @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
        <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
            <div class="row">
                <div class="col-md-6">
                    {{ Form::panelTextLang('title', $lang, $text) }}
                    {{ Form::panelTextareaLang('content_short', $lang, $text) }}
                </div>
                <div class="col-md-6">
                </div>

            </div>
            {{--{{ Form::panelTextareaLang('content', $lang, $text, true, 'withTitle') }}--}}
        </div>
    @endforeach
</div>