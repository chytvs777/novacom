<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @if(view()->exists("admin.$entity.edit.general.$text->key"))
        @include("admin.$entity.edit.general.$text->key")
    @else
        @include("admin.$entity.edit.general.default")
    @endif

</div>