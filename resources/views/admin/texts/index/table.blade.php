@if($texts->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th class="td-actions">#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($texts as $text)
            <tr>
                <td>{{ $text->key }}</td>
                <td><a href="{{ route ('admin.'. $entity.'.edit', $text) }}" class="pjax-link">{{ $text->title }}</a></td>
                <td class="td-actions">
                    <a href="{{route ('admin.'. $entity.'.edit', $text) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif