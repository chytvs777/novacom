@extends('panel::layouts.main')

@section('title', __('admin.'. $entity . '.title'))

@section('actions')
    <a href="{{ route('admin.'. $entity . '.create') }}" class="btn btn-sm btn-primary pjax-link"><span class="fa fa-plus"></span> {{ __('admin.'. $entity . '.add_button') }}</a>
@endsection

@section('main')
    <div class="ibox">
        <div class="ibox-content">
            {{--<h2>{{ __('admin.'. $entity . '.list') }}</h2>--}}
            {{--<div class="hr-line-dashed"></div>--}}
            <div class="js_table-wrapper">
                @include('admin.'. $entity . '.index.table')
            </div>
        </div>
        <div class="ibox-footer js_table-pagination">
            @include('admin.partials.pagination', ['paginator' => $pages])
        </div>
    </div>
@endsection