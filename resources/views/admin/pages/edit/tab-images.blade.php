<div role="tabpanel" class="tab-pane " id="images">
    <h2>Изображения</h2>
    <div class="hr-line-dashed"></div>

    <div class="hr-line-dashed"></div>
    <h3 class="h3-content">Галерея</h3>
    {!! $page->getImagesView($page::IMAGE_TYPE_IMAGE_GALLERY) !!}

</div>