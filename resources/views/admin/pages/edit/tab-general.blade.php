<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang')

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('title', $lang, $page) }}
{{--                        {{ Form::panelTextareaLang('content_short', $lang, $page, null) }}--}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::panelTextLang('meta_title', $lang, $page) }}
                        {{ Form::panelTextareaLang('meta_description', $lang, $page) }}
                    </div>
                    <div class="col-md-12">
                        @include('admin.partials.components.advantages', ['model' => $page, 'lang' => $lang])
                    </div>
                    <div class="col-md-12">
                        @include('admin.partials.components.content', ['model' => $page, 'lang' => $lang])
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hr-line-dashed"></div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::panelSelect('blocks', __('admin.pages.blocks'), count($page->blocks) ? $page->blocks : [], ['class' => 'form-control js_panel_input-chosen', 'multiple' => 'multiple']) !!}
            {{ Form::panelText('slug', null, '', ['placeholder' => 'Заполняется автоматически']) }}
        </div>
    </div>

</div>