<div role="tabpanel" class="tab-pane " id="images">
    <h2>Изображения</h2>

    <div class="hr-line-dashed"></div>
    <h3 class="h3-content">Фото</h3>
    {!! $command->getImagesView($command::IMAGE_TYPE_IMAGE) !!}

    <small>370x370</small>

</div>