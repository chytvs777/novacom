<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang')

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('full_name', $lang, $command) }}
                        {{ Form::panelTextareaLang('content', $lang, $command) }}
                        {{ Form::panelTextareaLang('review_text', $lang, $command) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::panelTextLang('position', $lang, $command) }}
                        {{ Form::panelTextLang('phone_home', $lang, $command) }}
                        {{ Form::panelTextLang('phone_mobile', $lang, $command) }}

                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="row">
        <div class="col-md-6">
            {{ Form::panelText('email') }}
            {{ Form::panelRadio('is_leadership') }}
            {{ Form::panelRadio('is_press_service') }}
            {{ Form::panelRadio('is_show_news') }}
            {{ Form::panelRadio('is_review') }}
        </div>
    </div>


</div>