@if($commands->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th class="td-actions"></th>
            <th class="td-actions">#</th>
            <th>{{ __('admin_labels.full_name') }}</th>
            <th>{{ __('admin_labels.position') }}</th>
            <th>{{ __('admin_labels.phone_home') }}</th>
            <th>{{ __('admin_labels.phone_mobile') }}</th>
            <th>{{ __('admin_labels.is_leadership') }}</th>
            <th>{{ __('admin_labels.is_press_service') }}</th>
            <th>{{ __('admin_labels.is_review') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="js-sortable" data-url="{{ action('Admin\HomeController@reorder', ['class' => $commands->getQueueableClass()]) }}">
        @foreach($commands as $command)
            <tr data-id="{{ $command->id }}">
                <td><span class="btn btn-xs btn-white f-z-10"><span class="fa fa-bars"></span></span></td>
                <td>{{ $command->id }}</td>
                <td><a href="{{ route ('admin.'. $entity.'.edit', $command) }}" class="pjax-link">{{ $command->full_name }}</a></td>
                <td>{{ $command->position }}</td>
                <td nowrap>{{ $command->phone_home }}</td>
                <td nowrap>{{ $command->phone_mobile }}</td>
                <td>{!! __('pretty.yes_or_no.'. $command->is_leadership) !!}</td>
                <td>{!! __('pretty.yes_or_no.'. $command->is_press_service) !!}</td>
                <td>{!! __('pretty.yes_or_no.'. $command->is_review) !!}</td>
                <td class="td-actions">
                    <a href="{{ route ('admin.'. $entity.'.edit', $command) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route ('admin.'. $entity.'.delete', $command) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif