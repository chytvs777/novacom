@if ($breadcrumbs)
    <ul class="breadcrumb__list">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li class="breadcrumb__item"><a href="{{ $breadcrumb->url }}" class="breadcrumb__link pjax-link">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="breadcrumb__item"><div class="breadcrumb__link">{{ $breadcrumb->title }}</div></li>
            @endif
        @endforeach
    </ul>
@endif
