<div class="row border-bottom">
    <nav class="navbar-static-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-warning " href="#"><i class="fa fa-bars"></i> </a>
            {{--{!! Form::open(['route' => 'admin.sync1c', 'class' => 'input-group-btn add-button-top js_form-ajax', 'style' => 'margin-left: 15px;'])  !!}--}}
                {{--<button class="btn btn-sm btn-white" type="submit"><span class="fa fa-spinner"></span> Синхронизация 1C</button>--}}
            {{--{!! Form::close() !!}--}}
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="{{ action('Admin\\Auth\\LoginController@logout') }}">
                    <i class="fa fa-sign-out"></i> Выйти
                </a>
            </li>
        </ul>
    </nav>
</div>