@php($name = $lang. '[' .$label. ']')
@php($val = $model->translate($lang)->{$label} ?? '')
@php($dataLabels = ['box_one_title', 'box_two_title'])
@if(in_array($label, $dataLabels))
    @php($name = $lang. '[data][' .$label. ']')
    @php($val = $model->translate($lang)->data[$label] ?? '')
@endif
<div class="form-group">
    {!! Form::label($name, __('admin_labels.'. $label), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text($name, $val, ['class' => 'form-control ']) !!}
        <p class="error-block"></p>
    </div>
</div>