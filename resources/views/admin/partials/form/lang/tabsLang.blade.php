@php($class = $class ?? 'category')
<ul class="tabs js_sticky" role="tablist">
    @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
        <li role="presentation" class="{{ $loop->index == 0 ? 'active' : '' }}"><a href="#{{ $class }}-lang-{{ $lang }}" role="tab" data-toggle="tab">{{ mb_strtoupper($localization['native']) }}</a></li>
    @endforeach
</ul>