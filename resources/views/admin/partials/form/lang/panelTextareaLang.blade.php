@php($name = $lang. '[' .$label. ']')
@php($val = $model->translate($lang)->{$label} ?? '')
@php($dataLabels = ['box_one_description', 'box_two_description'])
@if(in_array($label, $dataLabels))
    @php($name = $lang. '[data][' .$label. ']')
    @php($val = $model->translate($lang)->data[$label] ?? '')
@endif

@if($redactor)
    @php($redactor = $redactor === 'small' ? 'js_panel_input-froala1-small' : 'js_panel_input-froala1')
@endif

@if(!$type)
    <div class="form-group">
        {!! Form::label($name, __('admin_labels.'. $label), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::textarea($name, $val, ['class' => 'form-control '. ($redactor ? $redactor : '')]) !!}
            <p class="error-block"></p>
        </div>
    </div>
@else
    @if($redactor)
        <div class="hr-line-dashed"></div>
        <h3 class="edit">{{ __('admin_labels.'. $label) }}</h3>
    @endif
    {{ Form::textarea($name, $val, ['class' => 'form-control '. ($redactor ? $redactor : ''), 'placeholder' => __('admin_labels.'. $label)]) }}
@endif