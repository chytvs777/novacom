<div class="form-group">
    {{ Form::label($name .'-yes', __('admin_labels.'. $name), ['class' => 'control-label col-md-4']) }}
    <div class="col-md-8">
        <div class="radio radio-warning radio-inline">
            {{ Form::radio($name, 1, null, ['id' => $name .'-yes', 'class' => $class]) }}
            <label for="{{ $name }}-yes">Да</label>
        </div>
        <div class="radio radio-danger radio-inline">
            {{ Form::radio($name, 0, !$selected ? true : false, ['id' => $name.'-no', 'class' => $class]) }}
            <label for="{{ $name }}-no">Нет</label>
        </div>
    </div>
</div>