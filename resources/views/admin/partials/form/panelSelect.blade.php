@php($fieldName = $name)
@if(isset($arr['multiple']))
    @php($fieldName .= '[]')
@endif

<div class="form-group">
    {!! Form::label($fieldName, __('admin_labels.'. $name), ['class' => ($col ? 'col-md-4' : '' . ' control-label')]) !!}
    <div class="{{ $col ? 'col-md-8' : '' }}">
        {!! Form::select($fieldName, $values, $selected, array_merge(['class' => 'form-control'], $arr)) !!}
        <p class="error-block"></p>
    </div>
</div>