@if(!$type)
    <div class="form-group">
        {!! Form::label($name, $label ?: __('admin_labels.'. $name), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::textarea($name, null, ['class' => 'form-control '. ($redactor ? 'js_panel_input-redactor' : ''), $disabled ? 'disabled' : '']) !!}
            <p class="error-block"></p>
        </div>
    </div>
@else
    @if($redactor)
        <div class="hr-line-dashed"></div>
        <h3 class="edit">{{ __('admin_labels.'. $name) }}</h3>
    @endif
    {{ Form::textarea($name, null, ['class' => 'form-control '. ($redactor ? 'js_panel_input-froala' : ''), 'placeholder' => __('admin_labels.'. $name), $disabled ? 'disabled' : '']) }}
@endif