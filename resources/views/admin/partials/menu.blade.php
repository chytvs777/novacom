<li class="{{ str_contains(request()->url(), route('admin.articles.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.articles.index') }}" class="pjax-link">
        <i class="fa fa-newspaper-o"></i>
        <span class="text">  {{ __('admin.articles.title') }}</span>
    </a>
</li>

<li class="{{ str_contains(request()->url(), route('admin.texts.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.texts.index') }}" class="pjax-link">
        <i class="fa fa-font"></i>
        <span class="text"> {{ __('admin.texts.title') }}</span>
    </a>
</li>

<li class="sub-menu {{ str_contains(request()->url(), route('admin.vacancies.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.vacancies.index') }}">
        <i class="fa fa-bullhorn"></i>
        <span class="nav-label"> {{ __('admin.vacancies.title') }}</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse {{ str_contains(request()->url(), route('admin.vacancies.index')) ? 'in' : '' }}">
        <li class="{{
            str_contains(request()->url(), route('admin.vacancies.index'))
            && !str_contains(request()->url(), route('admin.vacancies.directions.index'))
            ? 'active' : '' }}">
            <a href="{{ route('admin.vacancies.index')  }}" class="pjax-link"> {{ __('admin.vacancies.title') }}</a>
        </li>
        <li class="{{ str_contains(request()->url(), route('admin.vacancies.directions.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.vacancies.directions.index')  }}" class="pjax-link"> {{ __('admin.vacancies.directions.title') }}</a>
        </li>
    </ul>
</li>

<li class="sub-menu {{ str_contains(request()->url(), route('admin.projects.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.projects.index') }}">
        <i class="fa fa-briefcase"></i>
        <span class="nav-label"> {{ __('admin.projects.title') }}</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse {{ str_contains(request()->url(), route('admin.projects.index')) ? 'in' : '' }}">
        <li class="{{
            str_contains(request()->url(), route('admin.projects.index'))
            && !str_contains(request()->url(), route('admin.projects.options.index'))
            ? 'active' : '' }}">
            <a href="{{ route('admin.projects.index')  }}" class="pjax-link"> {{ __('admin.projects.title') }}</a>
        </li>
        <li class="{{ str_contains(request()->url(), route('admin.projects.options.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.projects.options.index')  }}" class="pjax-link"> {{ __('admin.projects.options.title') }}</a>
        </li>
    </ul>
</li>

<li class="{{ str_contains(request()->url(), route('admin.commands.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.commands.index') }}" class="pjax-link">
        <i class="fa fa-users"></i>
        <span class="text">  {{ __('admin.commands.title') }}</span>
    </a>
</li>

<li class="{{ str_contains(request()->url(), route('admin.histories.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.histories.index') }}" class="pjax-link">
        <i class="fa fa-history"></i>
        <span class="text">  {{ __('admin.histories.title') }}</span>
    </a>
</li>


{{--<li class="{{ str_contains(request()->url(), route('admin.pages.index')) ? 'active' : '' }}">--}}
    {{--<a href="{{ route('admin.pages.index') }}" class="pjax-link">--}}
        {{--<i class="fa fa-file"></i>--}}
        {{--<span class="text"> {{ __('admin.pages.title') }}</span>--}}
    {{--</a>--}}
{{--</li>--}}

<li class="{{ str_contains(request()->url(), route('admin.users.index')) ? 'active' : '' }}">
    <a href="{{ route('admin.users.index') }}" class="pjax-link">
        <i class="fa fa-user"></i>
        <span class="text">  {{ __('admin.users.title') }}</span>
    </a>
</li>

<li class="sub-menu {{ str_contains(request()->url(), route('admin.settings.edit')) ? 'active' : '' }}">
    <a href="{{ route('admin.settings.edit') }}">
        <i class="fa fa-cog"></i>
        <span class="nav-label"> {{ __('admin.settings.title') }}</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="nav nav-second-level collapse">
        <li class="{{ str_contains(request()->url(), route('admin.settings.edit')) ? 'active' : '' }}">
            <a href="{{ route('admin.settings.edit') }}" class="pjax-link"> {{ __('admin.settings.title') }} </a>
        </li>

        <li class="{{ str_contains(request()->url(), route('admin.settings.countries.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.settings.countries.index') }}" class="pjax-link"> {{ __('admin.settings.countries.title') }} </a>
        </li>

        <li class="{{ str_contains(request()->url(), route('admin.settings.cities.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.settings.cities.index') }}" class="pjax-link"> {{ __('admin.settings.cities.title') }} </a>
        </li>

        <li class="{{ str_contains(request()->url(), route('admin.settings.translation.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.settings.translation.index') }}" class="pjax-link"> {{ __('admin.settings.translation.index') }} </a>
        </li>
    </ul>
</li>

<hr>

<li>
    <a href="{{ route('index.home') }}" target="_blank">
        <i class="fa fa-sign-in"></i>
        <span class="text"> Перейти на сайт</span>
    </a>
</li>

