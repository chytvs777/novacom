
<div class="table-responsive">
    <table class="table table-bordered table-stripped js_multiple-wrapper">
        <thead>
        <tr>
            <th>{{ __('admin_labels.title') }}</th>
            @if(isset($withDescription))
                <th>{{ __('admin_labels.description') }}</th>
            @endif
            @if(isset($withIcon))
                <th>{{ __('admin_labels.icon') }}</th>
            @endif
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        @php($i = 0)
        @if($model->translate($lang) && isset($model->translate($lang)->data['box_two']))
            @foreach($model->translate($lang)->data['box_two'] as $item)
                <tr class="js_multiple-row js_reindex-{{ $lang }}-box_two" data-name="{{ $lang }}-box_two">
                    <td>
                        {!! Form::text($lang. "[data][box_two][$i][title]", $item['title'], ['class' => "form-control js_$lang-box_two-title"]) !!}
                    </td>
                    @if(isset($withDescription))
                        <td>
                            {!! Form::textarea($lang. "[data][box_two][$i][description]", $item['description'], ['class' => "form-control js_$lang-box_two-description"]) !!}
                        </td>
                    @endif
                    @if(isset($withIcon))
                        <td>
                            {!! Form::select($lang. "[data][box_two][$i][icon]", $withIcon, $item['icon'], ['class' => "form-control js_$lang-box_two-icon"]) !!}
                        </td>
                    @endif
                    <td>
                        <div class="input-group input-group-sm">
                            <span class="input-group-btn">
                                <span class="btn btn-default js_multiple-remove" data-name="{{ $lang }}-box_two">
                                    <i class="fa fa-trash"></i>
                                </span>
                            </span>
                        </div>
                    </td>
                </tr>
                @php($i++)
            @endforeach
        @endif
        <tr class="js_multiple-row js_multiple-row-clone js_reindex-{{ $lang}}-box_two" data-name="{{ $lang}}-box_two">
            <td>
                {!! Form::text(null, null, ['class' => "form-control js_$lang-box_two-title", 'disabled']) !!}
            </td>
            @if(isset($withDescription))
                <td>
                    {!! Form::textarea(null, null, ['class' => "form-control js_$lang-box_two-description", 'disabled']) !!}
                </td>
            @endif
            @if(isset($withIcon))
                <td>
                    {!! Form::select(null, $withIcon, null, ['class' => "form-control js_$lang-box_two-icon", 'disabled']) !!}
                </td>
            @endif
            <td>
                <div class="input-group input-group-sm">
                    <span class="input-group-btn">
                        <span class="btn btn-default js_multiple-remove" data-name="{{ $lang}}-box_two">
                            <i class="fa fa-trash"></i>
                        </span>
                    </span>
                </div>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="3">
                <button class="btn btn-default btn-sm js_multiple-add" data-name="{{ $lang}}-box_two" type="button">
                    <i class="fa fa-plus"></i> Добавить
                </button>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
