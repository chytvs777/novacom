@if($users->count())
    <div class="table-responsive">
        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>{{ __('admin_labels.first_name') }}</th>
                <th>{{ __('admin_labels.email') }}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td><a href="{{route ('admin.' . $entity . '.edit', $user)}}" class="pjax-link">{{ $user->first_name}}</a></td>
                    <td><i class="fa fa-email"></i> {{ $user->email }}</td>
                    <td class="td-actions">
                        <a href="{{route ('admin.' . $entity . '.edit', $user)}}"
                           class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                            <i class="fa fa-edit"></i>
                        </a>
                        @if(auth()->id() != $user->id)
                            <a href="{{route ('admin.' . $entity . '.delete', $user)}}"
                               class="btn btn-sm btn-danger js_panel_confirm"
                               data-toggle="tooltip" title="Удалить">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif