@extends('panel::layouts.main')

@section('title', $user->id ? __('admin.'. $entity . '.edit') : __('admin.'. $entity . '.create'))

@section('actions')
    <a href="{{ url()->previous() }}" class="btn btn-default js_form-ajax-back pjax-link"><i class="fa fa-chevron-left"></i> Вернуться назад</a>
@endsection

@section('main')
    {!! Form::model($user, ['route' => 'admin.'. $entity . '.store', 'class' => 'ibox form-horizontal  js_form-ajax js_form-ajax-reset']) !!}
        {!! Form::hidden('id', $user->id) !!}
        <div class="ibox-content">
            <h2>{{ $user->id ? __('admin.'. $entity . '.edit') : __('admin.'. $entity . '.create') }}</h2>
            <div class="hr-line-dashed"></div>
            <div class="row">
                <div class="col-md-6">
                    {{ Form::panelText('first_name') }}
                    {{ Form::panelText('email') }}
                    <div class="form-group">
                        {!! Form::label('password', 'Пароль', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-8">
                            {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                            <p class="error-block"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-footer">
            {{ Form::panelButton() }}
        </div>
    {!! Form::close() !!}
@endsection