<div role="tabpanel" class="tab-pane " id="images">
    <h2>Изображение</h2>

    <div class="hr-line-dashed"></div>
    <h3 class="h3-content">Фото в списке</h3>
    {!! $project->getImagesView($project::IMAGE_TYPE_IMAGE) !!}
    <small>580x351</small>

    <div class="hr-line-dashed"></div>
    <h3 class="h3-content">Заказчик</h3>
    {!! $project->getImagesView($project::IMAGE_TYPE_CUSTOMER) !!}

</div>