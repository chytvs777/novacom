<div role="tabpanel" class="tab-pane active" id="general">
    <h2>Основные</h2>
    <div class="hr-line-dashed"></div>

    @include('admin.partials.form.lang.tabsLang')

    <div class="tab-content">
        @foreach(LaravelLocalization::getSupportedLocales() as $lang => $localization)
            <div role="tabpanel" class="tab-pane {{ $loop->index == 0 ? 'active' : '' }}" id="category-lang-{{ $lang }}">
                <div class="row">
                    <div class="col-md-6">
                        {{ Form::panelTextLang('title', $lang, $project) }}
                        {{ Form::panelTextareaLang('content_short', $lang, $project) }}
                    </div>
                    <div class="col-md-6">
                        {{ Form::panelTextLang('customer_name', $lang, $project) }}
                        {{ Form::panelTextLang('customer_info', $lang, $project) }}
                        {{ Form::panelTextLang('customer_site', $lang, $project) }}
                    </div>
                    <div class="col-md-12 m-b-md">
                        <div class="hr-line-dashed"></div>
                        <h3 class="edit">Отзыв</h3>
                        {{ Form::panelTextLang('review_name', $lang, $project) }}
                        {{ Form::panelTextLang('review_position', $lang, $project) }}
                        {{ Form::panelTextareaLang('review_text', $lang, $project) }}
                    </div>
                    <div class="col-md-12 m-b-md">
                        <div class="hr-line-dashed"></div>
                        <h3 class="edit">Блок1</h3>
                        @include('admin.partials.components.box_one', ['model' => $project, 'lang' => $lang])
                    </div>
                    <div class="col-md-12 m-b-md">
                        <div class="hr-line-dashed"></div>
                        <h3 class="edit">Блок2</h3>
                        {{ Form::panelTextLang('box_two_title', $lang, $project) }}
                        @include('admin.partials.components.box_two', ['model' => $project, 'lang' => $lang])
                    </div>
                </div>
                {{ Form::panelTextareaLang('content', $lang, $project, true, 'withTitle') }}
            </div>
        @endforeach
    </div>
    <div class="hr-line-dashed"></div>
    <div class="row">
        <div class="col-md-6">
            {{ Form::panelSelect('customer_type', __('index.pages.projects.customer_types')) }}
            {!! Form::panelSelect('options', $optionsGroup, $project->options ? $project->options->pluck('id')->toArray() : '', ['class' => 'form-control js_panel_input-chosen', 'multiple' => 'multiple']) !!}
            {!! Form::panelRadio('is_review') !!}
{{--            {{ Form::panelText('slug', null, '', ['placeholder' => 'Заполняется автоматически']) }}--}}
        </div>
    </div>

</div>