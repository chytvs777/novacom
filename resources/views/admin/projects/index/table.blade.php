@if($projects->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th>{{ __('admin_labels.slug') }}</th>
            <th>{{ __('admin_labels.created_at') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{ $project->id }}</td>
                <td><a href="{{route ('admin.'. $entity.'.edit', $project)}}" class="pjax-link">{{ $project->title }}</a></td>
                <td>
                    {!! Form::goToUrl($project->url) !!}
                </td>
                <td>{{ $project->created_at }}</td>
                <td class="td-actions">
                    <a href="{{route ('admin.'. $entity.'.edit', $project) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route ('admin.'. $entity.'.delete', $project) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif