@if($options->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th class="td-actions">#</th>
            <th>{{ __('admin_labels.title') }}</th>
            <th>{{ __('admin_labels.type') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($options as $option)
            <tr>
                <td>{{ $option->id }}</td>
                <td><a href="{{ route ('admin.'. $entity.'.edit', $option) }}" class="pjax-link">{{ $option->title }}</a></td>
                <td>{{ __('index.pages.projects.types.'. $option->type) }}</td>
                <td class="td-actions">
                    <a href="{{ route ('admin.'. $entity.'.edit', $option) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route ('admin.'. $entity.'.delete', $option) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif