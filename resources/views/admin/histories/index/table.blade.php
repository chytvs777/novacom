@if($histories->count())
<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <thead>
        <tr>
            <th class="td-actions"></th>
            <th class="td-actions">#</th>
            <th>{{ __('admin_labels.years') }}</th>
            <th></th>
        </tr>
        </thead>
        <tbody class="js-sortable" data-url="{{ action('Admin\HomeController@reorder', ['class' => $histories->getQueueableClass()]) }}">
        @foreach($histories as $history)
            <tr data-id="{{ $history->id }}">
                <td><span class="btn btn-xs btn-white f-z-10"><span class="fa fa-bars"></span></span></td>
                <td>{{ $history->id }}</td>
                <td><a href="{{ route ('admin.'. $entity.'.edit', $history) }}" class="pjax-link">{{ $history->years }}</a></td>
                <td class="td-actions">
                    <a href="{{ route ('admin.'. $entity.'.edit', $history) }}" class="btn btn-sm btn-primary pjax-link" data-toggle="tooltip" title="Редактировать">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route ('admin.'. $entity.'.delete', $history) }}" class="btn btn-sm btn-danger js_panel_delete js_panel_delete-table-row pjax-link" data-toggle="tooltip" title="Удалить">
                        <i class="fa fa-trash-o "></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@else
    <p class="text-muted">Ничего не найдено по данному запросу</p>
@endif