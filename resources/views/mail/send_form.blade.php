@extends('mail.root')

@section('content')
    <h2>Обратная связь с сайта {{  config('app.url') }}</h2>
    <br>
    @if(isset($data['url']))
        <b>Url:</b> {{ $data['url'] }} <br>
    @endif
    @if(isset($data['first_name']))
        <b>Имя:</b> {{ $data['first_name'] }} <br>
    @endif
    @if(isset($data['email']))
        <b>Email:</b> {{ $data['email'] }} <br>
    @endif
    @if(isset($data['company_name']))
        <b>Компания:</b> {{ $data['company_name'] }} <br>
    @endif
    @if(isset($data['position']))
        <b>Должность:</b> {{ $data['position'] }} <br>
    @endif
    @if(isset($data['phone']))
        <b>Телефон:</b> {{ $data['phone'] }} <br>
    @endif
    @if(isset($data['message']))
        <b>Сообщение:</b> {{ $data['message'] }} <br>
    @endif
    @if(isset($data['question']))
        <b>Вопрос:</b> {{ $data['question'] }} <br>
    @endif
@stop