@extends('mail.root')

@section('content')
    <h2>Новое резюме с сайта {{  config('app.url') }}</h2>
    <br>
    @if(isset($data['vacancy']) && $data['vacancy'])
        <b>Вакансия:</b> {{ $data['vacancy'] }} <br>
    @endif
    @if(isset($data['country']) && $data['country'])
        <b>Страна:</b> {{ $data['country'] }} <br>
    @endif
    @if(isset($data['city']) && $data['city'])
        <b>Город:</b> {{ $data['city'] }} <br>
    @endif
    @if(isset($data['first_name']))
        <b>Имя:</b> {{ $data['first_name'] }} <br>
    @endif
    @if(isset($data['last_name']))
        <b>Фамилия:</b> {{ $data['last_name'] }} <br>
    @endif
    @if(isset($data['email']))
        <b>Email:</b> {{ $data['email'] }} <br>
    @endif
    @if(isset($data['company_name']))
        <b>Компания:</b> {{ $data['company_name'] }} <br>
    @endif
    @if(isset($data['phone']))
        <b>Телефон:</b> {{ $data['phone'] }} <br>
    @endif
    @if(isset($data['message']))
        <b>Сообщение:</b> {{ $data['message'] }} <br>
    @endif
@stop