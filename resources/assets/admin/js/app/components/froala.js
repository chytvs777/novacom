function initFroalaDDD() {

    $.fn.initFroala = function () {
        var $froala = this;
        var options = {
            language: 'ru',
            imageDefaultWidth: 0,
            imageUploadURL: typeof urlUploadFroala !== 'undefined' ? urlUploadFroala : '/upload/froala/images',
            htmlAllowComments: false,
            htmlRemoveTags: ['style', 'base'],
            imageInsertButtons: ['imageBack', '|', 'imageUpload', 'imageByURL'],
            // linkList: [{
            //     text: window.location.hostname,
            //     href: window.location.hostname,
            //     target: '_blank',
            //     rel: 'nofollow'
            // },],
            toolbarButtons: [
                'fullscreen',
                '|',
                'bold', 'italic', 'underline', 'strikeThrough', 'color',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
                '|',
                'insertLink', 'insertImage', 'insertVideo', 'insertTable',
                '|',
                'undo', 'redo',
                '|',
                'clearFormatting', 'insertHR',
                '|',
                'html'
            ],
            // colorsBackground: [
            //     '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
            //     '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
            // ],
            // colorsText: [
            //     '#15E67F', '#E3DE8C', '#D8A076', '#D83762', '#76B6D8', 'REMOVE',
            //     '#1C7A90', '#249CB8', '#4ABED9', '#FBD75B', '#FBE571', '#FFFFFF'
            // ],
            // colorsDefaultTab: 'background',
            colorsStep: 6,
            toolbarButtonsXS: [
                'bold', 'italic', 'underline',
                '|',
                'align',
                '|',
                'undo', 'redo',
            ],
            toolbarButtonsSM: [
                'bold', 'italic', 'underline', 'strikeThrough',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL',
                '|',
                'undo', 'redo',  'html'
            ],
            toolbarButtonsMD: [
                'fullscreen', 'bold', 'italic', 'underline', 'strikeThrough',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
                '|',
                'insertLink', 'insertImage', 'insertVideo', 'insertTable', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html'
            ],
            quickInsertButtons: ['image', 'table', 'ul', 'ol'],
        };
        if ($froala.data('images-ajax')) {
            options.imageManagerLoadURL = $froala.data('images-ajax');
            options.imageInsertButtons.push('imageManager');
        }
        if ($froala.data('styles')) {
            options.paragraphStyles = $froala.data('styles');
            options.toolbarButtons.push('paragraphStyle')
        }
        $froala.froalaEditor(options);
        return this;
    };

    $('.js_panel_input-froala1').each(function () {
        $(this).initFroala();
    });
}

function initFroalaDDDSmall() {

    $.fn.initFroala = function () {
        var $froala = this;
        var options = {
            language: 'ru',
            htmlAllowComments: false,
            htmlRemoveTags: ['style', 'base'],
            toolbarButtons: [
                'bold', 'italic', 'underline', 'strikeThrough',
            ],

        };
        $froala.froalaEditor(options);
        return this;
    };

    $('.js_panel_input-froala1-small').each(function () {
        $(this).initFroala();
    });
}

initFroalaDDDSmall();
initFroalaDDD();

window.initFroalaDDD = initFroalaDDD;
window.initFroalaDDDSmall = initFroalaDDDSmall;