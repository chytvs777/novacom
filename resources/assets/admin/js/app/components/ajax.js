$(document).ready(function() {
    $(document).on('change', '.js_ajax-change', ajaxChange);
    $(document).on('submit', '.js_form-ajax', submitFormAjax);

    $(document).on('click', '.js_panel_confirm', confirm);

    function confirm(e) {
        e.preventDefault();
        let $link = $(this);
        let url = $link.attr('href');
        let question = $(this).data('question') || 'Вы действительно хотите удалить?';
        let textSuccess = $(this).data('success') || 'Удаление прошло успешно';
        bootbox.confirm({
            message: question,
            callback: function (result) {
                if (result) {
                    $.get(url, (data) => {
                        if (data.result == 'success') {
                            if ($link.hasClass('js_update-filter')) {
                                $('.js_table-search select:first').trigger('input');
                            } else {
                                $link.closest('tr').remove();
                            }
                            window.showNotification(textSuccess, 'success');
                        } else {
                            window.showNotification(data.message || 'Ошибка', 'error');
                        }
                    })
                }
            }
        })
        return false;
    }

    function submitFormAjax(e) {
        e.preventDefault();
        let $form = $(this);
        $form.trigger('panel-form-ajax-submitted');
        $('.wrapper-spinner').show();
        $form.find('.form-group').removeClass('has-error');
        $form.find('.error-block').html('');
        $form.ajaxSubmit({
            success: function (data) {
                $('.wrapper-spinner').hide();
                if (data.result == 'success') {
                    $form.trigger('panel-form-ajax-success', [data]);
                    if ($form.hasClass('js_form-ajax-redirect')) {
                        let redirectLink = data.link || data.redirect;
                        setTimeout(() => window.location.href = redirectLink, 2000);
                    }
                    if ($form.hasClass('js_form-ajax-popup')) {
                        $form.closest('.modal').modal('hide');
                    }
                    if ($form.hasClass('js_form-ajax-table')) {
                        $('.js_table-search select:first').trigger('input');
                    }
                    if ($form.hasClass('js_form-current-page')) {
                        let $currentPage = $('.js_current-page');
                        if ($currentPage.length) {
                            $currentPage.click();
                        }

                    }
                    if (data.message) {
                        window.showNotification(data.message, 'success');
                    } else {
                        window.showNotification('Данные успешно сохранены', 'success');
                    }
                    if (data.view) {
                        $('.js_table-wrapper').html(data.view);
                    }
                }
            },
            error: function (data) {
                $('.wrapper-spinner').hide();
                $form.trigger('panel-form-ajax-error', [data]);
                $.each(data.responseJSON.errors, function (input, errors) {
                    let inputArray = input.split('.');
                    let $input = $form.find(':input[name="' + input + '"]');
                    if (!$input.length && inputArray.length == 1) {
                        $input = $form.find(':input[name="' + inputArray[0] + '[]"]:eq(' + inputArray[1] + ')');
                    }
                    if (inputArray.length == 2) {
                        $input = $form.find(`:input[name="${inputArray[0]}[${inputArray[1]}]"]`);
                    }
                    if (inputArray.length == 3) {
                        $input = $form.find(`:input[name="${inputArray[0]}[${inputArray[1]}][${inputArray[2]}]"]`);
                    }
                    let text = '';
                    $.each(errors, (i, error) => text += error + "<br>");
                    if ($input.length) {
                        let $wrapper = $input.closest('.form-group');
                        let $error_block = $wrapper.find('.error-block');
                        $wrapper.addClass('has-error');
                        let $help_block = '<span class="help-block">' + text + '</span>';
                        $error_block.append($help_block);
                    } else {
                        window.showNotification(text, 'error');
                    }
                });
                if (data.message) {
                    window.showNotification(data.message, 'error');
                } else {
                    window.showNotification('Ошибка сохранения данных', 'error');
                }
                if (data.view) {
                    $('.js_table-wrapper').html(data.view);
                }
            }
        });
        return false;
    }

    function ajaxChange() {
        let val = $(this).val()
        let url = $(this).data('url')
        let wrapper = $(this).data('wrapper')
        $.get(url, {val: val}, (response) => {
            if (response.html) {
                $(`.${wrapper}`).html(response.html)
            } else {
                $(`.${wrapper}`).html('')
            }
            if (response.message) window.showNotification(response.message, 'error');
        })
    }

});
