$.fn.noAttr = function(name) {
    return this.attr(name) === undefined;
};

window.reindexBlocksLang = (lg, parentName, parentFields, childName, childFields) => {
    return function () {
        let i = 0;
        $(`.js_reindex-${lg}-${parentName}`).each(function () {
            let newName = `${lg}[data][${parentName}][${i}]`;
            parentFields.map(
                parentField => {
                    let attribute = `${newName}[${parentField}]`;
                    let $input = $(this).find(`.js_${lg}-${parentName}-${parentField}`);
                    $input.attr('name', attribute);
                    if ($input.hasClass('js_panel_input-froala-after')) {
                        if ($input.noAttr('disabled')) {
                            $input.removeClass('js_panel_input-froala-after').initFroala();
                        }
                    }
                }
            );
            i++;
        });
    };
};