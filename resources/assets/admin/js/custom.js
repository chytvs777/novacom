$(document).ready(function () {

    $(document).on('input', '.js_table-search :input', searchTable);
    $(document).on('submit', '.js_table-search', searchTable);
    $(document).on('click', '.js_table-pagination a', paginateTable);
    $(document).on('click', '.js_table-reset', resetSearchTable);
    $(document).on('panel-form-ajax-success', '.js_form-ajax', resetAjaxForm);

    $(document).on('click', '#side-menu .pjax-link', activeMenu);
    $(document).pjax('.pjax-link', '#pjax-container', {fragment: '#pjax-container', timeout: 5000});
    
    function activeMenu() {
        $('#side-menu li').not('.sub-menu').removeClass('active');
        $(this).closest('li').addClass('active');
    }

    $('#pjax-container').on('pjax:beforeSend', () => {
        $('.wrapper-spinner').show();
    })

    $('#pjax-container').on('pjax:complete', () => {
        $('.wrapper-spinner').hide();
        window.initSortable();
        window.initFroalaDDD();
        window.initFroalaDDDSmall();
        $('[data-toggle="tooltip"]').tooltip();
        init();
    })

    function resetAjaxForm() {
        if ($(this).hasClass('js_form-ajax-reset') && !$('[name=id]').val()) {
            $('.js_form-ajax-back').click()
        }
    }

    function searchTable(e) {
        e.preventDefault();
        $('.wrapper-spinner').show();
        let $form = $('.js_table-search');
        history.pushState({}, '', '?' + $form.serialize());
        $form.ajaxSubmit({
            success: function (data) {
                $('.wrapper-spinner').hide();
                renderData(data);
            }
        });
        return false;
    }

    function paginateTable(e) {
        e.preventDefault();
        let link = $(this).attr('href');
        history.pushState({}, '', link);

        $('.wrapper-spinner').show();
        $.get(link, function (data) {
            $('.wrapper-spinner').hide();
            renderData(data);
            $.scrollTo($('.ibox-content'), 400);
        });
        return false;
    }

    function renderData(data) {
        let $table = $('.js_table-wrapper');
        let $pagination = $('.js_table-pagination');
        $table.html(data.view);
        $pagination.html(data.pagination);
        $('[data-toggle="tooltip"]').tooltip();
    }

    function resetSearchTable(e) {
        e.preventDefault();
        let $form = $('.js_table-search');
        $form.find('input').not('.js_table-reset-no').val('');
        $form.find('textarea').val('');
        $form.find('select').prop('selectedIndex', 0);
        searchTable(e);
        return false;
    }
    
    function init() {

        $(".js_input-select2").select2({
            allowClear: true
        });
    }

    init()
    
    window.init = init;

    $.fn.initFroala = function () {
        var $froala = this;
        var options = {
            language: 'ru',
            imageUploadURL: typeof urlUploadFroala !== 'undefined' ? urlUploadFroala : '/upload/froala/images',
            imageDefaultWidth: 0,
            htmlAllowComments: false,
            imageInsertButtons: ['imageBack', '|', 'imageUpload', 'imageByURL'],
            linkList: [{
                text: window.location.hostname,
                href: window.location.hostname,
                target: '_blank',
                rel: 'nofollow'
            },],
            toolbarButtons: [
                'fullscreen',
                '|',
                'bold', 'italic', 'underline', 'strikeThrough',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
                '|',
                'insertLink', 'insertImage', 'insertVideo', 'insertTable',
                '|',
                'undo', 'redo',
                '|',
                'clearFormatting', 'selectAll',
                '|',
                'html'
            ],
            toolbarButtonsXS: [
                'bold', 'italic', 'underline',
                '|',
                'align',
                '|',
                'undo', 'redo',
            ],
            toolbarButtonsSM: [
                'bold', 'italic', 'underline', 'strikeThrough',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL',
                '|',
                'undo', 'redo', 'html'
            ],
            toolbarButtonsMD: [
                'fullscreen', 'bold', 'italic', 'underline', 'strikeThrough',
                '|',
                'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote',
                '|',
                'insertLink', 'insertImage', 'insertVideo', 'insertTable', 'undo', 'redo', 'clearFormatting', 'selectAll', 'html'
            ],
            'quickInsertButtons': ['image', 'table', 'ul', 'ol'],
        };
        if ($froala.data('images-ajax')) {
            options.imageManagerLoadURL = $froala.data('images-ajax');
            options.imageInsertButtons.push('imageManager');
        }
        if ($froala.data('styles')) {
            options.paragraphStyles = $froala.data('styles');
            options.toolbarButtons.push('paragraphStyle')
        }

        $froala.froalaEditor(options);
        return this;
    };

    function initFroala() {
        $('.js_input-froala').each(function () {
            $(this).initFroala();
        });
    }

    initFroala();

});