$(document).on('change', '.js_catalog-filter', submitFilter);
$(document).on('submit', '.js_filter__form', submitFilter);
$(document).on('change', '.js_filter__form--vacancies select[name=direction_id]', submitFilter);
$(document).on('change', '.js_filter__form--projects select', submitFilter);
$(document).on('click', '.js_filter__remove-option', removeOption);

function removeOption () {
    let type = $(this).data('type')

    $(this).closest('.js_filter').find(`select[data-type=${type}]`).val('')
    // $(this).closest('.form-search__result-item').remove()

    submitFilter()
}

let updateWrapper = url => {
    history.pushState({}, '', decodeURIComponent(url));

    if (url.indexOf('?') == -1) {
        url += '?';
    }

    $.getJSON(`${url}&is_ajax=1`, function (data) {
        $('.js_filter__wrap').html(data.view);
    });
}

function submitFilter() {
    let $form = $('.js_filter__form');
    let $default = $form.find('[data-default]');
    $default.each(function () {
        if ($(this).data('default') == $(this).val()) $(this).prop('disabled', true)
    })

    $form.find(':input[name="_token"]').prop('disabled', true);

    let newUrl = location.pathname;
    if ($form.serialize().length) {
        newUrl = `${location.pathname}?${$form.serialize()}`;
    }

    $form.find(':input[name="_token"]').prop('disabled', false);

    $default.each(function () {
        if ($(this).data('default') == $(this).val()) $(this).prop('disabled', false)
    })

    updateWrapper(newUrl)
    return false;
}