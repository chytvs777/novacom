$(document).on('click', '.js_show--more a', paginatorMore)

function paginatorMore() {
    let $this = $(this);
    let $wrapContent = $('.js_wrapper--content');
    let $wrapPaginator = $('.js_wrapper--paginator');
    let url = $this.attr('href');
    $this.addClass('btn--loading')
    history.pushState(null, null, url);
    $.get(url + '&is_ajax=1', (response) => {
        $this.removeClass('btn--loading')
        if (response.result == 'success') {
            $wrapContent.append(response.view_content);
            $wrapPaginator.html(response.view_paginator);
        }
    })
    return false;

}