$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on('submit', '.js_form', submitAjaxForm);

function submitAjaxForm() {
    let $form = $(this);
    let $submitButton = $(this).find('button');
    $submitButton.addClass('btn--loading').attr('disabled', true);
    $form.find('.js_form__error').text('')

    $form.ajaxSubmit({
        data: {'is_ajax': 1},
        success: (response) => {
            if (response.message) toastr.success(response.message);
            if ($form.hasClass('js_form--popup-close')) {
                $form.closest('.popup').find('.popup__close').click();
            }
            $form.find('input, textarea').not('[type=submit]').val('')
            $submitButton.removeClass('btn--loading').attr('disabled', false);
        },
        error: (response) => {
            $submitButton.removeClass('btn--loading').attr('disabled', false);
            toastr.error(window.error_message);

            let i = 0;
            $.each(response.responseJSON.errors, function (input, errors) {
                let inputArray = input.split('.');
                let $input = $form.find(':input[name="' + input + '"]');
                if (!$input.length && inputArray.length > 1) {
                    $input = $form.find(':input[name="' + inputArray[0] + '[' + inputArray[1] + ']"]');
                }
                let errorText = errors[0];
                if ($input.length) {
                    let $wrapper = $input.closest('.js_form__wrap');
                    $wrapper.find('.js_form__error').text(errorText);
                }
                i++;
            });
        }
    });
    return false;
}
