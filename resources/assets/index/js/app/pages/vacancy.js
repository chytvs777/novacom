$(document).on('change', '.js_form--vacancy select[name=country_id]', updateCities);

function updateCities () {
    let $form = $(this).closest('.js_form--vacancy')
    let url = $form.data('url-cities')
    let val = $(this).val()
    let $cities = $form.find('[name=city_id]')

    $.get(`${url}/${val}`, (response) => {
        $cities.empty();
        $.each(response, function(value, key) {
            $cities
                .prepend($('<option></option>')
                .attr('value', value)
                .text(key))
        });
        $cities.val('')
    })
}