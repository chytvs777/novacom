var $window = $(window);
var $document = $(document);


function openDroplist() {
    $(this).closest('.drop-list__item').toggleClass('is--active');

};



function slideProgect() {
    if ($('.slide-progect__inner').length) {
        $('.owl-carousel.slide-progect__inner').owlCarousel({
            items: 1,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 2000,
            navigation : true,
            navigationText : ["",""],
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', '']


        })
    }
}

function b_status() {
    if ($('.b-status__inner').length) {
        $('.owl-carousel.b-status__inner').owlCarousel({
            items: 2,
            loop: true,
            margin: 10,
            autoplay: true,
            // autoHeight:true,
            // autoplayHoverPause: true,
            nav: false,
            navText: ['', ''],
            smartSpeed:1750,
            autoplayTimeout: 15000,
            swipe: false,
            draggable: false,
            touchMove: false,
            responsive:{
                0:{
                    items:1
                },
                1199:{
                    items:2
                }
            }
        })
    }
}


function otziv_personal() {
    if ($('.otziv-personal__inner').length) {
        $('.owl-carousel.otziv-personal__inner').owlCarousel({
            items: 2,
            loop: true,
            margin: 10,
            autoplay: true,
            // autoHeight:true,
            // autoplayHoverPause: true,
            nav: false,
            navText: ['', ''],
            smartSpeed:1750,
            autoplayTimeout: 15000,
            swipe: false,
            draggable: false,
            responsive:{
                0:{
                    items:1
                },
                992:{
                    items:2
                }
            }
        })
    }
}

function otsivSlide() {
    if ($('.slider-otziv__inner').length) {
        $('.owl-carousel.slider-otziv__inner').owlCarousel({
            items: 1,
            loop: true,
            margin: 10,
            autoplay: true,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', ''],
            smartSpeed:1750,
            autoplayTimeout: 5000


        })
    }
}

function smallNewsSlider() {
    if ($('.small-news-slider').length) {
        $('.small-news-slider.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            autoplay: true,
            smartSpeed:1750,
            autoplayTimeout: 5000,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: false
            // navText: ['', ''],
            // responsive:{
            //     0:{
            //         items:1
            //     },
            //     500:{
            //         items:2
            //     },
            //     767:{
            //         items:3
            //     },
            //     1000:{
            //         items:4
            //     },
            //     1100:{
            //         items:5
            //     }
            // }
        })
    }
}

function partnersSlide() {
    if ($('.partners__slider').length) {
        $('.slid-partners__inner.owl-carousel').owlCarousel({
            items: 5,
            loop: true,
            margin: 30,
            autoplay: true,
            smartSpeed:1750,
            autoplayTimeout: 5000,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', ''],
            responsive:{
                1190:{
                    items:5,
                    nav: true
                },
                1000:{
                    items:4,
                    nav: false
                },
                767:{
                    items:3,
                    nav: false
                },
                500:{
                    items:2,
                    nav: false
                },
                0:{
                    items:1,
                    nav: false
                }



            }

        })
    }
}


function viewProjectSlider() {
    if ($('.view-our-projects__slider.owl-carousel').length) {
        $('.view-our-projects__slider.owl-carousel').owlCarousel({
            items: 2,
            loop: true,
            margin: 60,
            autoplay: true,
            smartSpeed:1000,
            autoplayTimeout: 4000,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', ''],
            navigation : true,
            navigationText : ["",""],
            responsive:{
                0:{
                    items:1,
                    margin: 10,
                },
                500:{
                    items:1,
                    margin: 10,
                },
                767:{
                    items:1,
                    margin: 10,
                },
                1000:{
                    items:1,
                    margin: 10,
                },
                1100:{
                    items:2,
                    nav: true,

                }
            }

        })
    }
    if ($('.view-our-projects__slider-3cell.owl-carousel').length) {
        $('.view-our-projects__slider-3cell.owl-carousel').owlCarousel({
            items: 3,
            loop: true,
            margin: 20,
            autoplay: true,
            smartSpeed:1000,
            autoplayTimeout: 4000,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', ''],
            navigation : true,
            navigationText : ["",""],
            responsive:{
                0:{
                    items:1,
                    margin: 10,
                },
                500:{
                    items:1,
                    margin: 10,
                },
                767:{
                    items:1,
                    margin: 10,
                },
                1000:{
                    items:2,
                    margin: 10,
                },
                1100:{
                    items:3,
                    nav: true

                }
            }

        })
    }
}


function clientsSlide() {
    if ($('.slid-clients__inner').length) {
        $('.owl-carousel.slid-clients__inner').owlCarousel({
            items: 5,
            loop: true,
            margin: 30,
            autoplay: true,
            smartSpeed:1750,
            autoplayTimeout: 4000,
            // autoHeight:true,
            autoplayHoverPause: true,
            nav: true,
            navText: ['', ''],
            responsive:{
                0:{
                    items:1
                },
                500:{
                    items:2
                },
                767:{
                    items:3
                },
                1000:{
                    items:4
                },
                1100:{
                    items:5
                }
            }

        })
    }
}


function startAnimationIndexCompanu() {
    if ($('.info-companu').length) {
        var show = true;
        var countbox = ".info-companu";
        $(window).on("scroll load resize", function () {

            if (!show) return false;                   // Отменяем показ анимации, если она уже была выполнена

            var w_top = $(window).scrollTop();        // Количество пикселей на которое была прокручена страница
            var e_top = $(countbox).offset().top;     // Расстояние от блока со счетчиками до верха всего документа

            var w_height = $(window).height();        // Высота окна браузера
            var d_height = $(document).height();      // Высота всего документа

            var e_height = $(countbox).outerHeight(); // Полная высота блока со счетчиками

            if (w_top + 350 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height) {

                $('.info-companu__first-circle').addClass('show-circle');

                $(".spincrement").spincrement({
                    thousandSeparator: "",
                    duration: 2200
                });
                show = false;
            }
        });
    }
};

function loadMap(){
    if ($('.js-maps').length) {
        ymaps.ready(function () {
            var myMap = new ymaps.Map('maps', {
                    center: [53.871076, 27.562860],
                    zoom: 17
// controls: []
                }, {
                    searchControlProvider: 'yandex#search'
                }),

// // Создаём макет содержимого.
// MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
// '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
// ),

                myPlacemark1 = new ymaps.Placemark([53.871076, 27.562860], {

// iconContent: '1',
                        balloonContent: 'Минск ',
                        hintContent: 'Минск'
                    },
                    {
                        iconLayout: 'default#image',
                        iconImageHref: '../assets/index/images/map-point.png',
                        iconImageSize: [33, 45],
                        iconImageOffset: [-16, -22]
                    })

//                 myPlacemark2 = new ymaps.Placemark([55.755814, 37.617635], {
//
// // iconContent: '1',
//                         balloonContent: 'Москва',
//                         hintContent: 'Москва'
//                     },
//                     {
//                         iconLayout: 'default#image',
//                         iconImageHref: './images/map-point.png',
//                         iconImageSize: [35, 54],
//                         iconImageOffset: [-17, -27]
//                     }),
//
//                 myPlacemark3 = new ymaps.Placemark([52.518665, 13.402065], {
//
// // iconContent: '1',
//                         balloonContent: 'Берлин',
//                         hintContent: 'Берлин'
//                     },
//                     {
//                         iconLayout: 'default#image',
//                         iconImageHref: './images/map-point.png',
//                         iconImageSize: [35, 54],
//                         iconImageOffset: [-17, -27]
//                     });

            myMap.behaviors
            // Отключаем часть включенных по умолчанию поведений:
            // - drag - перемещение карты при нажатой левой кнопки мыши;
            // - magnifier.rightButton - увеличение области, выделенной правой кнопкой мыши.
                .disable(['scrollZoom', 'rightMouseButtonMagnifier'])
            myMap.geoObjects
                .add(myPlacemark1)
            // .add(myPlacemark2)
            // .add(myPlacemark3);

        });
    };
};

function historySlide() {
    if ($('.history__inner').length) {
        $('.history__body').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            // prevArrow: '.prew-arrow',
            // nextArrow: '.next-arrow',
            fade: true,
            asNavFor: '.history__thumbs',
            accessibility: false,
            // cssEase: 'linear'
//adaptiveHeight: true
        });

        $('.history__thumbs').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.history__body',
            dots: false,
            centerMode: false,
            arrows: false,
            focusOnSelect: true,
            // nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 129 129"><path d="M40.4 121.3c-.8.8-1.8 1.2-3 1.2s-2-.4-2.8-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51C33 12 33 9.3 34.6 7.7c1.6-1.6 4.2-1.6 5.8 0l54 54c1.5 1.5 1.5 4 0 5.7l-54 54z"/></svg></button>',
            // prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 129 129"><path d="M88.6 121.3c.8.8 1.8 1.2 3 1.2s2-.4 2.8-1.2c1.6-1.6 1.6-4.2 0-5.8l-51-51 51-51c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8 0l-54 54c-1.6 1.5-1.6 4 0 5.7l54 54z"/></svg></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 900,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }, {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
        var backs = $('.slick-slide.slick-current.slick-active .history-slide__images').data('item');
        $('.big-bg-slider').css('background-image', 'url('+ backs + '');
    }
}


function patchForImgHistorySlide(){
    var backs = $('.slick-slide.slick-current.slick-active .history-slide__images').data('item');
    $('.big-bg-slider').css('background-image', 'url('+ backs + '');
    $('.big-bg-slider').css('opacity', '0');
    function explode(){
        $('.big-bg-slider').css('opacity', '1');
    }
    setTimeout(explode, 400);
}


function openDropLang() {
    $(this).toggleClass('m-head-data__lang--active');
    $('.nav-menu').removeClass('nav-menu--active');
}

function openMobMenu(){
    $('.nav-menu').addClass('nav-menu--active');
    $('.m-head-data__lang').removeClass('m-head-data__lang--active');
}

function closeMobMenu(){
    $('.nav-menu').removeClass('nav-menu--active');
}

function openDropMobMenu(){
    $('.nav-menu__drop-arrow').removeClass('nav-menu__drop-arrow--active');
    $(this).addClass('nav-menu__drop-arrow--active');
}


// Popup show
function popupShow(event) {

    event.preventDefault();

    var $popup = $('#' + $(this).data('popup'));

    $popup.addClass('popup_show');

    $('body').addClass('overlayed');
    $('.wrapper').addClass('overlayed');
    return false;
}

// popup close on click
function popupCheckCount() {
    if (($('.popup_show').length) < 1) {
        $('body').removeClass('overlayed');
        $('.wrapper').removeClass('overlayed');
    }
}

function popupClose(e) {

    if ($(this).is('.js-popup__close')) {
        e.preventDefault();

        var $popup = $(this).closest('.popup_show');

        $popup.removeClass('popup_show');

        popupCheckCount();
    }
    else if ($(this).is('.popup_show')) {
        var $popup = $(this),
            $popupBox = $popup.find('.popup__box');

        if ($popupBox.has(e.target).length === 0) {

            $popup.removeClass('popup_show');

            popupCheckCount();
        }
    }
}

function tabsAboutPage(){
    var index =  $(this).index('.tabs-clients__item');
    $('.tabs-clients__item').removeClass('tabs-clients__item--active');
    $(this).addClass('tabs-clients__item--active');
    console.log(index + '+');
    $('.page-clients__item').removeClass('page-clients__item--active');
    $('.page-clients__item').eq(index).addClass('page-clients__item--active');
    // $('.page-clients__item').hide;
    // $('.page-clients__item').eq(index).show(500);
}

$window.on('load', function () {

});

$window.on('resize', function () {

});

$document.ready(function () {
    startAnimationIndexCompanu();
    otsivSlide();
    otziv_personal();
    clientsSlide();
    historySlide();
    loadMap();
    slideProgect();
    smallNewsSlider();
    partnersSlide();
    viewProjectSlider();
    b_status();

});

$window.on('scroll', function () {
});

$document.on('click', '.drop-list__content', openDroplist);
$document.on('click', '.m-head-data__lang', openDropLang);
$document.on('click', '.btn-mob', openMobMenu);
$document.on('click', '.mob-clos-menu', closeMobMenu);
$document.on('click', '.nav-menu__drop-arrow', openDropMobMenu);
$document.on('click', '.tabs-clients__item', tabsAboutPage);
$(document).on('click', '*[data-popup]', popupShow);
$(document).on('click touchend', '.js-popup__close, .popup_show', popupClose);


$('.index-prewiews__item').hover(function () {
    $('.index-prewiews__item').addClass('index-prewiews__item--active');
    $('.index-prewiews__item').removeClass('index-prewiews__item--hover');
    $(this).removeClass('index-prewiews__item--active');
    $(this).addClass('index-prewiews__item--hover');

    // setTimeout(function(){
    //     $(this).removeClass('index-prewiews__item--active');
    //     $(this).addClass('index-prewiews__item--hover');
    // }, 0);


});

$('.index-prewiews').mouseleave(function(){
    $('.index-prewiews__item').removeClass('index-prewiews__item--active');
    $('.index-prewiews__item').removeClass('index-prewiews__item--hover');
});

function indexPrewiewsDeleteClass(){
    $(this).removeClass('index-prewiews__item--active');
    $(this).addClass('index-prewiews__item--hover');
}

// $('.history__body').on('afterChange', function(event, slick, index) {
//     patchForImgHistorySlide();
// });


$window.on('touchend', function () {
    return true;
});

