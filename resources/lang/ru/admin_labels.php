<?php

return [
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'full_name' => 'ФИО',
    'type' => 'Тип',
    'status' => 'Статус',
    'middle_name' => 'Отчество',
    'password' => 'Пароль',
    'user_id' => 'Пользователь',
    'email' => 'Email',
    'created_at' => 'Создан',
    'phone' => 'Телефон',
	'title' => 'Заголовок',
	'slug' => 'Сссылка',
	'meta_title' => 'Meta title',
	'meta_description' => 'Meta description',
	'content' => 'Описание',
	'content_short' => 'Краткое описание',
	'announcement' => 'Анонс',
	'review_text' => 'Текст отзыва',
	'review_author' => 'Автор отзыва',
	'reference_information' => 'Справочная информация',
	'position' => 'Должность',
	'phone_home' => 'Офисн. телефон',
	'phone_mobile' => 'Моб. телефон',
	'is_leadership' => 'Руководитель',
	'is_press_service' => 'Пресс служба',
	'is_show_news' => 'Показывать на главной странице новостей',
	'press_service_id' => 'Пресс-служба',
	'direction_id' => 'Направление',
	'is_hot' => 'Горячая',
	'box_one_title' => 'Заголовок',
	'box_one_description' => 'Описание',
	'description' => 'Описание',
	'box_two_title' => 'Заголовок',
	'box_two_description' => 'Описание',
	'is_review' => 'Показать отзыв',
	'command_id' => 'Сотрудник',
	'customer_name' => 'Заказчика',
	'customer_info' => 'Информация о заказчике',
	'customer_site' => 'Сайт заказчика',
	'options' => 'Значения',
	'content_title' => 'Заголовок контента',
	'content_sub_title' => 'Доп. заголовок контента',
	'content_one' => 'Контент левая колонка',
	'content_two' => 'Контент правая колонка',
	'years' => 'Года',
	'icon' => 'Иконка',
	'customer_type' => 'Тип заказчика',
	'email_for_notification' => 'E-mail для уведомлений',
	'facebook' => 'facebook',
	'in' => 'LinkedIN',
	'review_name' => 'Имя автора',
	'review_position' => 'Должность автора',
	'country_id' => 'Страна',
];