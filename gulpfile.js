let elixir = require('secret-elixir');

elixir.config.sourcemaps = false;
elixir.config.css.base64.enabled = true;

let
    gulp = require('gulp'),
    spritesmith = require('gulp.spritesmith'),
    pug = require('gulp-pug'),
    srcPug = './resources/assets/index/pages/*.pug',
    resources = './resources/assets/',
    public = './public/assets/',
    node = './node_modules/',
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    size = require('gulp-size'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    proxy = 'http://novacom.git',
    srcLess = [
        `${node}toastr/toastr.less`,
        `${resources}index/less/owl.carousel.min.css`,
        `${resources}index/less/owl.theme.default.min.css`,
        `${resources}index/less/slick.less`,
        `${resources}index/less/slick-theme.less`,
        `${resources}index/less/style.less`,
    ];

// png sprite
gulp.task('sprite', () => {
    let spriteData =
        gulp.src(`${resources}index/images/sprite/*.png`)
            .pipe(spritesmith({
                retinaSrcFilter: [`${resources}index/images/sprite/*@2x.png`],
                imgName: 'sprite.png',
                imgPath: '../images/sprite/sprite.png',
                retinaImgName: 'sprite@2x.png',
                retinaImgPath: '../images/sprite/sprite@2x.png',
                cssName: 'sprite.less',
                cssFormat: 'less',
                padding: 6,
                algorithm: 'top-down'
            }));
    spriteData.img.pipe(gulp.dest(`${public}index/images/sprite/`));
    spriteData.css.pipe(gulp.dest(`${public}index/styles/components/`));
});

gulp.task('sprite:svg', function (callback) {
    const svgstore = require('gulp-svgstore');
    const svgmin = require('gulp-svgmin');
    const cheerio = require('gulp-cheerio');
    console.log('---------- Сборка SVG спрайта');
    return gulp.src('./resources/assets/index/images/svg/*.svg')
        .pipe(svgmin(function (file) {
            return {
                plugins: [{
                    cleanupIDs: {
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore({inlineSvg: true}))
        .pipe(cheerio({
            run: function ($) {
                $('svg').attr('style', 'display:none');
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        .pipe(rename('sprite-svg.svg'))
        .pipe(size({
            title: 'Размер',
            showFiles: true,
            showTotal: false,
        }))
        .pipe(gulp.dest('./public/assets/index/images/svg/'));
});

// pug to html
gulp.task('pug', function () {
    return gulp.src(srcPug)
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('./public/markup'));
});

gulp.task('lesss', function() {
    return gulp.src(srcLess)
        .pipe(less())
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./public/assets/index/css'));
});

elixir(mix => {

    mix
    // Index
        .task('lesss', [
            `${resources}index/styles/style.less`,
            `${resources}index/less/**/*.less`
        ])

        .scripts([
            `${node}jquery/dist/jquery.min.js`,
            `${node}toastr/toastr.js`,
            `${node}jquery-form/src/jquery.form.js`,
            `${resources}index/js/jquery.spincrement.js`,
            `${resources}index/js/owl.carousel.min.js`,
            `${resources}index/js/slick.js`,
            `${resources}index/js/custom.js`,
        ], `${public}index/js/main.js`)

        .task('pug', [
            `${resources}index/pages/*.pug`,
            `${resources}index/blocks/**/*.pug`,
            `${resources}index/templates/**/*.pug`
        ])

        .task('sprite:svg')
        //
        // .browserify(`${resources}index/js/app.js`, `${public}index/js/app.js`)
        //
        .copy(`${resources}index/fonts`, `${public}index/fonts`)
        .copy(`${resources}index/fonts`, './public/build/assets/index/fonts')

        .copy(`${resources}index/images`, `${public}index/images`)
        .copy(`${resources}index/images`, './public/build/assets/index/images')


    //Admin
    // .less([
    //     `${resources}admin/less/styles.less`,
    // ], `${public}admin/css/styles.css`)
    //
    // .scripts([
    //     `${node}@sergiovilar/jquery-pjax/jquery.pjax.js`,
    //     `${node}jquery.scrollto/jquery.scrollTo.min.js`
    // ], `${public}admin/js/libs.js`)
    //
    .browserify(`${resources}admin/js/index.js`, `${public}admin/js/scripts.js`)


    // mix.browserSync({
    //     files: [
    //         `./public/assets/**/**/*`,
    //         `./public/markup/*`
    //     ],
    //     proxy: proxy,
    //     logConnections: false,
    //     reloadOnRestart: true,
    //     notify: false,
    //     open: true
    // })
});
