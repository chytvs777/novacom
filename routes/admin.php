<?php

Route::auth();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'UserController@index')->name('home');

    Route::post('reorder', 'HomeController@reorder');

    //Users
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::get('create', 'UserController@create')->name('create');
        Route::post('store', 'UserController@store')->name('store');
        Route::group(['prefix' => '{user}'], function () {
            Route::get('edit', 'UserController@edit')->name('edit');
            Route::get('delete', 'UserController@delete')->name('delete');
        });
    });

    //Articles
    Route::group(['prefix' => 'articles', 'as' => 'articles.', 'namespace' => 'Article'], function () {
        Route::get('/', 'ArticleController@index')->name('index');
        Route::get('create', 'ArticleController@create')->name('create');
        Route::post('store', 'ArticleController@store')->name('store');
        Route::group(['prefix' => '{article}'], function () {
            Route::get('edit', 'ArticleController@edit')->name('edit');
            Route::get('delete', 'ArticleController@delete')->name('delete');
        });
    });

    //Vacancies
    Route::group(['prefix' => 'vacancies', 'as' => 'vacancies.', 'namespace' => 'Vacancy'], function () {
        Route::group(['prefix' => 'directions', 'as' => 'directions.'], function () {
            Route::get('/', 'DirectionController@index')->name('index');
            Route::get('create', 'DirectionController@create')->name('create');
            Route::post('store', 'DirectionController@store')->name('store');
            Route::group(['prefix' => '{direction}'], function () {
                Route::get('edit', 'DirectionController@edit')->name('edit');
                Route::get('delete', 'DirectionController@delete')->name('delete');
            });
        });

        Route::get('/', 'VacancyController@index')->name('index');
        Route::get('create', 'VacancyController@create')->name('create');
        Route::post('store', 'VacancyController@store')->name('store');
        Route::group(['prefix' => '{vacancy}'], function () {
            Route::get('edit', 'VacancyController@edit')->name('edit');
            Route::get('delete', 'VacancyController@delete')->name('delete');
        });
    });

    //Projects
    Route::group(['prefix' => 'projects', 'as' => 'projects.', 'namespace' => 'Project'], function () {
        Route::group(['prefix' => 'options', 'as' => 'options.'], function () {
            Route::get('/', 'OptionController@index')->name('index');
            Route::get('create', 'OptionController@create')->name('create');
            Route::post('store', 'OptionController@store')->name('store');
            Route::group(['prefix' => '{option}'], function () {
                Route::get('edit', 'OptionController@edit')->name('edit');
                Route::get('delete', 'OptionController@delete')->name('delete');
            });
        });

        Route::get('/', 'ProjectController@index')->name('index');
        Route::get('create', 'ProjectController@create')->name('create');
        Route::post('store', 'ProjectController@store')->name('store');
        Route::group(['prefix' => '{project}'], function () {
            Route::get('edit', 'ProjectController@edit')->name('edit');
            Route::get('delete', 'ProjectController@delete')->name('delete');
        });
    });

    //Commands
    Route::group(['prefix' => 'commands', 'as' => 'commands.'], function () {
        Route::get('/', 'CommandController@index')->name('index');
        Route::get('create', 'CommandController@create')->name('create');
        Route::post('store', 'CommandController@store')->name('store');
        Route::group(['prefix' => '{command}'], function () {
            Route::get('edit', 'CommandController@edit')->name('edit');
            Route::get('delete', 'CommandController@delete')->name('delete');
        });
    });

    //Histories
    Route::group(['prefix' => 'histories', 'as' => 'histories.'], function () {
        Route::get('/', 'HistoryController@index')->name('index');
        Route::get('create', 'HistoryController@create')->name('create');
        Route::post('store', 'HistoryController@store')->name('store');
        Route::group(['prefix' => '{history}'], function () {
            Route::get('edit', 'HistoryController@edit')->name('edit');
            Route::get('delete', 'HistoryController@delete')->name('delete');
        });
    });

    //Texts
    Route::group(['prefix' => 'texts', 'as' => 'texts.'], function () {
        Route::get('/', 'TextController@index')->name('index');
        Route::post('store', 'TextController@store')->name('store');
        Route::group(['prefix' => '{text}'], function () {
            Route::get('edit', 'TextController@edit')->name('edit');
        });
    });

//    //Pages
//    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
//        Route::get('/', 'PageController@index')->name('index');
//        Route::get('create', 'PageController@create')->name('create');
//        Route::post('store', 'PageController@store')->name('store');
//        Route::group(['prefix' => '{page}'], function () {
//            Route::get('edit', 'PageController@edit')->name('edit');
//            Route::get('delete', 'PageController@delete')->name('delete');
//        });
//    });

    //Settings
    Route::group(['prefix' => 'settings', 'as' => 'settings.', 'namespace' => 'Setting'], function () {
        Route::get('edit', 'SettingController@edit')->name('edit');
        Route::post('store', 'SettingController@store')->name('store');

        Route::group(['prefix' => 'translation', 'as' => 'translation.'], function () {
            Route::get('/', 'TranslationController@index')->name('index');
            Route::post('/', 'TranslationController@store')->name('store');
        });

        //Countries
        Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
            Route::get('/', 'CountryController@index')->name('index');
            Route::get('create', 'CountryController@create')->name('create');
            Route::post('store', 'CountryController@store')->name('store');
            Route::group(['prefix' => '{country}'], function () {
                Route::get('edit', 'CountryController@edit')->name('edit');
                Route::get('delete', 'CountryController@delete')->name('delete');
            });
        });

        //Cities
        Route::group(['prefix' => 'cities', 'as' => 'cities.'], function () {
            Route::get('/', 'CityController@index')->name('index');
            Route::get('create', 'CityController@create')->name('create');
            Route::post('store', 'CityController@store')->name('store');
            Route::group(['prefix' => '{city}'], function () {
                Route::get('edit', 'CityController@edit')->name('edit');
                Route::get('delete', 'CityController@delete')->name('delete');
            });
        });
    });
});