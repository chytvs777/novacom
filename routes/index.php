<?php

Route::get('/', 'HomeController@index')->name('home')->middleware('checkBrowserLocale');
Route::post('send-form', 'HomeController@sendForm')->name('send.form');

//Seo
//Route::get('sitemap.xml', 'YmlController@sitemap')->name('sitemap');
//Route::get('robots.txt', 'YmlController@robotsTxt');

//Company
Route::group(['prefix' => 'company'], function () {
    Route::get('/', 'PageController@company')->name('company');

    Route::get('about', 'PageController@about')->name('about');
    Route::get('leadership', 'PageController@leadership')->name('leadership');
    Route::get('history', 'PageController@history')->name('history');

    //Articles
    Route::group(['prefix' => 'blog', 'as' => 'articles.'], function () {
        Route::get('/', 'ArticleController@index')->name('index');
        Route::get('{article}', 'ArticleController@show')->name('show');
    });
});

//Career
Route::group(['prefix' => 'career'], function () {
    Route::get('/', 'PageController@career')->name('career');

    Route::get('work', 'PageController@work')->name('work');

    //Vacancies
    Route::group(['prefix' => 'vacancies', 'as' => 'vacancies.'], function () {
        Route::get('/', 'VacancyController@index')->name('index');
        Route::get('apply-now', 'VacancyController@applyNow')->name('applyNow');
        Route::post('form', 'VacancyController@form')->name('form');
        Route::get('get-cities/{country?}', 'VacancyController@getCities')->name('getCities');
        Route::get('{vacancy}', 'VacancyController@show')->name('show');
    });
});

//Projects
Route::group(['prefix' => 'projects', 'as' => 'projects.'], function () {
    Route::get('/', 'ProjectController@index')->name('index');

    Route::group(['prefix' => 'solutions', 'as' => 'solutions.'], function () {
        Route::get('/', 'ProjectController@solutions')->name('index');
        Route::get('{project}', 'ProjectController@show')->name('show');
    });
});

Route::get('contacts', 'PageController@contacts')->name('contacts');


//Route::get('{page}', 'PageController@index')->name('page');