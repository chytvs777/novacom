<?php

//--Admin--//
Breadcrumbs::register('admin.home', function($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.home'));
});

//Users
Breadcrumbs::register('admin.users.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.users.title'), route('admin.users.index'));
});
Breadcrumbs::register('admin.users.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push(__('admin.users.create'), route('admin.users.create'));
});
Breadcrumbs::register('admin.users.edit', function($breadcrumbs, $user) {
    $breadcrumbs->parent('admin.users.index');
    $breadcrumbs->push(__('admin.users.edit'). ' '.  $user->first_name, route('admin.users.edit', $user));
});

//Articles
Breadcrumbs::register('admin.articles.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.articles.title'), route('admin.articles.index'));
});
Breadcrumbs::register('admin.articles.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.articles.index');
    $breadcrumbs->push(__('admin.articles.create'), route('admin.articles.create'));
});
Breadcrumbs::register('admin.articles.edit', function($breadcrumbs, $article) {
    $breadcrumbs->parent('admin.articles.index');
    $breadcrumbs->push(__('admin.articles.edit'). ' '.  $article->title, route('admin.articles.edit', $article));
});

//Commands
Breadcrumbs::register('admin.commands.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.commands.title'), route('admin.commands.index'));
});
Breadcrumbs::register('admin.commands.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.commands.index');
    $breadcrumbs->push(__('admin.commands.create'), route('admin.commands.create'));
});
Breadcrumbs::register('admin.commands.edit', function($breadcrumbs, $command) {
    $breadcrumbs->parent('admin.commands.index');
    $breadcrumbs->push(__('admin.commands.edit'). ' '.  $command->full_name, route('admin.commands.edit', $command));
});

//Histories
Breadcrumbs::register('admin.histories.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.histories.title'), route('admin.histories.index'));
});
Breadcrumbs::register('admin.histories.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.histories.index');
    $breadcrumbs->push(__('admin.histories.create'), route('admin.histories.create'));
});
Breadcrumbs::register('admin.histories.edit', function($breadcrumbs, $history) {
    $breadcrumbs->parent('admin.histories.index');
    $breadcrumbs->push(__('admin.histories.edit'). ' '.  $history->years, route('admin.histories.edit', $history));
});

//Pages
Breadcrumbs::register('admin.pages.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.pages.title'), route('admin.pages.index'));
});
Breadcrumbs::register('admin.pages.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.pages.index');
    $breadcrumbs->push(__('admin.pages.create'), route('admin.pages.create'));
});
Breadcrumbs::register('admin.pages.edit', function($breadcrumbs, $page) {
    $breadcrumbs->parent('admin.pages.index');
    $breadcrumbs->push(__('admin.pages.edit'). ' '.  $page->title, route('admin.pages.edit', $page));
});

//Vacancies
Breadcrumbs::register('admin.vacancies.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.vacancies.title'), route('admin.vacancies.index'));
});
Breadcrumbs::register('admin.vacancies.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.vacancies.index');
    $breadcrumbs->push(__('admin.vacancies.create'), route('admin.vacancies.create'));
});
Breadcrumbs::register('admin.vacancies.edit', function($breadcrumbs, $vacancy) {
    $breadcrumbs->parent('admin.vacancies.index');
    $breadcrumbs->push(__('admin.vacancies.edit'). ' '.  $vacancy->title, route('admin.vacancies.edit', $vacancy));
});
//Directions
Breadcrumbs::register('admin.vacancies.directions.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.vacancies.index');
    $breadcrumbs->push(__('admin.vacancies.directions.title'), route('admin.vacancies.directions.index'));
});
Breadcrumbs::register('admin.vacancies.directions.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.vacancies.directions.index');
    $breadcrumbs->push(__('admin.vacancies.directions.create'), route('admin.vacancies.directions.create'));
});
Breadcrumbs::register('admin.vacancies.directions.edit', function($breadcrumbs, $direction) {
    $breadcrumbs->parent('admin.vacancies.directions.index');
    $breadcrumbs->push(__('admin.vacancies.directions.edit'). ' '.  $direction->title, route('admin.vacancies.directions.edit', $direction));
});

//Projects
Breadcrumbs::register('admin.projects.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.projects.title'), route('admin.projects.index'));
});
Breadcrumbs::register('admin.projects.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push(__('admin.projects.create'), route('admin.projects.create'));
});
Breadcrumbs::register('admin.projects.edit', function($breadcrumbs, $project) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push(__('admin.projects.edit'). ' '.  $project->title, route('admin.projects.edit', $project));
});
//Options
Breadcrumbs::register('admin.projects.options.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.index');
    $breadcrumbs->push(__('admin.projects.options.title'), route('admin.projects.options.index'));
});
Breadcrumbs::register('admin.projects.options.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.projects.options.index');
    $breadcrumbs->push(__('admin.projects.options.create'), route('admin.projects.options.create'));
});
Breadcrumbs::register('admin.projects.options.edit', function($breadcrumbs, $option) {
    $breadcrumbs->parent('admin.projects.options.index');
    $breadcrumbs->push(__('admin.projects.options.edit'). ' '.  $option->title, route('admin.projects.options.edit', $option));
});

//Texts
Breadcrumbs::register('admin.texts.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(trans('admin.texts.title'), route('admin.texts.index'));
});
Breadcrumbs::register('admin.texts.edit', function($breadcrumbs, $text) {
    $breadcrumbs->parent('admin.texts.index');
    $breadcrumbs->push(trans('admin.texts.edit'). ' '.  $text->key, route('admin.texts.edit', $text));
});

//Settings
Breadcrumbs::register('admin.settings.edit', function($breadcrumbs) {
    $breadcrumbs->parent('admin.home');
    $breadcrumbs->push(__('admin.settings.edit'), route('admin.settings.edit'));
});
Breadcrumbs::register('admin.settings.translation.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.settings.edit');
    $breadcrumbs->push(__('admin.settings.translation.index'), route('admin.settings.translation.index'));
});
//Countries
Breadcrumbs::register('admin.settings.countries.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.settings.edit');
    $breadcrumbs->push(__('admin.settings.countries.title'), route('admin.settings.countries.index'));
});
Breadcrumbs::register('admin.settings.countries.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.settings.countries.index');
    $breadcrumbs->push(__('admin.settings.countries.create'), route('admin.settings.countries.create'));
});
Breadcrumbs::register('admin.settings.countries.edit', function($breadcrumbs, $country) {
    $breadcrumbs->parent('admin.settings.countries.index');
    $breadcrumbs->push(__('admin.settings.countries.edit'). ' '.  $country->title, route('admin.settings.countries.edit', $country));
});
//Cities
Breadcrumbs::register('admin.settings.cities.index', function($breadcrumbs) {
    $breadcrumbs->parent('admin.settings.edit');
    $breadcrumbs->push(__('admin.settings.cities.title'), route('admin.settings.cities.index'));
});
Breadcrumbs::register('admin.settings.cities.create', function($breadcrumbs) {
    $breadcrumbs->parent('admin.settings.cities.index');
    $breadcrumbs->push(__('admin.settings.cities.create'), route('admin.settings.cities.create'));
});
Breadcrumbs::register('admin.settings.cities.edit', function($breadcrumbs, $city) {
    $breadcrumbs->parent('admin.settings.cities.index');
    $breadcrumbs->push(__('admin.settings.cities.edit'). ' '.  $city->title, route('admin.settings.cities.edit', $city));
});