<?php

Breadcrumbs::register('index.home', function($breadcrumbs) {
    $breadcrumbs->push(__('index.pages.home.title'), route('admin.home'));
});


//Company
Breadcrumbs::register('index.company', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(__('index.pages.company.title'), route('index.company'));
});
Breadcrumbs::register('index.about', function($breadcrumbs) {
    $breadcrumbs->parent('index.company');
    $breadcrumbs->push(__('index.pages.company.about.title'), route('index.about'));
});
Breadcrumbs::register('index.leadership', function($breadcrumbs) {
    $breadcrumbs->parent('index.company');
    $breadcrumbs->push(__('index.pages.company.leadership.title'), route('index.leadership'));
});
Breadcrumbs::register('index.history', function($breadcrumbs) {
    $breadcrumbs->parent('index.company');
    $breadcrumbs->push(__('index.pages.company.history.title'), route('index.history'));
});
//Articles
Breadcrumbs::register('index.articles.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.company');
    $breadcrumbs->push(__('index.pages.company.articles.title'), route('index.articles.index'));
});
Breadcrumbs::register('index.articles.show', function($breadcrumbs, $article) {
    $breadcrumbs->parent('index.articles.index');
    $breadcrumbs->push('', null);
});


//Career
Breadcrumbs::register('index.career', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(__('index.pages.career.title'), route('index.career'));
});
Breadcrumbs::register('index.work', function($breadcrumbs) {
    $breadcrumbs->parent('index.career');
    $breadcrumbs->push(__('index.pages.career.work.title'), route('index.work'));
});
Breadcrumbs::register('index.vacancies.applyNow', function($breadcrumbs) {
    $breadcrumbs->parent('index.career');
    $breadcrumbs->push(__('index.pages.career.apply_now.title'), route('index.vacancies.applyNow'));
});
//Vacancies
Breadcrumbs::register('index.vacancies.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.career');
    $breadcrumbs->push(__('index.pages.career.vacancies.title'), route('index.vacancies.index'));
});
Breadcrumbs::register('index.vacancies.show', function($breadcrumbs, $vacancy) {
    $breadcrumbs->parent('index.vacancies.index');
    $breadcrumbs->push('', null);
});


//Projects
Breadcrumbs::register('index.projects.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(__('index.pages.projects.title'), route('index.projects.index'));
});
//Vacancies
Breadcrumbs::register('index.projects.solutions.index', function($breadcrumbs) {
    $breadcrumbs->parent('index.projects.index');
    $breadcrumbs->push(__('index.pages.projects.solutions.title'), route('index.projects.solutions.index'));
});
Breadcrumbs::register('index.projects.solutions.show', function($breadcrumbs, $vacancy) {
    $breadcrumbs->parent('index.projects.solutions.index');
    $breadcrumbs->push('', null);
});


//Contacts
Breadcrumbs::register('index.contacts', function($breadcrumbs) {
    $breadcrumbs->parent('index.home');
    $breadcrumbs->push(__('index.pages.contacts.title'), route('index.contacts'));
});
