<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(CommandsSeeder::class);
        $this->call(ArticlesSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(TextsSeeder::class);
        $this->call(DirectionsSeeder::class);
        $this->call(VacanciesSeeder::class);
        $this->call(OptionsSeeder::class);
        $this->call(ProjectsSeeder::class);
        $this->call(HistoriesSeeder::class);
    }
}
