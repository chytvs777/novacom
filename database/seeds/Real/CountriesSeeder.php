<?php

use App\Models\Setting\Country;
use App\Models\Setting\City;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    public function run()
    {
        Country::query()->delete();
        City::query()->delete();

        $fakerCountries = [
            [
                'ru' => [
                    'title' => 'Беларусь',
                ],
                'en' => [
                    'title' => 'Belarus',
                ],
                'cities' => [
                    [
                        'ru' => [
                            'title' => 'Минск',
                        ],
                        'en' => [
                            'title' => 'Minsk',
                        ],
                    ],
                    [
                        'ru' => [
                            'title' => 'Гомель',
                        ],
                        'en' => [
                            'title' => 'Gomel',
                        ],
                    ],
                ],
            ],
            [
                'ru' => [
                    'title' => 'Россия',
                ],
                'en' => [
                    'title' => 'Russia',
                ],
                'cities' => [
                    [
                        'ru' => [
                            'title' => 'Москва',
                        ],
                        'en' => [
                            'title' => 'Moscow',
                        ],
                    ],
                ],
            ],
        ];

        foreach ($fakerCountries as $item) {
            $country = Country::create(array_except($item, 'cities'));
            foreach ($item['cities'] as $city) {
                City::create($city + ['country_id' => $country->id]);
            }
        }

    }
}
