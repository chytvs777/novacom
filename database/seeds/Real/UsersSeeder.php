<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        User::query()->delete();

        User::create([
            'first_name' => 'bylink',
			'email' => 'info@bylink.by',
            'password' => 'dapaedi',
        ]);

        User::create([
            'first_name' => 'Novacom',
			'email' => 'info@nvcm.net',
            'password' => 'infoN',
        ]);

    }
}
