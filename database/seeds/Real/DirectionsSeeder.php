<?php

use App\Models\Vacancy\Direction;
use Illuminate\Database\Seeder;

class DirectionsSeeder extends Seeder
{
    public function run()
    {
        Direction::query()->delete();

        $faker = [
            [
                'ru' => [
                    'title' => 'Cross device department',
                ],
                'en' => [
                    'title' => '',
                ],
            ],
            [
                'ru' => [
                    'title' => 'Embedded Systems Department',
                ],
                'en' => [
                    'title' => '',
                ],
            ],
            [
                'ru' => [
                    'title' => 'SPA',
                ],
                'en' => [
                    'title' => '',
                ],
            ]
        ];

        foreach ($faker as $order => $item) {
            Direction::create($item + ['order' => $order]);
        }
    }

}
