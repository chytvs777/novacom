<?php

use App\Models\Setting\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    public function run()
    {
        Setting::query()->delete();

        $setting = Setting::create([
            'email_for_notification' => 'nedbik@mail.ru',
            'facebook' => '#',
            'in' => '#',
        ]);

        $setting->syncImages($this->images());
    }

    protected function images()
    {
        $imagesClients = [];
        $imagesTechnologiesOne = [];
        $imagesTechnologiesTwo = [];

        for ($i = 0; $i < 8; $i++) {
            $imagesClients[] = public_path('assets' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'ico-clients'. rand(1, 5). '.png');
        }

        for ($i = 0; $i < 6; $i++) {
            $imagesTechnologiesOne[] = public_path('assets' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . (rand(0, 1) ? 'ico-project-java.png' : 'ico-project-NOsql.png'));
        }

        for ($i = 0; $i < 3; $i++) {
            $imagesTechnologiesTwo[] = public_path('assets' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . (rand(0, 1) ? 'ico-project-oracle.png' : 'ico-project-ibm.png'));
        }

        $data = [
            'images' => [
                Setting::IMAGE_TYPE_CLIENTS => [
                    'path' => $imagesClients,
                    'main' => 1,
                    'order' => 1,
                ],
                Setting::IMAGE_TYPE_TECHNOLOGIES_ONE => [
                    'path' => $imagesTechnologiesOne,
                    'main' => 1,
                    'order' => 1,
                ],
                Setting::IMAGE_TYPE_TECHNOLOGIES_TWO => [
                    'path' => $imagesTechnologiesTwo,
                    'main' => 1,
                    'order' => 1,
                ],
            ],
        ];
        return $data;
    }
}
