<?php

use App\Models\Project\Option;
use Illuminate\Database\Seeder;

class OptionsSeeder extends Seeder
{
    public function run()
    {
        Option::query()->delete();

        $faker = [
            Option::TYPE_INDUSTRY => [
                [
                    'ru' => [
                        'title' => 'Госорганы',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Услуги',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Финансы',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Образование',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Ритейл',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Бизнес',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Строительство',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Холдинги',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Банк',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'E-commerce',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
            ],
            Option::TYPE_TECHNOLOGY => [
                [
                    'ru' => [
                        'title' => 'SQL',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Java',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Pithon',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'C#',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Cassandra',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'C/C++',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
            ],
            Option::TYPE_EXPERTISE => [
                [
                    'ru' => [
                        'title' => 'Высоконагруженные системы',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Распределенные системы',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Портальные решения',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Реестры',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Архив',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Интеграция',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'loT',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
                [
                    'ru' => [
                        'title' => 'Документооборот',
                    ],
                    'en' => [
                        'title' => '',
                    ],
                ],
            ],
        ];

        foreach ($faker as $type => $items) {
            foreach ($items as $order => $item) {
                $option = Option::create($item + [
                    'order' => $order,
                    'type' => $type,
                ]);

                if ($type == Option::TYPE_TECHNOLOGY) $option->syncImages($this->image());
            }
        }
    }

    protected function image()
    {
        $name = rand(0, 1) ? 'ico-project-java.png' : 'ico-project-sql.png';

        $data = [
            'images' => [
                Option::IMAGE_TYPE_IMAGE => [
                    'path' => [
                        0 => public_path('assets' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $name),
                    ],
                    'main' => 1,
                    'order' => 1,
                ],
            ],
        ];

        return $data;
    }
}
