<?php

use App\Models\History;
use Illuminate\Database\Seeder;

class HistoriesSeeder extends Seeder
{
    public function run()
    {
        History::query()->delete();

        $faker = ['2010-2011', '2011-2012', '2012-2013', '2013-2014', '2014-2015', '2015-2016', '2016-2017'];


        foreach ($faker as $order => $item) {
            $command = History::create([
                    'years' => $item,
                    'content' => rand(0, 1) ? 'Основание компании Novacom  и формирование первой команды разработчиков' : 'Основание компании Novacom  и формирование первой'
                ]
                + ['order' => $order]);

            $command->syncImages($this->images(rand(0, 1) ? '404.jpg' : 'bg-history.jpg'));
        }
    }

    protected function images($img)
    {
        $data = [
            'images' => [
                History::IMAGE_TYPE_IMAGE => [
                    'path' => [
                        0 => public_path('assets' . DIRECTORY_SEPARATOR . 'index' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . $img),
                    ],
                    'main' => 1,
                    'order' => 1,
                ],
            ],
        ];

        return $data;
    }
}
