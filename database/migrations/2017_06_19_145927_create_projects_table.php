<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('is_review');
            $table->string('slug');
            $table->string('customer_type');
            $table->integer('order')->unsigned();
            $table->timestamps();
        });

        Schema::create('project_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('title');
            $table->text('content_short');
            $table->string('customer_name');
            $table->string('customer_info');
            $table->string('customer_site');
            $table->string('review_name');
            $table->string('review_position');
            $table->text('review_text');
            $table->longText('content');
            $table->longText('data');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('locale')->index();
            $table->unique(['project_id', 'locale']);
        });

        Schema::create('option_project', function (Blueprint $table) {
            $table->integer('option_id')->unsigned()->index();
            $table->foreign('option_id')->references('id')->on('options')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('project_id')->unsigned()->index();
            $table->foreign('project_id')->references('id')->on('projects')->onUpdate('cascade')->onDelete('cascade');
            $table->primary(['option_id', 'project_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_project');
        Schema::dropIfExists('project_translations');
        Schema::dropIfExists('projects');
    }
}
