<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\TranslationLoader\LanguageLine;

class CreateLanguageLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('language_lines');

        Schema::create('language_lines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->index('group');
            $table->string('key');
            $table->text('text');
            $table->timestamps();
        });

        $messages = array_dot(trans('index_file'));
        foreach ($messages as $key => $message) {
            $text = [];
            foreach (\LaravelLocalization::getSupportedLocales() as $lang => $localization) {
                app()->setLocale($lang);
                $text[$lang] = trans('index_file.' . $key);
            }
            app()->setLocale('ru');

            LanguageLine::create([
                'group' => 'index',
                'key' => $key,
                'text' => $text,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_lines');
    }
}
