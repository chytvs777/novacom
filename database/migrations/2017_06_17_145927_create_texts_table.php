<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('texts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->timestamps();
        });

        Schema::create('text_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('text_id')->unsigned();
            $table->foreign('text_id')->references('id')->on('texts')->onDelete('cascade');
            $table->string('title');
            $table->text('content_short');
            $table->longText('content');
            $table->longText('data');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('locale')->index();
            $table->unique(['text_id', 'locale']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_translations');
        Schema::dropIfExists('texts');
    }
}
