<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('command_id')->unsigned()->nullable();
            $table->foreign('command_id')->references('id')->on('commands')->onDelete('set null');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('article_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->string('title');
            $table->text('content_short');
            $table->text('announcement');
            $table->text('review_text');
            $table->text('review_author');
            $table->text('reference_information');
            $table->longText('content');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('locale')->index();
            $table->unique(['article_id', 'locale']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_translations');
        Schema::dropIfExists('articles');
    }
}
