<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->boolean('is_leadership');
            $table->boolean('is_press_service');
            $table->boolean('is_show_news');
            $table->boolean('is_review');
            $table->string('order');
            $table->timestamps();
        });

        Schema::create('command_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('command_id')->unsigned();
            $table->foreign('command_id')->references('id')->on('commands')->onDelete('cascade');
            $table->string('full_name');
            $table->text('review_text');
            $table->text('position');
            $table->string('phone_home');
            $table->string('phone_mobile');
            $table->longText('content');
            $table->string('locale')->index();
            $table->unique(['command_id', 'locale']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('command_translations');
        Schema::dropIfExists('commands');
    }
}
