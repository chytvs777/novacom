<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('command_id')->unsigned()->nullable();
            $table->foreign('command_id')->references('id')->on('commands')->onDelete('set null');
            $table->integer('direction_id')->unsigned()->nullable();
            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('set null');
            $table->string('slug');
            $table->boolean('is_hot');
            $table->integer('order')->unsigned();
            $table->timestamps();
        });

        Schema::create('vacancy_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vacancy_id')->unsigned();
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade');
            $table->string('title');
            $table->text('content_short');
            $table->string('content_title');
            $table->string('content_sub_title');
            $table->text('content_one');
            $table->text('content_two');
            $table->longText('data');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->string('locale')->index();
            $table->unique(['vacancy_id', 'locale']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancy_translations');
        Schema::dropIfExists('vacancies');
    }
}
