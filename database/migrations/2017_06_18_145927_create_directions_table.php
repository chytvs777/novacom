<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('directions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->unsigned();
            $table->timestamps();
        });

        Schema::create('direction_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('direction_id')->unsigned();
            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('cascade');
            $table->string('title');
            $table->string('locale')->index();
            $table->unique(['direction_id', 'locale']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direction_translations');
        Schema::dropIfExists('directions');
    }
}
